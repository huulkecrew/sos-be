<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStudentsWorkExperience extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_work_experience', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->string('company_name');
            $table->date('work_start_date');
            $table->date('work_end_date');
            $table->text('work_description');
            $table->text('skills_used');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students_work_experience');
    }
}
