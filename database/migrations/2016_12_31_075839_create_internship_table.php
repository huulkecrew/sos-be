<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internship', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('company_name');
            $table->string('job_title');
            $table->text('job_description');
            $table->string('job_location');
            $table->integer('job_category');
            $table->date('job_start_date');
            $table->date('job_end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('internship');
    }
}
