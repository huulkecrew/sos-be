<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCityAddressToInternship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('internship', function (Blueprint $table) {
            //
            $table->string('job_address')->nullable();
            $table->string('job_city')->nullable();
            $table->string('job_location')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('internship', function (Blueprint $table) {
            //
            $table->dropColumn('job_address');
            $table->dropColumn('job_city');
            $table->string('job_location')->change();

        });
    }
}
