<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTableAddSocialFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
           $table->string('facebook_id')->after('id');
           $table->string('google_id')->after('facebook_id');
           $table->integer('connected_via')->default('1')->after('user_status')->comment = '1 = Normal, 2 = Social Media';
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
