<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValuesModuleACompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('module_a_company', function (Blueprint $table) {
            //
            $table->string('lc_c_a')->nullable();
            $table->string('lc_c_b')->nullable();
            $table->string('lc_c_c')->nullable();
            $table->string('lc_c_d')->nullable();
            $table->string('lc_d_1')->nullable();
            $table->string('lc_d_2')->nullable();
            $table->string('lc_d_3')->nullable();
            $table->string('lc_d_4')->nullable();
            $table->string('lc_d_5')->nullable();
            $table->string('lc_d_6')->nullable();
            $table->string('lc_d_7_a')->nullable();
            $table->string('lc_d_7_b')->nullable();
            $table->string('lc_d_8')->nullable();
            $table->integer('lc_d_9')->nullable();
            $table->integer('lc_d_10')->nullable();
            $table->integer('lc_d_11')->nullable();
            $table->integer('lc_d_12')->nullable();
            $table->integer('lc_d_13')->nullable();
            $table->integer('lc_d_14')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('module_a_company', function (Blueprint $table) {
            //            $table->string('lc_c_a')->nullable();
            $table->dropColumn('lc_c_b');
            $table->dropColumn('lc_c_c');
            $table->dropColumn('lc_c_d');
            $table->dropColumn('lc_d_1');
            $table->dropColumn('lc_d_2');
            $table->dropColumn('lc_d_3');
            $table->dropColumn('lc_d_4');
            $table->dropColumn('lc_d_5');
            $table->dropColumn('lc_d_6');
            $table->dropColumn('lc_d_7_a');
            $table->dropColumn('lc_d_7_b');
            $table->dropColumn('lc_d_8');
            $table->dropColumn('lc_d_9');
            $table->dropColumn('lc_d_10');
            $table->dropColumn('lc_d_11');
            $table->dropColumn('lc_d_12');
            $table->dropColumn('lc_d_13');
            $table->dropColumn('lc_d_14');
        });
    }
}
