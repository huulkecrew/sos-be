<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleACompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_a_company', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('internship_request_id')->unique();
            $table->integer('company_id');
            $table->integer('status');
            $table->string('lc_1')->nullable();
            $table->string('lc_5_a')->nullable();
            $table->string('lc_5_b')->nullable();
            $table->string('lc_6')->nullable();
            $table->string('lc_8')->nullable();
            $table->string('lc_9')->nullable();
            $table->string('lc_10')->nullable();
            $table->string('lc_11_a')->nullable();
            $table->string('lc_11_b')->nullable();
            $table->string('lc_11_c')->nullable();
            $table->string('lc_11_d')->nullable();
            $table->string('lc_12_a')->nullable();
            $table->string('lc_12_b')->nullable();
            $table->string('lc_13_a')->nullable();
            $table->string('lc_13_b')->nullable();
            $table->string('lc_14_a')->nullable();
            $table->string('lc_14_b')->nullable();
            $table->string('lc_15')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_a_company');
    }
}
