<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleECompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_e_company', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('internship_request_id')->unique();
            $table->integer('company_id');
            $table->string('module_company_e_1_a')->nullable();
            $table->string('module_company_e_1_b')->nullable();
            $table->string('module_company_e_1_c')->nullable();
            $table->string('module_company_e_1_d')->nullable();
            $table->string('module_company_e_1_e')->nullable();
            $table->integer('module_company_e_2')->nullable();
            $table->integer('module_company_e_3')->nullable();
            $table->integer('module_company_e_4')->nullable();
            $table->integer('module_company_e_5')->nullable();
            $table->integer('module_company_e_6')->nullable();
            $table->integer('module_company_e_7')->nullable();
            $table->integer('module_company_e_8')->nullable();
            $table->integer('module_company_e_9')->nullable();
            $table->integer('module_company_e_10')->nullable();
            $table->integer('module_company_e_11')->nullable();
            $table->integer('module_company_e_12')->nullable();
            $table->integer('module_company_e_13')->nullable();
            $table->integer('module_company_e_14')->nullable();
            $table->integer('module_company_e_15')->nullable();
            $table->integer('module_company_e_16')->nullable();
            $table->integer('module_company_e_17')->nullable();
            $table->string('module_company_g_c2_1_a')->nullable();
            $table->string('module_company_g_c2_1_b')->nullable();
            $table->string('module_company_g_c2_1_c')->nullable();
            $table->string('module_company_g_c2_1_d')->nullable();
            $table->string('module_company_g_d_1')->nullable();
            $table->string('module_company_g_d_2')->nullable();
            $table->string('module_company_g_d_3')->nullable();
            $table->string('module_company_g_d_4')->nullable();
            $table->string('module_company_g_d_5')->nullable();
            $table->string('module_company_g_d_6')->nullable();
            $table->integer('module_company_g_e_1')->nullable();
            $table->integer('module_company_g_e_2')->nullable();
            $table->integer('module_company_g_e_3')->nullable();
            $table->integer('module_company_g_e_4')->nullable();
            $table->integer('module_company_g_e_5')->nullable();
            $table->integer('module_company_g_e_6')->nullable();
            $table->integer('module_company_g_e_7')->nullable();
            $table->integer('module_company_g_e_8')->nullable();
            $table->integer('module_company_g_e_9')->nullable();
            $table->integer('module_company_g_e_10')->nullable();
            $table->integer('module_company_g_e_11')->nullable();
            $table->integer('module_company_g_e_12')->nullable();
            $table->integer('module_company_g_e_13')->nullable();
            $table->integer('module_company_g_e_14')->nullable();
            $table->integer('module_company_g_e_15')->nullable();
            $table->integer('module_company_g_e_16')->nullable();
            $table->integer('module_company_g_e_17')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_e_company');
    }
}
