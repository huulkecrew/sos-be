<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleFStudent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_f_student', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('internship_request_id')->unique();
            $table->integer('student_id');
            $table->string('module_student_f_1')->nullable();
            $table->string('module_student_f_2')->nullable();
            $table->string('module_student_f_3')->nullable();
            $table->string('module_student_f_4')->nullable();
            $table->string('module_student_f_5')->nullable();
            $table->string('module_student_f_6')->nullable();
            $table->string('module_student_f_7')->nullable();
            $table->string('module_student_f_8')->nullable();
            $table->string('module_student_f_9')->nullable();
            $table->string('module_student_f_10')->nullable();
            $table->string('module_student_f_11')->nullable();
            $table->integer('module_student_f_12_a')->nullable();
            $table->integer('module_student_f_12_b')->nullable();
            $table->integer('module_student_f_12_c')->nullable();
            $table->integer('module_student_f_12_d')->nullable();
            $table->integer('module_student_f_12_e')->nullable();
            $table->integer('module_student_f_12_f')->nullable();
            $table->integer('module_student_f_12_g')->nullable();
            $table->integer('module_student_f_12_j')->nullable();
            $table->integer('module_student_f_12_k')->nullable();
            $table->integer('module_student_f_12_l')->nullable();
            $table->integer('module_student_f_12_m')->nullable();
            $table->integer('module_student_f_12_n')->nullable();
            $table->integer('module_student_f_12_o')->nullable();
            $table->integer('module_student_f_12_p')->nullable();
            $table->string('module_student_f_13_a')->nullable();
            $table->string('module_student_f_13_b')->nullable();
            $table->string('module_student_f_13_c')->nullable();
            $table->string('module_student_f_13_1_a')->nullable();
            $table->string('module_student_f_13_1_b')->nullable();
            $table->string('module_student_f_13_1_c')->nullable();
            $table->string('module_student_f_13_2')->nullable();
            $table->string('module_student_g_1_a')->nullable();
            $table->string('module_student_g_1_b')->nullable();
            $table->string('module_student_g_1_c')->nullable();
            $table->string('module_student_g_1_d')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_f_student');
    }
}
