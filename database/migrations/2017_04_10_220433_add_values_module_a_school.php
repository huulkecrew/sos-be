<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValuesModuleASchool extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('module_a_school', function (Blueprint $table) {
            //
            $table->string('ls_1_c_a')->nullable();
            $table->string('ls_1_c_b')->nullable();
            $table->string('ls_1_c_c')->nullable();
            $table->string('ls_1_c_d')->nullable();
            $table->string('ls_1_c_e')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('module_a_school', function (Blueprint $table) {
            //
            $table->dropColumn('ls_1_c_a');
            $table->dropColumn('ls_1_c_b');
            $table->dropColumn('ls_1_c_c');
            $table->dropColumn('ls_1_c_d');
            $table->dropColumn('ls_1_c_e');
        });
    }
}
