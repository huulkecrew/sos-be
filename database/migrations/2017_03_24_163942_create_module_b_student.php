<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleBStudent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_b_student', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('internship_request_id')->unique();
            $table->integer('student_id');
            $table->string('ls_1_a')->nullable();
            $table->string('ls_1_b')->nullable();
            $table->string('ls_1_c')->nullable();
            $table->string('ls_1_d')->nullable();
            $table->string('ls_1_e')->nullable();
            $table->string('ls_1_f')->nullable();
            $table->string('ls_1_g')->nullable();
            $table->string('uploaded_pdf')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_b_student');
    }
}
