<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function(Blueprint $table)
        {
           $table->string('address')->after('profile_pic');
           $table->string('city')->after('address');
           $table->string('state')->after('city');
           $table->string('zip_code')->after('state');
           $table->string('nationality')->after('zip_code');
           $table->integer('is_disable_person')->after('nationality');
           $table->integer('school')->after('is_disable_person');
           $table->string('class')->after('school');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
