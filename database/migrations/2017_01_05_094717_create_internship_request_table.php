<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternshipRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internship_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('internship_id');
            $table->integer('company_id');
            $table->integer('student_id');
            $table->integer('school_id');
            $table->string('school_name');
            $table->string('school_email');
            $table->text('motivation_letter');
            $table->integer('is_approved_by_company');
            $table->integer('is_approved_by_school');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('internship_request');
    }
}
