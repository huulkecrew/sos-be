<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleASchool extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_a_school', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('internship_request_id')->unique();
            $table->integer('school_id');
            $table->string('ls_1_a')->nullable();
            $table->string('ls_1_b')->nullable();
            $table->string('ls_1_c')->nullable();
            $table->string('ls_1_d')->nullable();
            $table->string('ls_7_a')->nullable();
            $table->string('ls_7_b')->nullable();
            $table->string('ls_7_c')->nullable();
            $table->string('ls_15_a')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_a_school');
    }
}
