<?php

use App\UserTypes;
use Illuminate\Database\Seeder;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type1 = new UserTypes();
		$type1->type_name = 'Student';
		$type1->save();

		$type2 = new UserTypes();
		$type2->type_name = 'School';
		$type2->save();

		$type3 = new UserTypes();
		$type3->type_name = 'Company';
		$type3->save();
    }
}
