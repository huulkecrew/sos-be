<?php

use App\InternshipCategories;
use Illuminate\Database\Seeder;

class InternshipCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat1 = new InternshipCategories();
		$cat1->name = 'Accounting';
		$cat1->save();

		$cat2 = new InternshipCategories();
		$cat2->name = 'Admin & HR';
		$cat2->save();

		$cat3 = new InternshipCategories();
		$cat3->name = 'Banking / Finance';
		$cat3->save();

		$cat4 = new InternshipCategories();
		$cat4->name = 'Beauty Care /Health';
		$cat4->save();

		$cat5 = new InternshipCategories();
		$cat5->name = 'Building & Construction';
		$cat5->save();

		$cat6 = new InternshipCategories();
		$cat6->name = 'Design';
		$cat6->save();

		$cat7 = new InternshipCategories();
		$cat7->name = 'Education';
		$cat7->save();

		$cat8 = new InternshipCategories();
		$cat8->name = 'Engineering';
		$cat8->save();

		$cat9 = new InternshipCategories();
		$cat9->name = 'Hospitality / F & B';
		$cat9->save();

		$cat10 = new InternshipCategories();
		$cat10->name = 'Information Technology (IT)';
		$cat10->save();

		$cat11 = new InternshipCategories();
		$cat11->name = 'Insurance';
		$cat11->save();

		$cat12 = new InternshipCategories();
		$cat12->name = 'Management';
		$cat12->save();

		$cat13 = new InternshipCategories();
		$cat13->name = 'Manufacturing';
		$cat13->save();

		$cat14 = new InternshipCategories();
		$cat14->name = 'Media & Advertising';
		$cat14->save();

		$cat15 = new InternshipCategories();
		$cat15->name = 'Property';
		$cat15->save();

		$cat16 = new InternshipCategories();
		$cat16->name = 'Others';
		$cat16->save();
    }
}
