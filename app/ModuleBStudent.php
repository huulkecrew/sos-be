<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ModuleBStudent extends Model
{
    protected $table = 'module_b_student';
    
    public function getModuleBStudent($param, $count=false){
        /* Check for valid company user */
        
        
        $module_b_student = DB::table($this->table)
                ->join('internship_request', 'internship_request.id', '=', 'module_b_student.internship_request_id')
                ->join('company', 'company.id', '=', 'internship_request.company_id')
                ->join('school', 'school.id', '=', 'internship_request.school_id')
                ->join('internship', 'internship.id', '=', 'internship_request.internship_id')
                ->select('internship_request.*', "company.*","school.name", "school.school_logo","internship.*" ,"internship_request.id as internship_request_id")
                ->where('module_b_student.student_id', '=', $param["student_id"])
                ->orderBy('module_b_student.updated_at', 'desc');
        if(isset($param["status"]) && $param["status"] > 0 ){
            $module_b_student->where('module_b_student.status', '=', $param["status"]);
        }
        if( isset($param["internship_request_id"]) && $param["internship_request_id"]>0 ){

            $module_b_student->where('module_b_student.internship_request_id', '=', $param["internship_request_id"])
                    ->addSelect('module_b_student.*');
            $data = $module_b_student->first();
        } else {
            if($count){
                $data = $module_b_student->count(); 
            } else {
                $module_b_student->limit($param['limit'])
                        ->offset($param['offset']);
                $data = $module_b_student->get(); 
            }
        }


        return $data;
        
        
    }
}
