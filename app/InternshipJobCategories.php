<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternshipJobCategories extends Model
{
    protected $table = 'internship_job_categories';
}
