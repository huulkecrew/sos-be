<?php

namespace App\Http\Controllers;

use URL,
    Auth,
    Password,
    Mail,
    JWTAuth;
use App\User;
use App\Users;
use App\UserTypes;
use App\Students;
use App\StudentsWorkExperience;
use App\StudentsSchoolRecords;
use App\Certification;
use App\Company;
use App\School;
use App\Image;
use App\Email;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;

class UsersController extends Controller {

    /**
     * @api {get} /api/getallusertypes User List
     * @apiName GetAllUserTypes
     * @apiGroup User
     * @apiDescription Get SOS's user type list
     * @apiPermission None
     *
     * @apiSuccess (200) {Object[]} Success The address list, Empty list if no address .
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": [
     *     {
     *      "id": 11,
      "user_id": "74",
      "address_type": "R",
      "label": "Home",
      "company": "",
      "address_1": "11760 Freguson Road",
      "address_2": "",
      "city": "Dallas",
      "state_id": "41",
      "state": "TX",
      "zipcode": "75228",
      "country_id": "223",
      "country": "US",
      "lat": "32.85",
      "lng": "-96.65",
      "instructions": "",
      "created_at": "2016-06-03 06:46:57",
      "updated_at": "2016-06-03 06:46:57"
     *     }
     *     ...
     *   ],
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     *
     */
    public function getAlluserTypes() {
        $userTypes = $this->globalHelper->getAllUserTypes();
        $resp = array('code' => $this->successParam, 'response' => array('user_types' => $userTypes));
        return $this->response->array($resp);
    }

    /**
     * Register User
     * @param Request $request
     * @return $resp
     */
    public function register(Request $request) {
        /* Fetching request parameters */
        $request = $request->only('user_type_id', 'email', 'password', 'first_name', 'last_name');

        /* Checking for required parameters */
        $required_parameter = array('user_type_id', 'email', 'password');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid user type id */
        $checkuserType = UserTypes::where('id', $request['user_type_id'])->count();
        if ($checkuserType == 0) {
            $resp = array('code' => $this->errorParam, 'message' => 'Tipologia di utente non valido', 'error' => 'INVALID_USER_TYPE');
            return $this->response->array($resp);
        }

        if ($request['user_type_id'] == $this->studentProfileType) {
            $checkStudentProfile = Users::where(array('email' => $request['email'], 'user_type_id' => $this->studentProfileType))->count();
            if ($checkStudentProfile > 0) {
                $resp = array('code' => $this->errorParam, 'message' => 'Utente già creato.', 'error' => 'STUDENT_PROFILE_IS_ALREADY_CREATED');
                return $this->response->array($resp);
            }
        }

        /* Check Password length */
        if (strlen($request['password']) < 6) {
            $resp = array('code' => $this->errorParam, 'message' => 'Password almeno di 6 caratteri.', 'error' => 'PASSWORD_LENGTH_NOT_CORRECT');
            return $this->response->array($resp);
        }

        /* Check for already registered user */
        $checkUser = User::where('email', $request['email'])->count();
        if ($checkUser > 0) {
            $resp = array('code' => $this->errorParam, 'message' => 'Email già esistente.', 'error' => 'EMAIL_ALREADY_EXISTS');
            return $this->response->array($resp);
        }

        /* Process  Request */
        $user = new Users();
        $user->user_type_id = $request['user_type_id'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->save();
        //$this->sendRegistrationEmail($user);

        if (isset($user)) {
            /* Create profile */
            $profileData = $this->createUserProfile($user, $request);

            /* Send meail verification mail */
            if ($this->emailVerification == 1) {
                $this->sendVerificationEmail($user);
            }

            $token = JWTAuth::attempt([
                        'email' => $request['email'],
                        'password' => $request['password']
            ]);

            $userData = Users::where('id', $user->id)->first();
            $userData->token = $token;
            $userData->profile_data = $profileData;
            unset($userData->password, $userData->remember_token);

            $resp = array('code' => $this->successParam, 'message' => ($this->emailVerification == 1) ? 'Utente creato correttamente, ti invitiamo a verificare la tua mail.' : 'Utente creato correttamente.', 'response' => array('user_data' => $userData));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Utente non creato, riprova.', 'error' => 'UNABLE_TO_CREATE_USER');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Create user profile
     * @param $userData, $request
     * @return $profileData
     */
    public function createUserProfile($userData, $request) {
        $profileData = array();
        if ($request['user_type_id'] == $this->studentProfileType) {
            $profileData = $this->createStudentProfile($userData, $request);
        } elseif ($request['user_type_id'] == $this->companyProfileType) {
            $profileData = $this->createCompanyProfile($userData, $request);
        } elseif ($request['user_type_id'] == $this->schoolProfileType) {
            $profileData = $this->createSchoolProfile($userData, $request);
        }
        return $profileData;
    }

    /**
     * Create Student Profile
     * @param $user, $request
     * @return $resp
     */
    public function createStudentProfile($user, $request) {
        /* Process Request */
        $student = new Students();
        $student->student_user_id = $user->id;
        $student->first_name = $request['first_name'];
        $student->last_name = $request['last_name'];
        $student->status = 1;
        $student->save();

        if (!empty($student->id)) {
            return Students::where('id', $student->id)->get();
        }
    }

    /**
     * Create Company Profile
     * @param $user, $request
     * @return $resp
     */
    public function createCompanyProfile($user, $request) {
        /* Process Request */
        $company = new Company();
        $company->company_user_id = $user->id;
        $company->first_name = $request['first_name'];
        $company->last_name = $request['last_name'];
        $company->status = 1;
        $company->save();

        if (!empty($company->id)) {
            return Company::where('id', $company->id)->first();
        }
    }

    /**
     * Create Company Profile
     * @param $user, $request
     * @return $resp
     */
    public function createSchoolProfile($user, $request) {
        /* Process Request */
        $school = new School();
        $school->school_user_id = $user->id;
        $school->status = 1;
        $school->save();

        if (!empty($school->id)) {
            return School::where('id', $school->id)->first();
        }
    }

    public function sendRegistrationEmail($user) {
        $email = $user->email;
        $link = $url_call_to_action = env('FRONT_END_URL');

        if ($user->user_type_id == $this->studentProfileType) {
            $data = Email::getEmailContent("REGISTRATION-STUDENT", $user->first_name);
        } elseif ($user->user_type_id == $this->companyProfileType) {
            $data = Email::getEmailContent("REGISTRATION-COMPANY", $user->first_name);
        } elseif ($user->user_type_id == $this->schoolProfileType) {
            $data = Email::getEmailContent("REGISTRATION-SCHOOL", $user->first_name);
        }
        $html = Email::getHtmlContent($data, $url_call_to_action,env('EMAIL_ACTIVATION'));
        $result = Email::sendEmail($email, $html, $data);
        return $result;
    }

    /**
     * Send Verification Email
     * @param $user
     */
    public function sendVerificationEmail($user) {
        /* Email verification token */
        $token = getToken(50);
        User::where('id', $user->id)->update(array('email_verification_token' => $token));

        /* Send Email */
        $email = $user->email;
        $url_call_to_action = env('FRONT_END_URL') . env('EMAIL_VERIFICATION_FRONT_SLUG') . $user->id . '/' . $token;
        
        if ($user->user_type_id == $this->studentProfileType) {
            $data = Email::getEmailContent("REGISTRATION-STUDENT", $user->first_name);
        } elseif ($user->user_type_id == $this->companyProfileType) {
            $data = Email::getEmailContent("REGISTRATION-COMPANY", $user->first_name);
        } elseif ($user->user_type_id == $this->schoolProfileType) {
            $data = Email::getEmailContent("REGISTRATION-SCHOOL", $user->first_name);
        }
        $html = Email::getHtmlContent($data, $url_call_to_action,env('EMAIL_ACTIVATION_TMP'));
        
        $result = Email::sendEmail($email, $html, $data);
        return $result;

    }

    /**
     * Verify Email
     * @param Request $request
     * @return Auth::user json data
     */
    public function verifyEmail(Request $request) {
        /* Decoding request */
        $request = $request->only('user_id', 'verify_token');

        /* Checking for required parameters */
        $required_parameter = array('user_id', 'verify_token');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        $user = User::where(array('id' => $request['user_id']))->first();
        if (isset($user)) {
            $userData = array(
                'user_id' => $user->id,
                'email' => $user->email
            );
            /* Check for already verified user */
            if ($user->is_email_verified == 1) {
                $resp = array('code' => $this->successParam, 'message' => 'Email già verificata.', 'user_data' => $userData);
                return $this->response->array($resp);
            }

            /* Validate token */
            if ($request['verify_token'] == $user->email_verification_token) {
                User::where('id', $user->id)->update(array('is_email_verified' => 1, 'user_status' => 1, 'email_verification_token' => ''));
                $resp = array('code' => $this->successParam, 'message' => 'Email verificata correttamente.', 'user_data' => $userData);
            } else {
                $resp = array('code' => $this->errorParam, 'message' => 'Link scaduto, riprova ad effettuare login.', 'error' => 'INVALID_TOKEN');
            }
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Utente inesistente.', 'error' => 'USER_NOT_FOUND');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Reset password
     * @param Request $request
     */
    public function resetPassword(Request $request) {
        /* Decoding request */
        $request = $request->only('email');

        /* Checking for required parameters */
        $required_parameter = array('email');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        $user = User::where('email', $request['email'])->first();
        if (!empty($user)) {
            $token = Password::createToken($user);
            $email = $request['email'];
            $url_call_to_action = $link = env('FRONT_END_URL') . env('CHANGE_PASSWORD_FRONT_SLUG') . $user->id . '/' . $token;
//            $html = sprintf(env('FORGOT_PASSWORD_MESSAGE'), $user->name, $link);

            /* Send Email */
            $data = Email::getEmailContent("RESET-PASSWORD");
            $html = Email::getHtmlContent($data, $url_call_to_action);
            $result = Email::sendEmail($email, $html, $data);
            if ($result) {
                $resp = array('code' => $this->successParam, 'message' => "Controlla l'email per reimpostare la password .");
            } else {
                $resp = array('code' => $this->errorParam, 'message' => 'Impossibile inviare email, riprova.', 'error' => 'UNABLE_TO_SEND_EMAIL');
            }
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Utente inesistente.', 'error' => 'USER_IS_NOT_EXISTS');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Change password by reset token
     * @param Request $request
     * @return $resp
     */
    public function changePassword(Request $request) {
        /* Fetching Request Params */
        $request = $request->only('token', 'user_id', 'password', 'password_confirmation');

        /* Checking for required parameters */
        $required_parameter = array('token', 'user_id', 'password', 'password_confirmation');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check Password length  && Match Password */
        if (strlen($request['password']) < 6 || strlen($request['password_confirmation']) < 6) {
            $resp = array('code' => $this->errorParam, 'message' => 'Password min 6 caratteri.', 'error' => 'PASSWORD_LENGTH_NOT_CORRECT');
            return $this->response->array($resp);
        }

        if ($request['password'] != $request['password_confirmation']) {
            $resp = array('code' => $this->errorParam, 'message' => 'Password devono essere uguali.', 'error' => 'PASSWORD_NOT_MATCHED');
            return $this->response->array($resp);
        }

        /* Check for valid user */
        $userId = $request['user_id'];
        $user = User::find($userId);

        if (isset($user)) {
            /* Validate token */
            try {
                $credentials = array('email' => $user->email, 'password' => $request['password'], 'password_confirmation' => $request['password'], 'token' => $request['token']);
                $response = Password::reset($credentials, function ($user, $password) {
                            $user->password = bcrypt($password);
                            $this->changed = $user->save();
                        });
            } catch (\Exception $e) {
                $resp = array('code' => $this->errorParam, 'message' => 'Riprova, è successo un\'errore.', 'error' => 'INTERNAL_ERROR');
            }

            if ($this->changed == 1) {
                $resp = array('code' => $this->successParam, 'message' => 'Password cambiata correttamente.');
            } else {
                $resp = array('code' => $this->errorParam, 'message' => 'Token non valido.', 'error' => 'INVALID_TOKEN');
            }
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Utente non esiste.', 'error' => 'USER_IS_NOT_EXISTS');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Login User
     * @param Request $request
     * @return $resp
     */
    public function login(Request $request) {
        /* Fetching request p[aram */
        $request = $request->only('email', 'password', "user_type_id");

        /* Checking for required parameters */
        $required_parameter = array('email', 'password');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }
        //Set students as default user type
        $user_type_id = isset($request["user_type_id"]) && $request["user_type_id"] !== "" ? $request["user_type_id"] : 1;
        /* Process Request */
        if (Auth::attempt(array('email' => $request['email'], 'password' => $request['password']))) {
            try {
                $token = JWTAuth::attempt(array('email' => $request['email'], 'password' => $request['password']));
                $user = Auth::user();
                $user->token = $token;

                if ($this->emailVerification == 1) {
                    if ($user->is_email_verified == 1) {
                        //Try to login with different 
                        if ($user->user_type_id == $user_type_id) {
                            $this->updateUserLoginDetials($user);
                            $resp = array('code' => $this->successParam, 'message' => 'Login effettuato correttamente.', 'user_data' => $user);
                        } else {
                            $resp = array('code' => $this->errorParam, 'message' => 'Non hai accesso a questo tipo di profilo', 'error' => 'LOGIN_WRONG_PROFILE');
                        }
                    } else {
                        //$this->sendVerificationEmail($user);
                        $resp = array('code' => $this->errorParam, 'message' => "La tua email deve essere ancora verificata, controlla nella tua casella di posta in arrivo o spam e clicca sul link di verifica email.", 'error' => 'EMAIL_IS_NOT_VERIFIED');
                    }
                } else {
                    $user->profile_data = $this->getProfileData($user);
                    $this->updateUserLoginDetials($user);
                    $resp = array('code' => $this->successParam, 'message' => 'Accesso corretto.', 'user_data' => $user);
                }
            } catch (JWTException $e) {
                $resp = array('code' => $this->errorParam, 'message' => 'Could not create token.', 'error' => 'COULD_NOT_CREATE_TOKEN');
            }
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Credenziali di accesso errati.', 'error' => 'USER_NOT_FOUND');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Get profile data
     * @param $userData
     * @return $profileData
     */
    public function getProfileData($userData) {
        $profileData = array();
        if ($userData->user_type_id == $this->studentProfileType) {
            $profileData = Students::where(array('student_user_id' => $userData->id))->get();
            if (!empty($profileData)) {
                $profileData->profile_pic = (!empty($profileData->profile_pic)) ? URL::to('/') . env('STUDENT_PROFILE_PIC_PATH') . $profileData->profile_pic : URL::to('/') . env('STUDENT_DEFAULT_PIC');
            }
        } elseif ($userData->user_type_id == $this->companyProfileType) {
            $companyData = Company::where(array('company_user_id' => $userData->id, 'status' => 1))->get();
            if (count($companyData) > 0) {
                foreach ($companyData as $val) {
                    $val->company_logo = (!empty($val->company_logo)) ? URL::to('/') . env('COMPANY_LOGO_PATH') . $val->company_logo : URL::to('/') . env('COMPANY_DEFAULT_PIC');
                    $profileData[] = $val;
                }
            }
        }

        return $profileData;
    }

    /**
     * pdate user login details
     * @param Obj $user
     */
    public function updateUserLoginDetials($user) {
        $updatedata = array(
            'last_login' => date('Y-m-d H:i:s'),
            'login_ip' => $_SERVER['REMOTE_ADDR']
        );
        User::where('id', $user->id)->update($updatedata);
    }

    /**
     * Edit user password
     * @param Request $request
     * @return $resp
     */
    public function editUserPassword(Request $request) {
        /* Fetching request parameters */
        $request = $request->only('user_id', 'password', 'confirm_password', "old_password");

        /* Checking for required parameters */
        $required_parameter = array('user_id', 'password', 'confirm_password', "old_password");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        $password = bcrypt($request['old_password']);

        /* Check for valid user */
        $checkUser = Users::where(array('id' => $request['user_id']))->first();

        if (empty($checkUser) || !Auth::attempt(array('email' => $checkUser->email, 'password' => $request['old_password']))) {
            $resp = array('code' => $this->errorParam, 'message' => 'Password vecchia sbagliata.', 'error' => 'WRONG_CREDENTIALS');
            return $this->response->array($resp);
        }

        /* Check Password length */
        if (strlen($request['password']) < 6) {
            $resp = array('code' => $this->errorParam, 'message' => 'Password length must be six digits.', 'error' => 'PASSWORD_LENGTH_NOT_CORRECT');
            return $this->response->array($resp);
        }

        if ($request['password'] != $request['confirm_password']) {
            $resp = array('code' => $this->errorParam, 'message' => 'Password and confirm password not matched.', 'error' => 'PASSWORD_NOT_MATCHED');
            return $this->response->array($resp);
        }

        /* Process Request */
        Users::where('id', $request['user_id'])->update(array('password' => bcrypt($request['password'])));
        $resp = array('code' => $this->successParam, 'message' => 'Password cambiata.');
        return $this->response->array($resp);
    }

    /**
     * Upload students profile pic
     * @param Request $request
     * @return $resp
     */
    public function uploadStudentProfilePic(Request $request) {
        /* Fetching request params */
        $object_info = $request->only('student_id');

        /* Checking for required parameters */
        $required_parameter = array('student_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student */
        $checkStudent = Students::where(array('student_user_id' => $object_info['student_id']))->first();
        if (empty($checkStudent)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo inesistente.', 'error' => 'STUDENT_NOT_EXISTS');
        }

        if (!empty($request->profile_pic)) {
            $url = URL::to('/');
            if (Input::file('profile_pic')->isValid() && substr(Input::file('profile_pic')->getMimeType(), 0, 5) == 'image') {
                $destinationPath = public_path() . env('STUDENT_PROFILE_PIC_PATH');
                $fileName = time() . Input::file('profile_pic')->getClientOriginalName();
                $fileName = str_replace(' ', '-', $fileName);

                if (Input::file('profile_pic')->move($destinationPath, $fileName)) {
                    /* Updating image in DB */
                    if (Students::where(array('student_user_id' => $object_info['student_id']))->update(array('profile_pic' => $fileName))) {
                        $imagepath = $url . env('STUDENT_PROFILE_PIC_PATH') . $fileName;
                        $resp = array('code' => $this->successParam, 'message' => 'Immagine caricata.', 'data' => array('student_id' => $object_info['student_id'], 'profile_pic' => $imagepath));
                    } else {
                        $resp = array('code' => $this->errorParam, 'message' => 'Impossibile caricare immagine.', 'error' => 'SERVER_ERROR');
                    }
                } else {
                    $resp = array('code' => $this->errorParam, 'message' => 'Impossibile caricare immagine.', 'error' => 'UNABLE_TO_UPLOAD_IMAGE');
                }
            } else {
                $resp = array('code' => $this->errorParam, 'message' => "Seleziona un'immagine valida", 'error' => 'PLEASE_SELECT_VALID_IMAGE');
            }
        } else {
            $resp = array('code' => $this->errorParam, 'message' => "Seleziona un'immagine valida.", 'error' => 'NO_IMAGE_SELECTED');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Upload company logo
     * @param Request $request
     * @return $resp
     */
    public function uploadCompanyLogo(Request $request) {
        /* Fetching request params */
        $object_info = $request->only('company_id');

        /* Checking for required parameters */
        $required_parameter = array('company_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid company */
        $checkCompany = Company::where(array('id' => $object_info['company_id']))->first();
        if (empty($checkCompany)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo inesistente.', 'error' => 'COMPANY_NOT_EXISTS');
        }

        if (!empty($request->company_logo)) {
            $url = URL::to('/');
            if (Input::file('company_logo')->isValid() && substr(Input::file('company_logo')->getMimeType(), 0, 5) == 'image') {
                $destinationPath = public_path() . env('COMPANY_LOGO_PATH');
                $fileName = time() . Input::file('company_logo')->getClientOriginalName();
                $fileName = str_replace(' ', '-', $fileName);

                if (Input::file('company_logo')->move($destinationPath, $fileName)) {
                    /* Updating image in DB */
                    if (Company::where(array('id' => $object_info['company_id']))->update(array('company_logo' => $fileName))) {
                        $imagepath = $url . env('COMPANY_LOGO_PATH') . $fileName;
                        $resp = array('code' => $this->successParam, 'message' => 'Company logo uploaded successfully.', 'data' => array('company_id' => $object_info['company_id'], 'company_logo' => $imagepath));
                    } else {
                        $resp = array('code' => $this->errorParam, 'message' => 'Unable to update company logo.', 'error' => 'SERVER_ERROR');
                    }
                } else {
                    $resp = array('code' => $this->errorParam, 'message' => 'Unable to upload company logo.', 'error' => 'UNABLE_TO_UPLOAD_LOGO');
                }
            } else {
                $resp = array('code' => $this->errorParam, 'message' => 'Please select valid company logo file.', 'error' => 'PLEASE_SELECT_VALID_IMAGE');
            }
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Please select company logo to upload.', 'error' => 'NO_IMAGE_SELECTED');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Upload company logo
     * @param Request $request
     * @return $resp
     */
    public function uploadSchoolLogo(Request $request) {
        /* Fetching request params */
        $object_info = $request->only('school_id');

        /* Checking for required parameters */
        $required_parameter = array('school_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid School */
        $checkSchool = School::where(array('id' => $object_info['school_id']))->first();
        if (empty($checkSchool)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo inesistente.', 'error' => 'SCHOOL_NOT_EXISTS');
        }

        if (!empty($request->school_logo)) {
            $url = URL::to('/');
            if (Input::file('school_logo')->isValid() && substr(Input::file('school_logo')->getMimeType(), 0, 5) == 'image') {
                $destinationPath = public_path() . env('SCHOOL_LOGO_PATH');
                $fileName = time() . Input::file('school_logo')->getClientOriginalName();
                $fileName = str_replace(' ', '-', $fileName);

                if (Input::file('school_logo')->move($destinationPath, $fileName)) {
                    /* Updating image in DB */
                    if (School::where(array('id' => $object_info['school_id']))->update(array('school_logo' => $fileName))) {
                        $imagepath = $url . env('SCHOOL_LOGO_PATH') . $fileName;
                        $resp = array('code' => $this->successParam, 'message' => 'Immagine caricata.', 'data' => array('school_id' => $object_info['school_id'], 'school_logo' => $imagepath));
                    } else {
                        $resp = array('code' => $this->errorParam, 'message' => 'Immagine impossibile da caricare.', 'error' => 'SERVER_ERROR');
                    }
                } else {
                    $resp = array('code' => $this->errorParam, 'message' => 'Immagine impossibile da caricare.', 'error' => 'UNABLE_TO_UPLOAD_LOGO');
                }
            } else {
                $resp = array('code' => $this->errorParam, 'message' => 'Immagine impossibile da caricare.', 'error' => 'PLEASE_SELECT_VALID_IMAGE');
            }
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Immagine impossibile da caricare.', 'error' => 'NO_IMAGE_SELECTED');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Get user profile data
     * @param Request $request
     * @return $resp
     */
    public function getUserProfileData(Request $request) {
        /* fetching request param */
        $request = $request->only('user_type_id', 'user_id', 'company_id', "school_id");

        /* Checking for required parameters */
        $required_parameter = array('user_type_id', 'user_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid user type id */
        $checkuserType = UserTypes::where('id', $request['user_type_id'])->count();
        if ($checkuserType == 0) {
            $resp = array('code' => $this->errorParam, 'message' => 'Invalid user type', 'error' => 'INVALID_USER_TYPE');
            return $this->response->array($resp);
        }

        /* Check for valid user */
        $checkUser = Users::where(array('id' => $request['user_id']))->first();
        if (empty($checkUser)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Utente inesistente.', 'error' => 'USER_NOT_EXISTS');
        }

        /* Get profile data depends on user type */
        $profileData = array();
        if ($request['user_type_id'] == $this->studentProfileType) {
            $profileData = $this->getStudentProfileData($request);
        } elseif ($request['user_type_id'] == $this->companyProfileType) {
            $profileData = $this->getCompanyProfileData($request);
        } elseif ($request['user_type_id'] == $this->schoolProfileType) {
            $profileData = $this->getSchoolProfileData($request);
        }

        $resp = array('code' => $this->successParam, 'response' => array('user_data' => $profileData));
        return $this->response->array($resp);
    }

    /**
     * Get student profile data
     * @param $request
     * @return $userData
     */
    public function getStudentProfileData($request) {
        $userData = Users::where(array('id' => $request['user_id'], 'user_type_id' => $request['user_type_id']))->first();

        $profileData = Students::where(array('student_user_id' => $userData['id']))->get();
        $profileData = Image::getImageProfile("Student", $profileData);
//        print_r($profileData);
//        exit();
        $userData->profile_data = $profileData;

        $userData->work_records = StudentsSchoolRecords::where(array('student_id' => $userData['id']))->get();

        $userData->work_experience = StudentsWorkExperience::where(array('student_id' =>$userData['id']))->get();
        
        $userData->certifications = Certification::where(array('student_id' => $userData['id']))->get();

        unset($userData->password, $userData->email_verification_token, $userData->remember_token);
        return $userData;
    }

    /**
     * Get company profile data
     * @param $request
     * @return $userData
     */
    public function getCompanyProfileData($request) {
        $userData = Users::where(array('id' => $request['user_id'], 'user_type_id' => $request['user_type_id']))->first();
        if (isset($request['company_id'])) {
            $profileData = Company::where(array('company_user_id' => $userData['id'], "id" => $request['company_id'], 'status' => 1))->get();
        } else {
            $profileData = Company::where(array('company_user_id' => $userData['id'], 'status' => 1))->get();
        }
        $profileData = \App\Image::getImageProfile("Company", $profileData);

        $userData->profile_data = $profileData;

        unset($userData->password, $userData->email_verification_token, $userData->remember_token);
        return $userData;
    }

    /**
     * Get company profile data
     * @param $request
     * @return $userData
     */
    public function getSchoolProfileData($request) {
        $userData = Users::where(array('id' => $request['user_id'], 'user_type_id' => $request['user_type_id']))->first();
        if (isset($request['school_id'])) {
            $profileData = School::where(array('school_user_id' => $userData['id'], "id" => $request['school_id'], 'status' => 1))->get();
        } else {
            $profileData = School::where(array('school_user_id' => $userData['id'], 'status' => 1))->get();
        }
        $profileData = \App\Image::getImageProfile("School", $profileData);

        $userData->profile_data = $profileData;

        unset($userData->password, $userData->email_verification_token, $userData->remember_token);
        return $userData;
    }

    /**
     * Do social login & signup
     * @param Request $request
     * @return $resp
     */
    public function doSocial(Request $request) {
        /* Fetching request param */
        $request = $request->only('user_type_id', 'social_id', 'social_type', 'email', 'name');

        /* Checking for required parameters */
        $required_parameter = array('user_type_id', 'social_id', 'social_type');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid user type id */
        $checkuserType = UserTypes::where('id', $request['user_type_id'])->count();
        if ($checkuserType == 0) {
            $resp = array('code' => $this->errorParam, 'message' => 'Invalid user type', 'error' => 'INVALID_USER_TYPE');
            return $this->response->array($resp);
        }

        /* Check for already existing user */
        $check = array($request['social_type'] . '_id' => $request['social_id'], 'email' => $request['email']);
        $checkUser = Users::where($check)->first();
        if (!empty($checkUser)) {
            $resp = $this->processSocialLogin($checkUser);
        } else {
            $checkEmail = Users::where('email', $request['email'])->first();
            if (!empty($checkEmail)) {
                Users::where('email', $request['email'])->update(array($request['social_type'] . '_id' => $request['social_id'], 'connected_via' => env('SOCIAL_CONNECTED_USER')));
                $resp = $this->processSocialLogin($checkEmail);
            } else {
                $resp = $this->processSocialSignup($request);
            }
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Process social login
     * @param $userData
     * @return $resp
     */
    public function processSocialLogin($userData) {
        /* Process Request */
        $token = JWTAuth::fromUser($userData);
        $user = Users::where('id', $userData->id)->first();
        unset($user->password, $user->remember_token);
        $user->token = $token;

        $user->profile_data = $this->getProfileData($user);
        $this->updateUserLoginDetials($user);
        $resp = array('code' => $this->successParam, 'message' => 'Login con successo.', 'response' => array('user_data' => $user));

        /* if($this->emailVerification == 1) {
          if($user->is_email_verified == 1) {
          $user->profile_data = $this->getProfileData($user);
          $this->updateUserLoginDetials($user);
          $resp = array('code' => $this->successParam, 'message' => 'User logged in successfully.', 'user_data' => $user);
          } else {
          $this->sendVerificationEmail($user);
          $resp = array('code' => $this->errorParam, 'message' => 'Your email is not verified, we have sent an email to you to verify your account.', 'error' => 'EMAIL_IS_NOT_VERIFIED');
          }
          } else {
          $user->profile_data = $this->getProfileData($user);
          $this->updateUserLoginDetials($user);
          $resp = array('code' => $this->successParam, 'message' => 'User logged in successfully.', 'user_data' => $user);
          } */
        return $resp;
    }

    /**
     * Process social login
     * @param $request
     * @return $resp
     */
    public function processSocialSignup($request) {
        if ($request['user_type_id'] == $this->studentProfileType) {
            $checkStudentProfile = Users::where(array('email' => $request['email'], 'user_type_id' => $this->studentProfileType))->count();
            if ($checkStudentProfile > 0) {
                $resp = array('code' => $this->errorParam, 'message' => 'Profilo già creato.', 'error' => 'STUDENT_PROFILE_IS_ALREADY_CREATED');
                return $this->response->array($resp);
            }
        }

        /* Process  Request */
        $nameArr = explode(' ', $request['name']);
        end($nameArr);
        $key = key($nameArr);
        $request['last_name'] = $nameArr[$key];
        unset($nameArr[$key]);
        $newName = $nameArr;
        $request['first_name'] = implode(' ', $newName);

        $user = new Users;
        $user->email = trim($request['email']);

        if ($request['social_type'] == 'google') {
            $user->google_id = $request['social_id'];
        }

        if ($request['social_type'] == 'facebook') {
            $user->facebook_id = $request['social_id'];
        }

        $user->user_type_id = $request['user_type_id'];
        $user->connected_via = env('SOCIAL_CONNECTED_USER');
        $user->save();

        if (isset($user)) {
            /* Create profile */
            $profileData = $this->createUserProfile($user, $request);

            /* Send meail verification mail */
            /* if($this->emailVerification == 1) {
              $this->sendVerificationEmail($user);
              } */

            $token = JWTAuth::fromUser($user);
            $userData = Users::where('id', $user->id)->first();
            unset($user->password, $user->remember_token);
            $userData->token = $token;
            $userData->profile_data = $profileData;

            /* $resp = array('code' => $this->successParam, 'message' => ($this->emailVerification == 1) ? 'User created successfully, please verify your email.' : 'User created successfully.', 'response' => array('user_data' => $userData)); */
            $resp = array('code' => $this->successParam, 'message' => 'Utente creato.', 'response' => array('user_data' => $userData));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Utente non creato.', 'error' => 'UNABLE_TO_CREATE_USER');
        }
        return $resp;
    }

    /**
     * Get use from token
     * @param Request request
     * @return user obj
     */
    public function tokencheck(Request $request) {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }
    
    public function sendMailChangePassword(Request $request) {
        $users = Users::get();
        $url_call_to_action = env('FRONT_END_URL');
        $data = Email::getEmailContent("NEW-WEB-SITE");
        $html = Email::getHtmlContent($data, $url_call_to_action);
        $email = "yiresse.abia@gmail.com";
        $count = 0;
        foreach ($users as $key => $value) {
            //User::where('id', $value->id)->update(array('is_email_verified' => 1));
            $result = Email::sendEmail($email, $html, $data);
            echo $count;
            $count++;
            break;
        }
        $resp = array('code' => $this->successParam, 'message' => 'Email inviate.');

    }

}
