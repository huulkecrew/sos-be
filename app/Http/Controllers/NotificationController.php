<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use URL, Auth, Password, Mail, JWTAuth;
use App\User;
use App\Users;
use App\UserTypes;
use App\Students;
use App\StudentsWorkExperience;
use App\StudentsSchoolRecords;
use App\Company;
use App\Internship;
use App\InternshipCategories;
use App\InternshipJobCategories;
use App\InternshipRequest;
use App\School;
use App\Image;

class NotificationController extends Controller
{
    //
    /**
     * Check all intership to which the user has been upplied
     * STATUS = 0 PENDIG
     * STATUS = 1 APPROVED
     * STATUS = 2 REJECTED
     * @param type $param
     * @return type
     */
    public function notificationStudentList(Request $request) {
        
        
         /* Fetching request params */
        $request = $request->only('student_id', "offset" , "limit");
        
        /* Checking for required parameters */
        $required_parameter = array( 'student_id', "offset" , "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }
        
        
        $internshipRequest = new InternshipRequest();
         /* Check for already apply */
        $arrayInternshipObj = $internshipRequest->getNotificationStudent($request);
        $arrayInternshipObj = Image::getImageProfile("Company",$arrayInternshipObj);
        if(count($arrayInternshipObj) > 0) {
            $next = (count($arrayInternshipObj) >= $request["limit"])?TRUE:FALSE;
            $resp = array('code' => $this->successParam, 'response' => array('internship_request' => $arrayInternshipObj , "has_next"=>$next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuna notifica presente.', 'error' => 'NOTHING_FOUND');
        }
        
        return $this->response->array($resp);
        
    }
    
    /**
     * Check all intership to which the user has been upplied
     * STATUS = 0 PENDIG
     * STATUS = 1 APPROVED
     * STATUS = 2 REJECTED
     * @param type $param
     * @return type
     */
    public function notificationCompanyList(Request $request) {
        
        
         /* Fetching request params */
        $request = $request->only('company_id', "offset" , "limit");
        
        /* Checking for required parameters */
        $required_parameter = array( 'company_id', "offset" , "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }
        
        
        $internshipRequest = new InternshipRequest();
         /* Check for already apply */
        $arrayInternshipObj = $internshipRequest->getNotificationStudent($request);
        $arrayInternshipObj = Image::getImageProfile("Company",$arrayInternshipObj);
        if(count($arrayInternshipObj) > 0) {
            $next = (count($arrayInternshipObj) >= $request["limit"])?TRUE:FALSE;
            $resp = array('code' => $this->successParam, 'response' => array('internship_request' => $arrayInternshipObj , "has_next"=>$next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuna notifica presente.', 'error' => 'NOTHING_FOUND');
        }
        
        return $this->response->array($resp);
        
    }
    
}
