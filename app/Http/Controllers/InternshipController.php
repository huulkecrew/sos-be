<?php

namespace App\Http\Controllers;

use URL,
    Auth,
    Password,
    Mail,
    JWTAuth;
use App\User;
use App\Users;
use App\UserTypes;
use App\Students;
use App\StudentsWorkExperience;
use App\StudentsSchoolRecords;
use App\Company;
use App\Internship;
use App\InternshipCategories;
use App\InternshipJobCategories;
use App\InternshipRequest;
use App\School;
use App\Image;
use App\Email;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;

class InternshipController extends Controller {

    /**
     * List internship categories
     * @return $resp
     */
    public function getInternshipCategories() {
        $cats = InternshipCategories::orderBy('name', 'asc')->get();

        if (count($cats) > 0) {
            foreach ($cats as $val) {
                $categories[] = array(
                    'id' => $val->id,
                    'name' => $val->name
                );
            }
            $resp = array('code' => $this->successParam, 'response' => array('categories' => $categories));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'No internship categories found.', 'error' => 'NO_CATEGORIES_FOUND');
        }

        /* Return response */
        return $this->response->array($resp);
    }

    /**
     * Add internship
     * @param Request $request
     * @return $resp
     */
    public function addInternship(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id', 'company_name', 'job_title', 'job_description', 'job_location', 'job_category', 'job_start_date', 'job_end_date' , "job_address","job_city" , "deadline");

        /* Checking for required parameters */
        $required_parameter = array('company_id', 'company_name', 'job_title', 'job_category', 'job_start_date', 'job_end_date' , "job_address","job_city" , "deadline");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid company */
        $checkCompany = Company::where(array('id' => $request['company_id']))->first();
        if (empty($checkCompany)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Company is not exists.', 'error' => 'COMPANY_IS_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $internship = new Internship();
        $internship->company_id = $request['company_id'];
        $internship->company_name = $request['company_name'];
        $internship->job_title = $request['job_title'];
        $internship->job_description = $request['job_description'];
        $internship->job_location = $request['job_location'];
        $internship->job_address = $request['job_address'];
        $internship->job_city = $request['job_city'];
        $internship->deadline = date($this->dateForDb, strtotime($request['deadline']));
        $internship->job_start_date = date($this->dateForDb, strtotime($request['job_start_date']));
        $internship->job_end_date = date($this->dateForDb, strtotime($request['job_end_date']));
        $internship->status = 1;
        $internship->save();

        if (!empty($internship->id)) {
            /* Adding internship job categories */
            if (!empty($request['job_category'])) {
                foreach ($request['job_category'] as $val) {
                    $catInfo = InternshipCategories::where('id', $val)->first();
                    $cat = new InternshipJobCategories();
                    $cat->internship_id = $internship->id;
                    $cat->internship_category_id = $val;
                    $cat->internship_category_name = $catInfo->name;
                    $cat->save();
                    unset($cat);
                }
            }

            $internship->job_category = $this->getInternshipCatInfo($internship->id);
            $resp = array('code' => $this->successParam, 'message' => 'Internship added successfully.', 'response' => array('data' => $internship));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Unable to add internship, please try again laster.', 'error' => 'UNABLE_TO_ADD_INTERNSHIP');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Get Internship Detail
     * @param Request $request
     * @return $resp
     */
    public function getInternshipDetails(Request $request) {
        /* Fetching request params */
        $request = $request->only('internship_id');

        /* Checking for required parameters */
        $required_parameter = array('internship_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid internship */
        $internship = Internship::where(array('id' => $request['internship_id']))->first();
        if (empty($internship)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Stage inesistenete.', 'error' => 'INTERNSHIP_IS_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process request */
        $company = Company::where(array('id' => $internship->company_id))->first();
        $company->company_logo = (!empty($company->company_logo)) ? URL::to('/') . env('COMPANY_LOGO_PATH') . $company->company_logo : URL::to('/') . env('COMPANY_DEFAULT_PIC');
        $internship->company_data = $company;
        $internship->job_category = $this->getInternshipCatInfo($request['internship_id']);
        $resp = array('code' => $this->successParam, 'response' => array('data' => $internship));
        return $this->response->array($resp);
    }

    /**
     * Edit internship
     * @param Request $request
     * @return $resp
     */
    public function editInternship(Request $request) {
        /* Fetching request params */
        $request = $request->only("internship_id",'company_name', 'job_title', 'job_description', 'job_location', 'job_category', 'job_start_date', 'job_end_date' , "job_address","job_city" , "deadline");

        /* Checking for required parameters */
        $required_parameter = array('internship_id' );
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid internship */
        $checkInternship = Internship::where(array('id' => $request['internship_id']))->first();
        if (empty($checkInternship)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Stage inesistente.', 'error' => 'INTERNSHIP_IS_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $updateData = $request;
        $jobCats = $updateData['job_category'];

        unset($updateData['internship_id'], $updateData['job_category']);

        $updateData['job_start_date'] = date($this->dateForDb, strtotime($updateData['job_start_date']));
        $updateData['job_end_date'] = date($this->dateForDb, strtotime($updateData['job_end_date']));
        $updateData['deadline'] = date($this->dateForDb, strtotime($updateData['deadline']));


        /* Updating internship data */
        Internship::where('id', $request['internship_id'])->update($updateData);

        /* Updating internship job categories */
        if (!empty($jobCats)) {
            InternshipJobCategories::where(array('internship_id' => $request['internship_id']))->delete();
            foreach ($jobCats as $val) {
                try {
                    $catInfo = InternshipCategories::where('id', $val)->first();
                    $cat = new InternshipJobCategories();
                    $cat->internship_id = $request['internship_id'];
                    $cat->internship_category_id = $val;
                    $cat->internship_category_name = $catInfo->name;
                    $cat->save();

                } catch (Exception $exc) {
                    $resp = array('code' => $this->errorParam, 'message' => $exc->getTraceAsString(), 'error' => 'INTERNSHIP_EDIT_CATEGORIES_ERROR');
                    /* Return Response */
                    return $this->response->array($resp);
                    break;
                }
                unset($cat);
            }
        }

        $updateData['job_category'] = $this->getInternshipCatInfo($request['internship_id']);
        $updateData['id'] = $request['internship_id'];

        $resp = array('code' => $this->successParam, 'message' => 'Stage modificato.', 'response' => array('data' => $updateData));

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Delete internship
     * @param Request $request
     * @return $resp
     */
    public function deleteInternship(Request $request) {
        /* Fetching request params */
        $request = $request->only('internship_id');

        /* Checking for required parameters */
        $required_parameter = array('internship_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid internship */
        $checkInternship = Internship::where('id', $request['internship_id'])->count();
        if ($checkInternship == 0) {
            $resp = array('code' => $this->successParam, 'message' => 'Stage inesistenete.', 'error' => 'INTERNSHIP_IS_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        Internship::where('id', $request['internship_id'])->delete();
        InternshipJobCategories::where('internship_id', $request['internship_id'])->delete();

        $resp = array('code' => $this->successParam, 'message' => 'Stage eliminat correttamente.');

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Get company all internship
     * @param Request $request
     * @return $resp
     */
    public function listCompanyAllInternships(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id', 'keyword', 'limit', 'offset');

        /* Checking for required parameters */
        $required_parameter = array('company_id', 'limit', 'offset');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid company */
        $checkCompany = Company::where(array('id' => $request['company_id']))->first();
        if (empty($checkCompany)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Company is not exists.', 'error' => 'COMPANY_IS_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process request */
        $internshipModel = new Internship();
        $internships = $internshipModel->getAllInternship($request);

        if (count($internships) > 0) {
            /* Check for pagination data info */
            $result = $request['limit'];
            $offset = $request['offset'];

            $data['limit'] = $result;
            $data['offset'] = $offset;

            $checkNext = $result + $offset;
            $totalInternships = $internshipModel->getTotalInternship($request);

            /* Check for hasNext */
            if ($checkNext >= $totalInternships) {
                $hasNext = FALSE;
            } else {
                $hasNext = TRUE;
            }

            foreach ($internships as $val) {
                $company = Company::where(array('id' => $val->company_id))->first();
                $company->company_logo = (!empty($company->company_logo)) ? URL::to('/') . env('COMPANY_LOGO_PATH') . $company->company_logo : URL::to('/') . env('COMPANY_DEFAULT_PIC');
                $val->company_data = $company;
                $val->job_category = $this->getInternshipCatInfo($val->id);
                $internshipData[] = $val;
            }

            $dataInfo = array(
                'hasNext' => $hasNext,
                'limit' => $result,
                'offset' => $offset,
                'total_records' => $totalInternships
            );

            $resp = array('code' => $this->successParam, 'response' => array('internships' => $internshipData, 'dataInfo' => $dataInfo));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuno stage trovato.', 'error' => 'NO_INTERNSHIP_FOUND');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Get all internship
     * @param Request $request
     * @return $resp
     */
    public function listAllInternships(Request $request) {
        /* Fetching request params */
        $request = $request->only('keyword', 'limit', 'offset');

        /* Checking for required parameters */
        $required_parameter = array('limit', 'offset');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Process request */
        $internshipModel = new Internship();
        $internships = $internshipModel->getAllInternship($request);

        if (count($internships) > 0) {
            /* Check for pagination data info */
            $result = $request['limit'];
            $offset = $request['offset'];

            $data['limit'] = $result;
            $data['offset'] = $offset;

            $checkNext = $result + $offset;
            $totalInternships = $internshipModel->getTotalInternship($request);

            /* Check for hasNext */
            if ($checkNext >= $totalInternships) {
                $hasNext = FALSE;
            } else {
                $hasNext = TRUE;
            }
            $internshipData = array();
            foreach ($internships as $val) {
                $company = Company::where(array('id' => $val->company_id))->first();
                if(isset($company->id)){
                $company->company_logo = (!empty($company->company_logo)) ? URL::to('/') . env('COMPANY_LOGO_PATH') . $company->company_logo : URL::to('/') . env('COMPANY_DEFAULT_PIC');
                $val->company_data = $company;
                $val->job_category = $this->getInternshipCatInfo($val->id);
                $internshipData[] = $val;
                }
            }

            $dataInfo = array(
                'hasNext' => $hasNext,
                'limit' => $result,
                'offset' => $offset,
                'total_records' => $totalInternships
            );

            $resp = array('code' => $this->successParam, 'response' => array('internships' => $internshipData, 'dataInfo' => $dataInfo));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuno stage trovato.', 'error' => 'NO_INTERNSHIP_FOUND');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Get internship cats
     * @param $internshipId 
     * @return $catData    
     */
    public function getInternshipCatInfo($internshipId) {
        $catData = array();
        $cats = InternshipJobCategories::where(array('internship_id' => $internshipId))->get();
        if (count($cats) > 0) {
            foreach ($cats as $val) {
                $catData[] = array(
                    'id' => $val->id,
                    'internship_id' => $val->internship_id,
                    'internship_category_id' => $val->internship_category_id,
                    'internship_category_name' => $val->internship_category_name
                );
            }
        }
        return $catData;
    }

    /**
     * Apply on internship
     * @param Request $request
     * @return $resp
     */
    public function applyOnInternship(Request $request) {
        /* Fetching request params */
        $request = $request->only('internship_id', 'student_id', 'school_id', 'school_name', 'school_email', 'school_telephone', 'motivation_letter');

        /* Checking for required parameters */
        $required_parameter = array('internship_id', 'student_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }
        if(isset($request["school_id"]) && $request["school_id"] > 0){
            $updateData["school"] = $request["school_id"];
            $updateData["school_name"] = $request["school_name"];
            Students::where(array('id' => $request['student_id']))->update($updateData);
        }


        /* Check for valid internship */
        $checkInternship = Internship::where('id', $request['internship_id'])->first();
        if (empty($checkInternship)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Stage inesistenete.', 'error' => 'INTERNSHIP_IS_NOT_EXISTS');
            return $this->response->array($resp);
        }

        if ( strtotime(date("Y-m-d",time())) > strtotime($checkInternship->deadline) &&  strtotime($checkInternship->deadline) > 0 ) {
            $resp = array('code' => $this->errorParam, 'message' => 'Deadline scaduta per l’invio delle candidature', 'error' => 'INTERNSHIP_IS_NOT_AVAILABLE');
            return $this->response->array($resp);
        }
        /* Check for valid student */
        $checkStudent = Students::where(array('id' => $request['student_id']))->first();
        if (empty($checkStudent)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Prima di cadidarti devi completare le informazioni del tuo profilo studente.', 'error' => 'STUDENT_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Check for already apply * 
         */
        $checkApply = InternshipRequest::where(array('internship_id' => $request['internship_id'], 'student_id' => $request['student_id']))->count();
        if ($checkApply > 0) {
            $resp = array('code' => $this->errorParam, 'message' => 'Stage a cui hai già inviato la candidatura.', 'error' => 'ALREADY_APPLIED');
            return $this->response->array($resp);
        }
         

        /* Process Request */
        $req = new InternshipRequest();
        $req->internship_id = $request['internship_id'];
        $req->company_id = $checkInternship->company_id;
        $req->student_id = $request['student_id'];
        $req->school_id = $request['school_id'];
        $req->school_name = $request['school_name'];
        $req->school_email = $request['school_email'];
        $req->school_telephone = $request['school_telephone'];
        $req->motivation_letter = $request['motivation_letter'];
        $req->is_approved_by_company = 0;
        $req->is_approved_by_school = 0;
        $req->save();

        if (!empty($req->id)) {
            /* Send Notification */
            $userStudent = Users::where('id', $checkStudent->student_user_id)->first();
            $company = Company::where('id', $checkInternship->company_id)->first();
            $companyUser = Users::where('id', $company->company_user_id)->first();

            if (!empty($request['school_id'])) {
                $schoolData = School::where('id', $request['school_id'])->first();
                $school = Users::where('id', $schoolData->school_user_id)->first();
                $schoolEmail = $school->email;
                $schoolName = $schoolData->name;
            } else {
                $schoolEmail = $request['school_email'];
                $schoolName = $request['school_name'];
            }

            $notification = array(
                'internship_id' => $request['internship_id'],
                'internship_data' => $checkInternship,
                'student_id' => $request['student_id'],
                'student_name' => ucfirst($checkStudent->first_name . ' ' . $checkStudent->last_name),
                'student_email' => $userStudent->email,
                'company_id' => $checkInternship->company_id,
                'company_name' => $company->company_name,
                'company_email' => $companyUser->email,
                'school_id' => $request['school_id'],
                'school_email' => $schoolEmail,
                'school_name' => $schoolName
            );
            $this->sendInternshipApplyNotification($notification);

            $resp = array('code' => $this->successParam, 'message' => 'Candidatura stage andata a buon fine.');
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Impossible partecipare allo stage, riprova.', 'error' => 'UNABLE_TO_APPLY_ON_INTERNSHIP');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Check all intership to which the user has been upplied
     * STATUS = 0 PENDIG
     * STATUS = 1 APPROVED
     * STATUS = 2 REJECTED
     * @param type $param
     * @return type
     */
    public function internshipAppliedList(Request $request) {


        /* Fetching request params */
        $request = $request->only('student_id', "offset", "limit");

        /* Checking for required parameters */
        $required_parameter = array('student_id', "offset", "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $internshipRequest = new InternshipRequest();
        /* Check for already apply */
        $arrayInternshipObj = $internshipRequest->getIntershipApplied($request);
        $count = $internshipRequest->getIntershipApplied($request);
        $arrayInternshipObj = Image::getImageProfile("Company", $arrayInternshipObj);

        if (count($arrayInternshipObj) > 0) {
            $next = (count($arrayInternshipObj) >= $request["limit"]) ? TRUE : FALSE;

            $resp = array('code' => $this->successParam, 'response' => array('internship_request' => $arrayInternshipObj, "has_next" => $next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuna richiesta di stage presente.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }

    /**
     * Check all intership to which the user has been upplied
     * STATUS = 0 PENDIG
     * STATUS = 1 APPROVED
     * STATUS = 2 REJECTED
     * 
     * @param type student_id = id logged, internship_request_id the id of the request
     * @return type
     */
    public function internshipAppliedDetail(Request $request) {


        /* Fetching request params */
        $request = $request->only('student_id', "internship_request_id");

        /* Checking for required parameters */
        $required_parameter = array('student_id', "internship_request_id");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $internshipRequest = new InternshipRequest();
        /* Check for already apply */
        $arrayInternshipObj = $internshipRequest->getIntershipApplied($request);
        $arrayInternshipObj = Image::getImageProfile("Company", $arrayInternshipObj);
        if (count($arrayInternshipObj) > 0) {
            $resp = array('code' => $this->successParam, 'response' => array('internship_request' => $arrayInternshipObj));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuna richiesta di stage presente.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }

    /**
     * Send apply internship notification
     * @param $data
     */
    public function sendInternshipApplyNotification($data) {
        $internshiplink = env('FRONT_END_URL') . env('VIEW_STAGE_FRONT_SLUG') . $data['internship_id'];
        
        /* Send notification to Student 
        $studentEmail = $data['student_email'];

        $data_content = Email::getEmailContent("STUDENT-REQUEST-INTERNSHIP" , $data);
        $html = Email::getHtmlContent($data_content, $internshiplink);
        $result = Email::sendEmail($studentEmail, $html, $data_content);
         * 
         */
        

        /* Send notification to school */
        $schoolEmail = $data['school_email'];
        
        /* Send Email */
        //We are checking if the school has already been signed up or not
        
        $data_content = (isset($data["school_id"]) && $data["school_id"] > 0 )?Email::getEmailContent("REQUEST-INTERNSHIP-TO-SCHOOL-SIGNED" , $data):Email::getEmailContent("REQUEST-INTERNSHIP-TO-SCHOOL-NOT-SIGNED" , $data);
        $html = Email::getHtmlContent($data_content, $internshiplink);
        $result = Email::sendEmail($schoolEmail, $html, $data_content);
        
        
            
        /* Send notification to company */
        $companyEmail = $data['company_email'];
         /* Send Email */
        $data_content = Email::getEmailContent("REQUEST-INTERNSHIP-TO-COMPANY" , $data);
        $html = Email::getHtmlContent($data_content, $internshiplink);
        $result = Email::sendEmail($companyEmail, $html, $data_content);
        
        return true;
    }
    

}
