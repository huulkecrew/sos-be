<?php

namespace App\Http\Controllers;

use URL, Auth, Password, Mail, JWTAuth;
use App\User;
use App\Users;
use App\UserTypes;
use App\Students;
use App\StudentsWorkExperience;
use App\StudentsSchoolRecords;
use App\Certification;
use App\ModuleBStudent;
use App\ModuleFStudent;
use App\Image;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;

class StudentsController extends Controller
{
    /**
     * Complete student profile
     * @param Request $request
     * @return $resp
     */
    public function completeStudentProfile(Request $request){
    	/* Fetching request params */
    	$request = $request->only('student_id', 'first_name', 'last_name','gender', 'phone_number', 'birthday', 'address', 'city', 'state', 'zip_code', 'nationality', 'is_disable_person', 'school', 'school_name', 'school_email', 'school_telephone', 'class');
    	
    	/* Checking for required parameters */
    	$required_parameter = array('student_id');
    	$chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student */
        $checkStudent = Students::where(array('student_user_id' => $request['student_id']))->first();
        if(empty($checkStudent)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo inesistente.', 'error' => 'STUDENT_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $updateData = $request;
        unset($updateData['student_id']);
        $updateData['birthday'] = date($this->dateForDb,strtotime($updateData['birthday']));

        /* Updating records */
        Students::where(array('student_user_id' => $request['student_id']))->update($updateData);

        /* Fetching profile info */
        $profileData = Students::where(array('student_user_id' => $request['student_id']))->first();
        $profileData->profile_pic = (!empty($profileData->profile_pic)) ? URL::to('/').env('STUDENT_PROFILE_PIC_PATH').$profileData->profile_pic : URL::to('/').env('STUDENT_DEFAULT_PIC');
        
        /* Send notification to school */
        if($request['school'] == 0) {
            $this->sendSchoolNotification($request);
        }

        $resp = array('code' => $this->successParam, 'message' => 'Profilo aggiornato correttamente.', 'response' => array('profile_data' => $profileData));
    	return $this->response->array($resp);
    }

    /**
     * Send School Notification
     * @param $request
     */
    public function sendSchoolNotification($request){
        $email = $request['school_email'];
        $link = env('FRONT_END_URL');
        $html = sprintf(env('SCHOOL_EMAIL_MESSAGE'), $request['school_name'], ucfirst($request['first_name'].' '.$request['last_name']), $link, env('APPLICATION_NAME'), $link, env('APPLICATION_NAME'));
        
        /* Send Email */
        try{
            Mail::send(array(), array(), function ($message) use ($email, $html) {
            $message->to($email)
                ->subject(env('SCHOOL_EMAIL_HEADER'))
                ->from(env('FROM_EMAIL'))
                ->setBody($html, 'text/html');
            });

            $resp = array('code' => $this->successParam, 'message' => 'Controlla la mail per reimpostare la password.');
        } 
        catch(\Exception $e){
        }
    }

    /**
     * Add student school records
     * @param Request $request
     * @return $resp
     */
    public function addStudentSchoolRecords(Request $request){
    	/* Fetching request params */
        $schoolData = array();
        $schoolData = $request->school;
    	$request = $request->only('student_id');
    	
    	/* Checking for required parameters */
    	$required_parameter = array('student_id');
    	$chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student */
        $checkStudent = Students::where(array('student_user_id' => $request['student_id']))->first();
        if(empty($checkStudent)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo inesistente.', 'error' => 'STUDENT_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        if(!empty($schoolData)) {
            foreach($schoolData['subject'] as $key => $val) {
                if(!empty($schoolData['id'][$key]) && StudentsSchoolRecords::where(array('id' => $schoolData['id'][$key], 'student_id' => $request['student_id']))->count() > 0) {
                    $id = $schoolData['id'][$key];
                    $updateData = array(
                        'subject' => $schoolData['subject'][$key],
                        'vote' => $schoolData['vote'][$key]
                    );

                    StudentsSchoolRecords::where(array('id' => $id, 'student_id' => $request['student_id']))->update($updateData);
                } else {
                    $records = new StudentsSchoolRecords();
                    $records->student_id = $request['student_id'];
                    $records->subject = $schoolData['subject'][$key];
                    $records->vote = $schoolData['vote'][$key];
                    $records->save();
                    unset($records);
                }
            }
            $resp = array('code' => $this->successParam, 'message' => 'Voti aggiornati correttamente.');
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Impossobile aggiungere voti, riprova ancora.', 'error' => 'UNABLE_TO_ADD_STUDENT_SCHOOL_RECORDS');
        }

    	return $this->response->array($resp);
    }

    /**
     * Get student school records
     * @param Request $request
     * @return $resp
     */
    public function getStudentSchoolRecords(Request $request){
        /* Fetching request params */
        $request = $request->only('student_id');
        
        /* Checking for required parameters */
        $required_parameter = array('student_id');
        $chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student */
        $checkStudent = Students::where(array('student_user_id' => $request['student_id']))->first();
        if(empty($checkStudent)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo non esistente', 'error' => 'STUDENT_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $schoolData = array();
        $schoolData = StudentsSchoolRecords::where(array('student_id' => $request['student_id']))->get();
        if(count($schoolData) > 0) {
            $resp = array('code' => $this->successParam, 'response' => array('school_data' => $schoolData));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessun voto presente', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }

    /**
     * Delete student school record
     * @param Request $request
     * @return $resp
     */
    public function deleteStudentSchoolRecord(Request $request){
        /* Fetching request param */
        $request = $request->only('id');

        /* Checking for required parameters */
        $required_parameter = array('id');
        $chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student school record */
        $checkRecord = StudentsSchoolRecords::where('id', $request['id'])->first();
        if(empty($checkRecord)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessun voto presente.', 'error' => 'RECORD_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        StudentsSchoolRecords::where('id', $request['id'])->delete();
        $resp = array('code' => $this->successParam, 'message' => 'Voto eliminato correttamente.');
        return $this->response->array($resp);
    }

    /**
     * Add student work experience
     * @param Request $request
     * @return $resp
     */
    public function addStudentWorkExperience(Request $request){
    	/* Fetching request params */
        $workData = array();
        $workData = $request->work;
    	$request = $request->only('student_id');
    	
    	/* Checking for required parameters */
    	$required_parameter = array('student_id');
    	$chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student */
        $checkStudent = Students::where(array('student_user_id' => $request['student_id']))->first();
        if(empty($checkStudent)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo non esistente', 'error' => 'STUDENT_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        if(!empty($workData)) {
            foreach($workData['company_name'] as $key => $val) {
                if(!empty($workData['id'][$key]) && StudentsWorkExperience::where(array('id' => $workData['id'][$key], 'student_id' => $request['student_id']))->count() > 0) {
                    $id = $workData['id'][$key];
                    $updateData = array(
                        'company_name' => $workData['company_name'][$key],
                        'work_start_date' => date($this->dateForDb, strtotime($workData['work_start_date'][$key])),
                        'work_end_date' => date($this->dateForDb, strtotime($workData['work_end_date'][$key])),
                        'work_description' => $workData['work_description'][$key],
                        'skills_used' => $workData['skills_used'][$key]
                    );
                    StudentsWorkExperience::where(array('id' => $id, 'student_id' => $request['student_id']))->update($updateData);
                } else {
                    $experience = new StudentsWorkExperience();
                    $experience->student_id = $request['student_id'];
                    $experience->company_name = $workData['company_name'][$key];
                    $experience->work_start_date = date($this->dateForDb, strtotime($workData['work_start_date'][$key]));
                    $experience->work_end_date = date($this->dateForDb, strtotime($workData['work_end_date'][$key]));
                    $experience->work_description = $workData['work_description'][$key];
                    $experience->skills_used = $workData['skills_used'][$key];
                    $experience->save();
                    unset($experience);
                }
            }
            $resp = array('code' => $this->successParam, 'message' => 'Student work experience updated successfully.');
        } else {
        	$resp = array('code' => $this->errorParam, 'message' => 'Unable to add student work experience.', 'error' => 'UNABLE_TO_ADD_STUDENT_WORK_EXPERIENCE');
        }

    	return $this->response->array($resp);
    }

    /**
     * Get student work experiences
     * @param Request $request
     * @return $resp
     */
    public function getStudentWorkExperiences(Request $request){
        /* Fetching request params */
        $request = $request->only('student_id');
        
        /* Checking for required parameters */
        $required_parameter = array('student_id');
        $chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student */
        $checkStudent = Students::where(array('student_user_id' => $request['student_id']))->first();
        if(empty($checkStudent)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo non esistente', 'error' => 'STUDENT_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $expData = array();
        $expData = StudentsWorkExperience::where(array('student_id' => $request['student_id']))->get();
        if(count($expData) > 0) {
            $resp = array('code' => $this->successParam, 'response' => array('experience_data' => $expData));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'No student work experience.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }

    /**
     * Delete student work experience
     * @param Request $request
     * @return $resp
     */
    public function deleteStudentWorkExperience(Request $request){
        /* Fetching request param */
        $request = $request->only('id');

        /* Checking for required parameters */
        $required_parameter = array('id');
        $chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student school record */
        $checkRecord = StudentsWorkExperience::where('id', $request['id'])->first();
        if(empty($checkRecord)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Student work experience not exists.', 'error' => 'RECORD_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        StudentsWorkExperience::where('id', $request['id'])->delete();
        $resp = array('code' => $this->successParam, 'message' => 'Student work experience deleted successfully.');
        return $this->response->array($resp);
    }
    
      /**
     * Add studentcertification
     * @param Request $request
     * @return $resp
     */
    public function addStudentCertification(Request $request){
    	/* Fetching request params */
        $cetificationData = array();
        $cetificationData = $request->certification;
    	$request = $request->only('student_id');
    	
    	/* Checking for required parameters */
    	$required_parameter = array('student_id');
    	$chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student */
        $checkStudent = Students::where(array('student_user_id' => $request['student_id']))->first();
        if(empty($checkStudent)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo non esistente', 'error' => 'STUDENT_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        if(!empty($cetificationData)) {
            foreach($cetificationData['name'] as $key => $val) {
                if(!empty($cetificationData['id'][$key]) && Certification::where(array('id' => $cetificationData['id'][$key], 'student_id' => $request['student_id']))->count() > 0) {
                    $id = $cetificationData['id'][$key];
                    $updateData = array(
                        'name' => $cetificationData['name'][$key],
                        'date' => date($this->dateForDb, strtotime($cetificationData['date'][$key])),
                        'vote' => $cetificationData['vote'][$key]
                    );
                    Certification::where(array('id' => $id, 'student_id' => $request['student_id']))->update($updateData);
                } else {
                    $certification = new Certification();
                    $certification->student_id = $request['student_id'];
                    $certification->name = $cetificationData['name'][$key];
                    $certification->date = date($this->dateForDb, strtotime($cetificationData['date'][$key]));
                    $certification->vote = $cetificationData['vote'][$key];
                    $certification->save();
                    unset($certification);
                }
            }
            $resp = array('code' => $this->successParam, 'message' => 'Student work experience updated successfully.');
        } else {
        	$resp = array('code' => $this->errorParam, 'message' => 'Unable to add student work experience.', 'error' => 'UNABLE_TO_ADD_STUDENT_WORK_EXPERIENCE');
        }

    	return $this->response->array($resp);
    }
    
        /**
     * Get student work experiences
     * @param Request $request
     * @return $resp
     */
    public function getStudentCertification(Request $request){
        /* Fetching request params */
        $request = $request->only('student_id');
        
        /* Checking for required parameters */
        $required_parameter = array('student_id');
        $chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student */
        $checkStudent = Students::where(array('student_user_id' => $request['student_id']))->first();
        if(empty($checkStudent)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo non esistente', 'error' => 'STUDENT_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $certData = array();
        $certData = Certification::where(array('student_id' => $request['student_id']))->get();
        if(count($certData) > 0) {
            $resp = array('code' => $this->successParam, 'response' => array('experience_data' => $certData));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'No student certification.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }
    
        /**
     * Delete student work experience
     * @param Request $request
     * @return $resp
     */
    public function deleteStudentCertification(Request $request){
        /* Fetching request param */
        $request = $request->only('id');

        /* Checking for required parameters */
        $required_parameter = array('id');
        $chk_error = check_required_value($required_parameter, $request);
        if($chk_error){
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter '.$chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid student school record */
        $checkRecord = Certification::where('id', $request['id'])->first();
        if(empty($checkRecord)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Student certification not exists.', 'error' => 'RECORD_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        Certification::where('id', $request['id'])->delete();
        $resp = array('code' => $this->successParam, 'message' => 'Student certification deleted successfully.');
        return $this->response->array($resp);
    }
    
    public function updateModuleBStudent(Request $request) {
        /* Fetching request params */
        $request = $request->only('student_id', "internship_request_id", "status", 'ls_1_a', 'ls_1_b', 'ls_1_c', 'ls_1_d', 'ls_1_e', 'ls_1_f', 'ls_1_g');
        /* Checking for required parameters */
        $required_parameter = array('student_id', "internship_request_id", "status");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        $data = ModuleBStudent::where(array('internship_request_id' => $request['internship_request_id']))->update($request);
        $resp = array('code' => $this->successParam, 'message' => 'Modifiche effettuate correttamente.', 'response' => array('data' => $data));
        return $this->response->array($resp);
    }
    
    public function updateModuleFStudent(Request $request) {
        /* Fetching request params */
        $request = $request->only('student_id', "internship_request_id", "status", 
                'module_student_f_1', 'module_student_f_2', 'module_student_f_3', 'module_student_f_4', 'module_student_f_5', 'module_student_f_6', 'module_student_f_7',  
                'module_student_f_8', 'module_student_f_9', 'module_student_f_10', 'module_student_f_11', 'module_student_f_12_a', 'module_student_f_12_b', 'module_student_f_12_c',
                'module_student_f_12_d', 'module_student_f_12_e', 'module_student_f_12_f', 'module_student_f_12_g', 'module_student_f_12_j', 'module_student_f_12_k', 'module_student_f_12_l', 
                'module_student_f_12_m', 'module_student_f_12_n', 'module_student_f_12_o', 'module_student_f_12_p', 'module_student_f_13_a', 'module_student_f_13_b', 'module_student_f_13_c',
                'module_student_f_13_1_a', 'module_student_f_13_1_b', 'module_student_f_13_1_c', 'module_student_f_13_2', 'module_student_g_1_a', 'module_student_g_1_b', 'module_student_g_1_c',
                'module_student_g_1_d');
        /* Checking for required parameters */
        $required_parameter = array('student_id', "internship_request_id", "status");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        $data = ModuleFStudent::where(array('internship_request_id' => $request['internship_request_id']))->update($request);
        $resp = array('code' => $this->successParam, 'message' => 'Modifiche effettuate correttamente.', 'response' => array('data' => $data));
        return $this->response->array($resp);
    }
    
    public function getModuleBStudent(Request $request) {
        /* Fetching request params */
        $request = $request->only('student_id', "offset", "limit", "status", "internship_request_id");

        /* Checking for required parameters */
        $required_parameter = array('student_id', "offset", "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $moduleBStudent = new ModuleBStudent;
        /* Check for already apply */
        $data = $moduleBStudent->getModuleBStudent($request);

//        $count  = $internship->countIntershipByCompany($request);
        $data = Image::getImageProfile("Company", $data);
        $data = Image::getImageProfile("School", $data);

        if (count($data) > 0) {
            $next = (count($data) >= $request["limit"]) ? TRUE : FALSE;

            $resp = array('code' => $this->successParam, 'response' => array('data' => $data, "has_next" => $next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessun risultato trovato.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
        
    }
    
    public function getModuleFStudent(Request $request) {
        /* Fetching request params */
        $request = $request->only('student_id', "offset", "limit", "status", "internship_request_id");

        /* Checking for required parameters */
        $required_parameter = array('student_id', "offset", "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $moduleFStudent = new ModuleFStudent;
        /* Check for already apply */
        $data = $moduleFStudent->getModuleFStudent($request);

//        $count  = $internship->countIntershipByCompany($request);
        $data = Image::getImageProfile("Company", $data);
        $data = Image::getImageProfile("School", $data);

        if (count($data) > 0) {
            $next = (count($data) >= $request["limit"]) ? TRUE : FALSE;

            $resp = array('code' => $this->successParam, 'response' => array('data' => $data, "has_next" => $next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessun risultato trovato.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
        
    }
    


} 
