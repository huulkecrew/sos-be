<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;

class UserContactsController extends BaseController
{   
    
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    /**
     * @api {post} /api/v1/contactus ContactUs
     * @apiName ContactUs
     * @apiGroup User
     * @apiDescription Add request to your warehouse
     * @apiPermission Customer
     * 
     * @apiParam {String} contact[fullname] Fullname is required.
     * @apiParam {String} contact[email] Email is is required.
     * @apiParam {String} contact[phone] Phone is optional.
     * @apiParam {String} contact[title] Subject/Title is required.
     * @apiParam {String} contact[description] Description is required.
     * @apiParam {String} contact[zipcode] Your valid zipcode required.
     * 
     * @apiError {Object[]} FullnameRequired The <code>fullname</code> is required.
     * @apiError {Object[]} FullnameInvalid  The <code>fullname</code> is invalid.
     * @apiError {Object[]} EmailRequired The <code>email</code> is required.
     * @apiError {Object[]} EmailInvalid  The <code>email</code> is invalid.
     * @apiError {Object[]} TitleRequired The <code>title</code> is required.
     * @apiError {Object[]} TitleInvalid  The <code>title</code> is invalid.
     * @apiError {Object[]} DescriptionRequired The <code>description</code> is required.
     * @apiError {Object[]} DescriptionInvalid  The <code>description</code> is invalid.
     * @apiError {Object[]} ZipcodeRequired The <code>zipcode</code> is required.
     * @apiError {Object[]} ZipcodeInvalid  The <code>zipcode</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": "Your message sent successfully.",
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     *
     * @apiSuccess (200) {Object[]} Success Special Product created in the store.
     * @apiError (500) {Object[]} ContactDetails can not be sent. 
     */
    public function add(Request $request) {
        $authUser   =   Auth::user();
        $userId     =   $authUser->id;
        $messages = [
            'contact.fullname.required'     =>  trans('user.fullname_required'),
            'contact.email.required'        =>  trans('user.email_required'),
            'contact.title.required'        =>  trans('user.contact_subject_required'),
            'contact.description.required'  =>  trans('user.contact_description_required'),
            'contact.zipcode.required'      =>  trans('user.zipcode_required'),
            'contact.zipcode.digits'        =>  trans('user.zipcode_invalid'),
            'contact.zipcode.exists'        =>  trans('user.zipcode_invalid')
        ];
        $rules = [
            'contact.fullname'         =>  'required|max:128',
            'contact.email'            =>  'required|max:96',
            'contact.phone'            =>  'phone:US',
            'contact.title'            =>  'required|max:225',
            'contact.description'      =>  'required',
            'contact.zipcode'          =>  'required|digits:5|exists:zipcode,zipcode'
        ];
        
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $requestData   = $request->only('contact')['contact'];
        $zipcodeInfo   = \App\Models\Zipcode::getItem($requestData['zipcode']);
        $contact       = new \App\Models\UserContact();
        $contact->fill($requestData);
        $contact->user_id = $userId;
        $contact->city    = $zipcodeInfo->city->name;
        $contact->state   = $zipcodeInfo->city->state->name;
        $contact->country = $zipcodeInfo->city->state->country->name;
        $contact->status  = config('constant.user_contact_us')['open'];
        if( !$contact->save() ) {
            return $this->response->errorInternalError(trans ('address.something_went_wrong'));
        }
        return $this->response->withArray(['message'=>  trans('user.contact_query_sent_successfully'), 'meta'=>['http_code'=>200]]);
    }
    
}