<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;
use App\Serializer\ArrayIncludeSerializer;

class HomeController extends BaseController
{   
    
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    /**
     * @api {get} /api/v1/home/addresses Address List
     * @apiName GetCustomerAddresses
     * @apiGroup Address
     * @apiDescription Get Customer's Address List
     * @apiPermission Customer
     * 
     * 
     * @apiSuccess (200) {Object[]} Success The address list, Empty list if no address .
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": [
     *     {
     *      "id": 11,
            "user_id": "74",
            "address_type": "R",
            "label": "Home",
            "company": "",
            "address_1": "11760 Freguson Road",
            "address_2": "",
            "city": "Dallas",
            "state_id": "41",
            "state": "TX",
            "zipcode": "75228",
            "country_id": "223",
            "country": "US",
            "lat": "32.85",
            "lng": "-96.65",
            "instructions": "",
            "created_at": "2016-06-03 06:46:57",
            "updated_at": "2016-06-03 06:46:57"
     *     }
     *     ...
     *   ],
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     */
    public function getShowcaseFeedback() {
       $feedbacks = \App\Models\OrderFeedback::getShowcaseFeedback();
       $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
       return $this->response->withCollection($feedbacks, \App\Transformer\OrderFeedbackTransformer::transformWithShowcaseContext(), 'data', null, ['http_code'=>200]);
    }
    
}
