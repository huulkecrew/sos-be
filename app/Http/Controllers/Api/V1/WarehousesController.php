<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;


class WarehousesController extends BaseController
{   
    protected $model;
    
    public function __construct(Response $response) {
        $this->model    = new \App\Models\Warehouse();
        parent::__construct($response);
    }
    
    /**
     * @api {get} /api/v1/warehouses/services/type Check Pickup/Delivery Services
     * @apiName CheckWarehousesServiceTypes
     * @apiGroup Warehouse
     * @apiDescription To Check either Pickup/Delivery services are avaialable on the zonal warehouses or not
     * @apiPermission Customer
     * 
     * @apiParam {Interger} zone_id Zone ID is required.
     * @apiParam {String="x,x,x,x,x"} warehouse_ids List of warehouse IDs separedted by commas like(1,2,3,4,5).
     * @apiParam {String="pickup","delivery"} service_type Service type is required 
     * 
     * @apiError {Object[]} ZoneIdRequired The <code>zone_id</code> is required.
     * @apiError {Object[]} ZoneIdInvalid  The <code>zone_id</code> is invalid.
     * @apiError {Object[]} WarehouseIdListRequired The <code>warehouse_ids</code> is required.
     * 
     * * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": {
     *      "delivery": true,  =>If the requested service type available on the zonal warehouses
     *      "pickup": false    =>If the requested service type not available on the zonal warehouses
     *    }
     * }
     * 
     * @apiSuccess (200) {Object[]} Success Requested Service either available or not.
     * @apiError (404) {Object[]} Services can not be retrieved on these warehouses.
     */
    public function getServicesOnWarehouse(Request $request) {
        $messages       =   [
            'zone_id.required'          =>  trans('warehouse.zone_id_required'),
            'zone_id.exists'            =>  trans('warehouse.zone_id_invalid'),
            'warehouse_ids.required'    =>  trans('warehouse.warehouse_ids_required')
        ];
        $rules      =   [
            'zone_id'           =>  'required|exists:zone,id',
            'warehouse_ids'     =>  'required'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $zoneId                 =   $request->input('zone_id');
        $warehouseIds           =   explode(',', $request->input('warehouse_ids'));
        $serviceTypes           =   \App\Models\WarehouseServiceOption::all();
        $warehouses             =   \App\Models\WarehouseZonalService::getWarehouseZonalService($zoneId, $warehouseIds);        
        if( $warehouses ) {
            $response           =   [];
            foreach ($serviceTypes as $service) {
                $response[$service->value]  = $this->isServiceAvailable($warehouses, $service->id);
            }
            return $this->response->withArray(['data'=> $response], ['http_code'=>200]);
        } else {
            return $this->response->errorNotFound(trans('warehouse.warehouse_service_not_available'));
        }
        
    }
    
    /**
     * @api {post} /api/v1/warehouses/services/schedules/prices Get Service Schedule And Price
     * @apiName ServiceSchedueAndPrices
     * @apiGroup Warehouse
     * @apiDescription Get All Pickup/Delivery services with their prices, Either are avaialable on the zonal warehouses or not
     * @apiPermission Customer
     * 
     * @apiParam {Interger} service_option_id Service Option ID is required.
     * @apiParam {String="x,x,x,x,x"} warehouse_ids List of warehouse IDs separedted by commas like(1,2,3,4,5).
     * @apiParam {String} zipcode Zipcode is required.
     * @apiParam {String} zone_id is required.
     * 
     * @apiError {Object[]} ServiceOptionIdRequired The <code>service_option_id</code> is required.
     * @apiError {Object[]} WarehouseIdsRequired  The <code>warehouse_ids</code> is invalid.
     * @apiError {Object[]} ZipcodeRequired The <code>zipcode</code> is required.
     * @apiError {Object[]} ZipcodeInvalid The <code>zipcode</code> is invalid.
     * 
     * * @apiSuccessExample {json} Success-Response:
     * {
  "data": [
    {
      "1": [
        {
          "service_options": {
            "day": "Sunday",
            "display": "July 3",
            "lable": "Today",
            "warehouse_ids": "1-2",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Sunday",
                "summary": "Sunday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Sunday",
                "summary": "Sunday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Monday",
            "display": "July 3",
            "lable": "Tomorrow",
            "warehouse_ids": "1-2",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Monday",
                "summary": "Monday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Monday",
                "summary": "Monday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Monday",
                "summary": "Monday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Monday",
                "summary": "Monday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Monday",
                "summary": "Monday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Monday",
                "summary": "Monday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Monday",
                "summary": "Monday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Monday",
                "summary": "Monday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Monday",
                "summary": "Monday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Monday",
                "summary": "Monday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Monday",
                "summary": "Monday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Monday",
                "summary": "Monday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Monday",
                "summary": "Monday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Tuesday",
            "display": "July 3",
            "lable": "Tuesday",
            "warehouse_ids": "1-2",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Tuesday",
                "summary": "Tuesday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Tuesday",
                "summary": "Tuesday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Wednesday",
            "display": "July 3",
            "lable": "Wednesday",
            "warehouse_ids": "1-2",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Wednesday",
                "summary": "Wednesday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Wednesday",
                "summary": "Wednesday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Thursday",
            "display": "July 3",
            "lable": "Thursday",
            "warehouse_ids": "1-2",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Thursday",
                "summary": "Thursday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Thursday",
                "summary": "Thursday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Friday",
            "display": "July 3",
            "lable": "Friday",
            "warehouse_ids": "1-2",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Friday",
                "summary": "Friday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Friday",
                "summary": "Friday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Friday",
                "summary": "Friday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Friday",
                "summary": "Friday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Friday",
                "summary": "Friday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Friday",
                "summary": "Friday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Friday",
                "summary": "Friday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Friday",
                "summary": "Friday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Friday",
                "summary": "Friday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Friday",
                "summary": "Friday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Friday",
                "summary": "Friday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Friday",
                "summary": "Friday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Friday",
                "summary": "Friday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Saturday",
            "display": "July 3",
            "lable": "Saturday",
            "warehouse_ids": "1-2",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Saturday",
                "summary": "Saturday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Saturday",
                "summary": "Saturday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        }
      ]
    },
    {
      "2": [
        {
          "service_options": {
            "day": "Sunday",
            "display": "July 3",
            "lable": "Today",
            "warehouse_ids": "4",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Sunday",
                "summary": "Sunday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Sunday",
                "summary": "Sunday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Sunday",
                "summary": "Sunday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Monday",
            "display": "July 3",
            "lable": "Tomorrow",
            "warehouse_ids": "4",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Monday",
                "summary": "Monday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Monday",
                "summary": "Monday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Monday",
                "summary": "Monday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Monday",
                "summary": "Monday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Monday",
                "summary": "Monday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Monday",
                "summary": "Monday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Monday",
                "summary": "Monday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Monday",
                "summary": "Monday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Monday",
                "summary": "Monday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Monday",
                "summary": "Monday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Monday",
                "summary": "Monday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Monday",
                "summary": "Monday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Monday",
                "summary": "Monday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Tuesday",
            "display": "July 3",
            "lable": "Tuesday",
            "warehouse_ids": "4",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Tuesday",
                "summary": "Tuesday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Tuesday",
                "summary": "Tuesday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Tuesday",
                "summary": "Tuesday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Wednesday",
            "display": "July 3",
            "lable": "Wednesday",
            "warehouse_ids": "4",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Wednesday",
                "summary": "Wednesday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Wednesday",
                "summary": "Wednesday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Wednesday",
                "summary": "Wednesday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Thursday",
            "display": "July 3",
            "lable": "Thursday",
            "warehouse_ids": "4",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Thursday",
                "summary": "Thursday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Thursday",
                "summary": "Thursday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Thursday",
                "summary": "Thursday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Friday",
            "display": "July 3",
            "lable": "Friday",
            "warehouse_ids": "4",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Friday",
                "summary": "Friday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Friday",
                "summary": "Friday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Friday",
                "summary": "Friday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Friday",
                "summary": "Friday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Friday",
                "summary": "Friday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Friday",
                "summary": "Friday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Friday",
                "summary": "Friday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Friday",
                "summary": "Friday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Friday",
                "summary": "Friday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Friday",
                "summary": "Friday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Friday",
                "summary": "Friday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Friday",
                "summary": "Friday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Friday",
                "summary": "Friday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        },
        {
          "service_options": {
            "day": "Saturday",
            "display": "July 3",
            "lable": "Saturday",
            "warehouse_ids": "4",
            "time_slots": [
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 AM - 10:00 AM",
                "heading": "Saturday",
                "summary": "Saturday, 9:00 AM - 10:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 AM",
                "window_ends_at": "10:00 AM",
                "surge_amount": "4.99",
                "window": "9:00 am - 10:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "10:00 AM - 11:00 AM",
                "heading": "Saturday",
                "summary": "Saturday, 10:00 AM - 11:00 AM",
                "modifier": " - First Delivery",
                "window_starts_at": "10:00 AM",
                "window_ends_at": "11:00 AM",
                "surge_amount": "4.99",
                "window": "10:00 am - 11:00 am",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "11:00 AM - 12:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 11:00 AM - 12:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "11:00 AM",
                "window_ends_at": "12:00 PM",
                "surge_amount": "4.99",
                "window": "11:00 am - 12:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "12:00 PM - 1:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 12:00 PM - 1:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "12:00 PM",
                "window_ends_at": "1:00 PM",
                "surge_amount": "4.99",
                "window": "12:00 pm - 1:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "1:00 PM - 2:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 1:00 PM - 2:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "1:00 PM",
                "window_ends_at": "2:00 PM",
                "surge_amount": "4.99",
                "window": "1:00 pm - 2:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "2:00 PM - 3:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 2:00 PM - 3:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "2:00 PM",
                "window_ends_at": "3:00 PM",
                "surge_amount": "4.99",
                "window": "2:00 pm - 3:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "3:00 PM - 4:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 3:00 PM - 4:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "3:00 PM",
                "window_ends_at": "4:00 PM",
                "surge_amount": "4.99",
                "window": "3:00 pm - 4:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "4:00 PM - 5:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 4:00 PM - 5:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "4:00 PM",
                "window_ends_at": "5:00 PM",
                "surge_amount": "4.99",
                "window": "4:00 pm - 5:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "5:00 PM - 6:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 5:00 PM - 6:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "5:00 PM",
                "window_ends_at": "6:00 PM",
                "surge_amount": "4.99",
                "window": "5:00 pm - 6:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "6:00 PM - 7:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 6:00 PM - 7:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "6:00 PM",
                "window_ends_at": "7:00 PM",
                "surge_amount": "4.99",
                "window": "6:00 pm - 7:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "7:00 PM - 8:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 7:00 PM - 8:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "7:00 PM",
                "window_ends_at": "8:00 PM",
                "surge_amount": "4.99",
                "window": "7:00 pm - 8:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "8:00 PM - 9:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 8:00 PM - 9:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "8:00 PM",
                "window_ends_at": "9:00 PM",
                "surge_amount": "4.99",
                "window": "8:00 pm - 9:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              },
              {
                "available": "1",
                "price": "4.99",
                "display_name": "9:00 PM - 10:00 PM",
                "heading": "Saturday",
                "summary": "Saturday, 9:00 PM - 10:00 PM",
                "modifier": " - First Delivery",
                "window_starts_at": "9:00 PM",
                "window_ends_at": "10:00 PM",
                "surge_amount": "4.99",
                "window": "9:00 pm - 10:00 pm",
                "display_price": "4.99",
                "section_heading": "Morning"
              }
            ]
          }
        }
      ]
    }
  ]
}
     * 
     * @apiSuccess (200) {Object[]} Success Requested Service either available or not with prices.
     * @apiError (404) {Object[]} Services can not be retrieved on these warehouses.
     */
    public function getServiceSchedulePrice(Request $request) {
        $messages       =   [
            'zipcode.required'              =>  trans('warehouse.zipcode_required'),
            'zipcode.exists'                =>  trans('warehouse.zipcode_invalid'),
            'zone_id.required'              =>  trans('warehouse.zone_id_required'),
            'zone_id.exists'                =>  trans('warehouse.zone_id_invalid'),
            'warehouse_ids.required'        =>  trans('warehouse.warehouse_ids_required'),
            'service_option_id.required'    =>  trans('warehouse.service_option_id_required'),
            'service_option_id.in'          =>  trans('warehouse.service_option_id_invalid'),
            'cart_id.required'              =>  trans('warehouse.cart_id_required')
        ];
        $rules      =   [
            'service_option_id'             =>  'required|exists:warehouse_service_option,id',
            'warehouse_ids'                 =>  'required|array',
            'zipcode'                       =>  'required|exists:zipcode,zipcode',
            'zone_id'                       =>  'required|exists:zone,id',
            'cart_id'                       =>  'required'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $zipcode            =   $request->input('zipcode');
        $warehouseIds       =   $request->input('warehouse_ids');
        $serviceOptionId    =   $request->input('service_option_id');
        $zoneId             =   $request->input('zone_id');
        $card_id            =   $request->input('cart_id');
        $servicePrices      =   $this->getServicePrice($zoneId, $warehouseIds, $serviceOptionId);
        $warehouses         =   $this->getWarehouses($warehouseIds);
        $groups             =   $this->getGroupWithWarehouses($warehouses);
        $services           =   \App\Models\WarehouseZonalServiceSchedule::getWarehouseZonalServiceShedule($zoneId, $warehouseIds, $serviceOptionId);
        $holidays           =   \App\Models\WarehouseZonalHoliday::where('zone_id', '=', $zoneId)->whereIn('warehouse_id', $warehouseIds)->get();
        $finalServices      =   $this->mergeHolidayAndSchedule($groups, $warehouses, $warehouseIds, $services, $holidays, $servicePrices);
        
        return $this->response->withArray(['data'=> $finalServices], ['http_code'=>200]);
    }
    
    private function getWarehouses($warehouseIds) {
        return \App\Models\Warehouse::whereIn('id', $warehouseIds)->get();
    }
    
    private function mergeHolidayAndSchedule($groups, $warehouses, $warehouseIds, $services, $holidays, $servicePrices) {
        $serviceShedule     =   $this->serviceSchedule($warehouseIds, $services);
        $servicePrice       =   $this->servicePrice($groups, $warehouseIds, $servicePrices);
        $serviceOptions = [];
        foreach ($groups as $groupId => $group) {
            $deliveryOptions = [];
            for($i=0; $i<7;++$i) {
                $today                  = \Carbon\Carbon::now()->addDays($i)->toDateTimeString();
                $todayDate              =   date('Y-m-d');
                $option = [];
                $option['day']          =   date('l', strtotime($today));
                $option['display']      =   date('F j');
                $option['lable']        =   ($i==0)?'Today':(($i==1)?'Tomorrow':date('l', strtotime($today)));;
                $option['warehouse_ids']= $warehouseKey = implode('-', $group);
                $option['time_slots'] = $this->getAllslots($option['day'], $todayDate, $groupId, $group, $serviceShedule, $servicePrice);
                array_push($deliveryOptions, array('service_options'=> $option));
            }
            array_push($serviceOptions, array($groupId => $deliveryOptions));
        }
        
        return $serviceOptions;
    }

    private function serviceSchedule($warehouseIds, $services) {
        $warehouse = [];
        foreach ($warehouseIds as $warehouseId) {
            $warehouse[$warehouseId] = [];
            foreach ($services as $service) {
                if($service->warehouse_id == $warehouseId) {
                    array_push($warehouse[$warehouseId], $service);
                }
            }
        }
        return $warehouse;
    }
    
    private function servicePrice($groups, $warehouseIds, $prices) {
        $warehouse = [];
        foreach ($warehouseIds as $warehouseId) {
            $warehouse[$warehouseId] = [];
            foreach($prices as $price) {
                if($price->warehouse_id == $warehouseId) {
                    array_push($warehouse[$warehouseId], $price);
                }
            }
        }
        $allGroup = [];
        foreach($groups as $groupId => $group) {
            $allGroup[$groupId] = [];
            foreach($group as $w) {
                array_push($allGroup[$groupId], $warehouse[$w]); 
            }
        }
            
        $priceInfo = [];
        foreach($groups as $groupId => $group) {
            $priceInfo[$groupId] = [];
            $warehouseKey = implode('-', $group);
            $dates = collect($warehouse[$group[0]])->pluck('service_date')->all();
            $dateInfo = [];
            foreach($dates as $date){
                $dateInfo[$date] = $this->groupPrices($date, $allGroup[$groupId]);
            }
            array_push($priceInfo[$groupId], [$warehouseKey=>$dateInfo]);
        }
        return $priceInfo;
    }
    
    private function groupPrices($date, $warehouses) {
        $all = [];
        foreach($warehouses as $warehouse) {
            foreach($warehouse as $item) {
                if($item->service_date == $date) {
                    $p = [];
                    $psurge = json_decode($item->pricing, true);
                    $p['pricing'] = $psurge['basic'][0];
                    $p['surge'] = $psurge['surge'][0];
                    $p['availability'] = json_decode($item->availability, true);
                    array_push($all, $p);
                }
            }
        }
        $len = count($all);
        $pricing = $all[0]['pricing'];
        $surge   = $all[0]['surge'];
        $availability = $all[0]['availability'];
        for($i=1; $i < $len; $i++) {
            $pricing = array_intersect($pricing, $all[$i]['pricing']);
            $surge = array_intersect($surge, $all[$i]['surge']);
            $availability = array_intersect($availability, $all[$i]['availability']);
            
        }
        return ['pricing'=>$pricing, 'surge'=>$surge, 'availability'=>$availability];
    }

    private function getGroupWithWarehouses($warehouses) {
        $groups = [];
        $allGroup = array_unique(collect($warehouses)->pluck('warehouse_group_id')->all());
        foreach ($allGroup as $group) {
            $groups[$group] = [];
            foreach ($warehouses as $warehouse) {
                if($warehouse->warehouse_group_id === $group) {
                    array_push($groups[$group], $warehouse->id);
                }
            }
        }
        return $groups;
    }
    
    private function getServicePrice($zoneId, $warehouseIds, $serviceOptionId) {
        return \App\Models\WarehouseZonalServicePrice::getWarehouseServicePrice($zoneId, $warehouseIds, $serviceOptionId);
    }

    private function getAllslots($weekDay, $todayDate, $groupId, $warehouses, $serviceShedule, $servicePrice) {
        $slots = [];
        $serviceStartHour = [];
        $serviceEndHour = [];
        $samedayTimeoutHour = [];
        $slotInterval = [];
        $slotTimeoutInterval = []; 
        $warehouseKey = implode('-', $warehouses);
        //prepare available slotes and 
        foreach($warehouses as $warehouse) {
            $services = $serviceShedule[$warehouse];
            foreach ($services as $service) {
                if($service->weekday_text === $weekDay) {
                    array_push($serviceStartHour, $service->service_start_hour);
                    array_push($serviceEndHour, $service->service_end_hour);
                    array_push($samedayTimeoutHour, $service->sameday_timeout_hour);
                    array_push($slotInterval, $service->slot_interval);
                    array_push($slotTimeoutInterval, $service->slot_timeout_interval);
                }
            }
        }
        
        $finalServiceStartHour      =   strtotime( max($serviceStartHour) );
        $finalServiceEndHour        =   strtotime( min($serviceEndHour) );
        $finalSlotInterval          =   max($slotInterval);
        $endHour                    =   (int)max($serviceStartHour) + (int)(max($slotInterval));
        while( $finalServiceStartHour < $finalServiceEndHour ) {
            $item = [];
            $last = $finalServiceStartHour+$finalSlotInterval*60*60;
            $interval                   =   date("g:i A", $finalServiceStartHour).' - '.date("g:i A", $last ); 
            $item['available']          =   $servicePrice[$groupId][0][$warehouseKey][$todayDate]['availability'][$endHour];
            $item['price']              =   $servicePrice[$groupId][0][$warehouseKey][$todayDate]['pricing'][$endHour];
            $item['display_name']       =   $interval;
            $item['heading']            =   $weekDay; 
            $item['summary']            =   $weekDay.', '.$interval;
            $item['modifier']           =   ' - First Delivery';//Need to discuss
            $item['window_starts_at']   =   date("g:i A", $finalServiceStartHour);
            $item['window_ends_at']     =   date("g:i A", $last );
            $item['surge_amount']       =   $servicePrice[$groupId][0][$warehouseKey][$todayDate]['surge'][$endHour];
            $item['window']             =   date("g:i a", $finalServiceStartHour).' - '.date("g:i a", $last );
            $item['display_price']      =   $servicePrice[$groupId][0][$warehouseKey][$todayDate]['pricing'][$endHour];
            $item['section_heading']    =   'Morning'; //TOODO
            $finalServiceStartHour      =   $last;
            array_push($slots, $item);
        }
        return $slots;
    }
    
    private function isServiceAvailable($warehouses, $id) {
        foreach ($warehouses as $warehouse) {
            if($warehouse->warehouse_service_option_id === $id) {
                return true;
            }
        }
        return false;
    }

    private function serviceTypeId($type) {
        return \App\Models\WarehouseServiceOption::select('id')->where('value', '=', $type)->first()->id;
    }
        
}
