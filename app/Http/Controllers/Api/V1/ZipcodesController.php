<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Validator;

class ZipcodesController extends BaseController
{
    
    protected $model;
    
    public function __construct(Response $response) {
        $this->model    = new \App\Models\Zipcode();
        parent::__construct($response);
    }
    
    /**
    * @api {get} /api/v1/address/display Zipcode Geo Info
    * @apiDescription Get Address in Zipcode
    * @apiPermission none
    * 
    * @apiGroup Address
    * 
    * @apiName  Address 
    *
    * @apiParam {Integer} zipcode Zipcode.
    * 
    * @apiError {Object[]} ZipcodeRequired The <code>zipcode</code> is required.
    * @apiError {Object[]} ZipcodeInvalid  The <code>zipcode</code> is invalid.
    * @apiError {Object[]} ZipcodeNotFound The <code>zipcode</code> was not found.
    * 
    * @apiSuccess {Integer} zipcode Zipcode.
    * @apiSuccess {Integer} zipcode zicode exist in the system.
    * @apiSuccess {String} city_id city ID to which zipcode belongs.
    * @apiSuccess {String} city_name city name to which zipcode belongs.
    * @apiSuccess {String} city_slug city slug name to which zipcode belongs.
    * @apiSuccess {Integer} state_id state ID to which zipcode belongs.
    * @apiSuccess {String} state_name state name to which zipcode belongs.
    * @apiSuccess {Integer} state_code state code to which zipcode belongs.
    * @apiSuccess {String} country_name Country name to which address belongs.
    * @apiSuccess {String} country_code1 Country code1 which belongs to the country.
    * @apiSuccess {String} country_code2 Country code2 which belongs to the country.
    *
    * @apiSuccessExample {json} Success-Response:
    * {
    *   "data": {
    *       "zipcode": 78705,
    *       "zone_id": 1,
    *       "city_id": 4,
    *       "city_name": "Austin",
    *       "city_slug": "austin-texas",
    *       "state_id": 41,
    *       "state_name": "Texas",
    *       "state_code": "TX",
    *       "country": "United States",
    *       "country_code1": "US",
    *       "country_code2": "USA",
    *       "service_zone_id": 0,
    *       "lat": "30.29619980",
    *       "lng": "-97.73899841",
    *       "timezone_offset": "-6",
    *       "service_status": 1,
    *       "status": 0
    *       }
    *   }
    * 
    * @apiSuccess (200) {Object[]} Success Address based on the zipcode.
    * @apiError (404) {Object[]} AddressNotFound The <code>address</code> of the zipcode was not found.
    * 
    * 
    */
    public function getAddress(Request $request) {
        $messages = [
            'zipcode.required'      => trans('zipcode.zipcode_required'),
            'zipcode.digits'        => trans('zipcode.zipcode_invalid'),
            'zipcode.exists'        => trans('zipcode.zipcode_invalid')
        ];
        
        $validator = Validator::make($request->all(), [
            'zipcode' => 'required|digits:5|exists:zipcode,zipcode'
        ], $messages);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $address = \App\Models\Zipcode::getItem($request->input('zipcode'));   

        if( $address ) {
            return $this->response->withArray(['data'=> \App\Transformer\ZipcodeTransformer::addressBasedZipcode($address)], ['http_code'=>200]);
        } else {
            return $this->response->errorNotFound(trans('zipcode.address_not_found'));
        }
    }
    
    
    
}
