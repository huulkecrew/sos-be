<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Serializer\ArrayIncludeSerializer;
use App\Helpers\CustomerHelper;


class ShoppingListsController extends BaseController
{   
    protected $model;
    
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    /**
     * @api {get} /api/v1/customer/shopping/list Get All
     * @apiName GetCustomerShoppingLists
     * @apiGroup Shopping List
     * @apiDescription Get Customer's Shopping List
     * @apiPermission Customer
     * 
     * 
     * @apiSuccess (200) {Object[]} Success The Shopping list, Empty list if no shopping list .
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": [
     *     {
     *      "id": 1,
     *      "user_id": 83,
     *      "name": "Demo test",
     *      "slug": "demo-test",
     *      "access_type": "private",
     *      "created_at": "2016-06-03 02:26:46",
     *      "updated_at": "2016-06-03 02:26:46"
     *    }
     *     ...
     *   ],
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     */
    public function get() {
        $authUser       =   Auth::user();
        $userId         =   $authUser->id;
        $shoppingList   =   \App\Models\ShoppingList::getUserShoppingList($userId);
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($shoppingList, new \App\Transformer\ShoppingListTransformer(), 'data', null, ['http_code'=>200]);   
    }
    
    /**
     * @api {post} /api/v1/customer/shopping/list Add
     * @apiName AddCustomerShoppingList
     * @apiGroup Shopping List
     * @apiDescription Add Customer's Shopping List
     * @apiPermission Customer
     * 
     * @apiParam {String="2...128"}  shoppinglist[name] Shopping List name is required.
     * 
     * @apiError {Object[]} ShoppingListNameRequired The <code>name</code> is required.
     * @apiError {Object[]} ShoppingListNameInvalid The <code>name</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": {
     *     "id": 7,
     *     "user_id": 83,
     *     "name": "Meat and Seafood",
     *     "slug": "meat-and-seafood",
     *     "access_type": "private",
     *     "created_at": "2016-06-04 05:49:18",
     *     "updated_at": "2016-06-04 05:49:18"
     *   },
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     * @apiSuccess (200) {Object} Success Shopping List has been added successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function add(Request $request) {
        $messages = [
            'shoppinglist.name.required'    => trans('shoppinglist.shopping_list_name_required')
        ];
        $rules = [
            'shoppinglist.name'             =>    'required|min:2,max:128'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }

        $authUser       =   Auth::user();
        $userId         =   $authUser->id;
        $requestData    =   $request->only('shoppinglist')['shoppinglist'];
        $shoppingList   =   new \App\Models\ShoppingList();
        $shoppingList->fill($requestData);
        $shoppingList->user_id      =   $userId;
        $shoppingList->slug         =   CustomerHelper::convertNameToSlug($requestData['name']);
        $shoppingList->access_type  =   config('constant.shopping_list_access_type')['private'];

        if( !$shoppingList->save() ) {
            return $this->response->errorInternalError(trans ('shoppinglist.shoppinglist_can_not_be_created'));
        }
        
        return $this->response->withItem($shoppingList, new \App\Transformer\ShoppingListTransformer(), 'data', ['http_code'=>200]);    
    }

    /**
     * @api {put} /api/v1/customer/shopping/list Update
     * @apiName UpdateCustomerShoppingList
     * @apiGroup Shopping List
     * @apiDescription Update Customer's Shopping List
     * @apiPermission Customer
     * 
     * @apiParam {Integer}  shoppinglist[id] Shopping List id is required.
     * @apiParam {String="2...128"}  shoppinglist[name] Shopping List name is required.
     * 
     * @apiError {Object[]} ShoppingListIdRequired The <code>id</code> is required.
     * @apiError {Object[]} ShoppingListIdRequired The <code>id</code> is required.
     * @apiError {Object[]} ShoppingListNameRequired The <code>name</code> is required.
     * @apiError {Object[]} ShoppingListNameInvalid The <code>name</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": {
     *     "id": 7,
     *     "user_id": 83,
     *     "name": "Update Meat and Seafood",
     *     "slug": "update-meat-and-seafood",
     *     "access_type": "private",
     *     "created_at": "2016-06-04 05:49:18",
     *     "updated_at": "2016-06-04 05:58:44"
     *   },
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     * @apiSuccess (200) {Object} Success Shopping List has been updated successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function update(Request $request) {
        $authUser               =   Auth::user();
        $userId                 =   $authUser->id;
        $messages               =   [
            'shoppinglist.id.required'      => trans('shoppinglist.shopping_list_id_required'),
            'shoppinglist.id.exists'        => trans('shoppinglist.shopping_list_id_invalid'),
            'shoppinglist.name'             => trans('shoppinglist.shopping_list_name_required')
        ];
        $rules              =   [
            'shoppinglist.id'       =>  'required|exists:customer_shopping_list,id,user_id,'.$userId,
            'shoppinglist.name'     =>  'required|min:2,max:128'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $requestData    =   $request->only('shoppinglist')['shoppinglist'];
        $shoppingList   =   \App\Models\ShoppingList::where('id', '=', $requestData['id'])
                ->where('user_id', '=', $userId)->first();
        $shoppingList->name         =   $requestData['name'];
        $shoppingList->slug         =   CustomerHelper::convertNameToSlug($requestData['name']);
        $shoppingList->updated_at   = \Carbon\Carbon::now();
        if( !$shoppingList->save() ) {
            return $this->response->errorInternalError(trans('shoppinglist.shopping_list_can_not_be_updated'));   
        }
        
        return $this->response->withItem($shoppingList, new \App\Transformer\ShoppingListTransformer(), 'data', ['http_code'=>200]);
    }

    /**
     * @api {delete} /api/v1/customer/shopping/list Delete
     * @apiName Delete Shopping List
     * @apiGroup Shopping List
     * @apiDescription Delete Shopping List
     * @apiPermission Customer
     * 
     * @apiParam {Interger} id Shopping List ID is required.
     * 
     * @apiError {Object} ShoppingListIdRequired The <code>id</code> is required.
     * @apiError {Object} ShoppingListIdInvalid The <code>id</code> is Invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     *  {
     *      "message": "Your shopping list has been deleted successfully"
     *  }
     * 
     * @apiSuccess (200) {Object} The address has been deleted successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} AddressNotDeleted The address can not be deleted.
     */
    public function delete(Request $request) {
        $authUser               =   Auth::user();
        $userId                 =   $authUser->id;
        $messages = [
            'id.required'       => trans('shoppinglist.shopping_list_id_required'),
            'id.exists'         => trans('shoppinglist.shopping_list_id_invalid')
        ];
        $rules = [
            'id'                => 'required|exists:customer_shopping_list,id,user_id,'.$userId
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $shoppingList = \App\Models\ShoppingList::where('id', '=', $request->input('id'))
                ->where('user_id', '=', $userId)->first();

        if( !$shoppingList->delete() ) {
            return $this->response->errorInternalError(trans('shoppinglist.shopping_list_can_not_be_deleted'));   
        }
        
        return $this->response->withArray(['message'=>  trans('shoppinglist.shopping_list_deleted_successfully')], ['http_code'=>200]);
    }
    
    //TODO
    public function shoppingListProduct(Request $request) {
        $authUser               =   Auth::user();
        $userId                 =   $authUser->id;
        $messages = [
            'id.required'       => trans('shoppinglist.shopping_list_id_required'),
            'id.exists'         => trans('shoppinglist.shopping_list_id_invalid')
        ];
        $rules = [
            'id'                => 'required|exists:customer_shopping_list,id,user_id,'.$userId
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $products = \App\Models\ShoppingList::getProductList($userId, $request->input('id'));
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($products, new \App\Transformer\ShoppingListTransformer(), 'data', null, ['http_code'=>200]);   
    }

    /**
     * @api {post} /api/v1/customer/shopping/list/product Add Product
     * @apiName AddProductInList
     * @apiGroup Shopping List
     * @apiDescription Add Product in Shopping List
     * @apiPermission Customer
     * 
     * @apiParam {Interger}  listproduct[shopping_list_id] Shopping List ID is required.
     * @apiParam {Interger}  listproduct[zonal_product_id] Product ID is required.
     * 
     * @apiError {Object[]} ShoppingListIdRequired The <code>id</code> is required.
     * @apiError {Object[]} ShoppingListIdInvalid The <code>id</code> is invalid.
     * @apiError {Object[]} ProductIdRequired The <code>zonal_product_id</code> is required.
     * @apiError {Object[]} ProductIdInvalid The <code>zonal_product_id</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": true,
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     * @apiSuccess (200) {Object} Success Product added in the List has been added successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function addProduct(Request $request) {
        
        $authUser       =   Auth::user();
        $userId         =   $authUser->id;
        $messages = [
            'listproduct.shopping_list_id.required'      => trans('shoppinglist.shoppinglist_id_required'),
            'listproduct.shopping_list_id.integer'       => trans('shoppinglist.shoppinglist_id_invalid'),
            'listproduct.shopping_list_id.exists'        => trans('shoppinglist.shoppinglist_id_invalid'),
            'listproduct.zonal_product_id.required'      => trans('shoppinglist.zonal_product_id_required'),
            'listproduct.zonal_product_id.integer'       => trans('shoppinglist.zonal_product_id_invalid'),
            'listproduct.zonal_product_id.exists'        => trans('shoppinglist.zonal_product_id_invalid')
        ];
        $rules = [
            'listproduct.shopping_list_id'      =>   'required|integer|exists:customer_shopping_list,id,user_id,'.$userId,
            'listproduct.zonal_product_id'      =>   'required|integer|exists:zonal_product,id,unlisted,0'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        //Get Post data
        $requestData                     = $request->only('listproduct')['listproduct'];
        $shoppingListProduct             = \App\Models\ShoppingListProduct::firstOrNew(['shopping_list_id' => $requestData['shopping_list_id'], 'zonal_product_id' => $requestData['zonal_product_id']]);
        $shoppingListProduct->status     = 0;
        $shoppingListProduct->deleted_at = NULL;
        try {
            $shoppingListProduct->save();
            return $this->response->withArray(['data'=> true], ['http_code'=>200]);
        } catch (Exception $e) {
            return $this->response->errorInternalError(trans ('shoppinglist.product_can_not_be_added_in_list'));
        } 
    }
    
    /**
     * @api {delete} /api/v1/customer/shopping/list/product Remove Product
     * @apiName Remove Product From List
     * @apiGroup Shopping List
     * @apiDescription Remove Product from List
     * @apiPermission Customer
     * 
     * @apiParam {Interger} id Shopping List ID is required.
     * 
     * @apiError {Object} ShoppingListIdRequired The <code>shopping_list_id</code> is Reuired.
     * @apiError {Object} ShoppingListIdInvalid The <code>shopping_list_id</code> is Invalid.
     * @apiError {Object} idRequired The <code>id</code> is required.
     * @apiError {Object} idInvalid The <code>id</code> is Invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     *  {
     *      "message": "Your product from shopping list has been deleted successfully"
     *  }
     * 
     * @apiSuccess (200) {Object} The product from list has been deleted successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} ProductNotRemoved The product can not be remove.
     */
    public function removeProduct(Request $request) {
        $authUser       =   Auth::user();
        $userId         =   $authUser->id;

        $messages = [
            'listproduct.shopping_list_id.required'      => trans('shoppinglist.shoppinglist_id_required'),
            'listproduct.shopping_list_id.integer'       => trans('shoppinglist.shoppinglist_id_invalid'),
            'listproduct.shopping_list_id.exists'        => trans('shoppinglist.shoppinglist_id_invalid'),
            'listproduct.zonal_product_id.required'      => trans('shoppinglist.zonal_product_id_required'),
            'listproduct.zonal_product_id.integer'       => trans('shoppinglist.zonal_product_id_invalid')
        ];
        $rules = [
            'listproduct.shopping_list_id'      =>   'required|integer|exists:customer_shopping_list,id,user_id,'.$userId,
            'listproduct.zonal_product_id'      =>   'required|integer'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        try {
            \App\Models\ShoppingListProduct::where('shopping_list_id', '=', $request->input('shopping_list_id'))
                ->where('zonal_product_id', '=', $request->input('zonal_product_id'))->delete();
            return $this->response->withArray(['message'=>  trans('shoppinglist.shopping_list_product_deleted_successfully')], ['http_code'=>200]);
        } catch (Exception $ex) {
            return $this->response->errorInternalError(trans('shoppinglist.shopping_list_product_can_not_be_deleted'));   
        }
    }

    /**
     * @api {post} /api/v1/customers/product/favorite Favorite Mark Product
     * @apiName FavoriteMarkProduct
     * @apiGroup Shopping List
     * @apiDescription Favorite Mark Product in Shopping List
     * @apiPermission Customer
     * 
     * @apiParam {Interger}  product_id Product ID is required.
     * 
     * @apiError {Object[]} ProductIdRequired The <code>product_id</code> is required.
     * @apiError {Object[]} ProductIdInvalid The <code>product_id</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": true,
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     * @apiSuccess (200) {Object} Success Product added in the List has been added successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function markProductToFavorite(Request $request) {
        $authUser       =   Auth::user();
        $userId         =   $authUser->id;

        $messages = [
            'zonal_product_id.required'            => trans('shoppinglist.zonal_product_id_required'),
            'zonal_product_id.integer'             => trans('shoppinglist.zonal_product_id_invalid'),
            'zonal_product_id.exists'              => trans('shoppinglist.zonal_product_id_invalid')
        ];
        $rules = [
            'zonal_product_id'            =>   'required|integer|exists:zonal_product,id,unlisted,0'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        try {
            $zonalProductId           = $request->input('zonal_product_id');
            $zonalProduct             = \App\Models\ZonalProduct::find($zonalProductId);
            $zonalProducts            = \App\Models\ZonalProduct::where('product_id', '=', $zonalProduct->product_id)
                                        ->where('unlisted', '=', 0)->get();
            $shoppingList = \App\Models\ShoppingList::firstOrNew([
                'user_id'       =>  $userId,
                'type'          =>  2
            ]);
            if( !$shoppingList->name ) {
                $shoppingList->name     =   'Favorite';
                $shoppingList->slug     =   CustomerHelper::convertNameToSlug($shoppingList->name);
                $shoppingList->save();                
            }
            $zonalProductIds = collect($zonalProducts)->pluck('id')->all();
            $shoppingList->zonalProducts()->detach($zonalProductIds)->attach($zonalProductIds);
            return $this->response->withArray(['data'=> true], ['http_code'=>200]);
        } catch (Exception $e) {
            return $this->response->errorInternalError(trans ('shoppinglist.product_can_not_be_mark_favorite'));
        }
    }
    
    /**
     * @api {post} /api/v1/customer/shopping/list/product/favorite Unfavorite Mark Product
     * @apiName UnfavoriteMarkProduct
     * @apiGroup Shopping List
     * @apiDescription Favorite Mark Product in Shopping List
     * @apiPermission Customer
     * 
     * @apiParam {Interger}  product_id Product ID is required.
     * 
     * @apiError {Object[]} ProductIdRequired The <code>zonal_product_id</code> is required.
     * @apiError {Object[]} ProductIdInvalid The <code>zonal_product_id</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *    "message": "Product mark as favorite done successfully"
     * }
     * 
     * @apiSuccess (200) {Object} Success Product added in the List has been added successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function unMarkProductToFavorite(Request $request) {
        $authUser       =   Auth::user();
        $userId         =   $authUser->id;

        $messages = [
            'zonal_product_id.required'            => trans('shoppinglist.zonal_product_id_required'),
            'zonal_product_id.integer'             => trans('shoppinglist.zonal_product_id_invalid'),
            'zonal_product_id.exists'              => trans('shoppinglist.zonal_product_id_invalid')
        ];
        $rules = [
            'zonal_product_id'            =>   'required|integer|exists:zonal_product,id,unlisted,0'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        try {
            $zonalProductId      = $request->input('zonal_product_id');
            $shoppingList        = \App\Models\ShoppingList::where('user_id', '=', $userId)->where('type', '=', 2)->first();
            if($shoppingList){
                $zonalProduct             = \App\Models\ZonalProduct::find($zonalProductId);
                $zonalProducts            = \App\Models\ZonalProduct::where('product_id', '=', $zonalProduct->product_id)
                                            ->where('unlisted', '=', 0)->get();
                $zonalProductIds = collect($zonalProducts)->pluck('id')->all();
                $shoppingList->zonalProducts()->detach($zonalProductIds);
            } 
            return $this->response->withArray(['message'=> trans('shoppinglist.unmarked_favorite_done')], ['http_code'=>200]);
        } catch (Exception $ex) {
            return $this->response->errorInternalError(trans ('shoppinglist.product_can_not_be_mark_favorite'));
        }
    }
}
