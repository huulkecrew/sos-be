<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Traits\UserUtils;
use Illuminate\Support\Facades\Event;
use App\Jobs\SendInviteEmail;

class InvitesController extends BaseController
{   
    use UserUtils;
    protected $model;
    
    public function __construct(Response $response) {
        parent::__construct($response);
        
        //Validate multiple emails
        Validator::extend("emails", function($attribute, $value, $parameters) {
            $rules = [
                'email' => 'required|email',
            ];
            $emailString = trim($value, ',');
            $s = trim($emailString, ' ');
            $emails = explode(',', $s);
            try {
                foreach ($emails as $email) {
                    $data = [
                        'email' => $email
                    ];
                    $validator = Validator::make($data, $rules);
                    if ($validator->fails()) {
                        return false;
                    }
                }
            } catch (Exception $ex) {
                return false;
            }
            return true;
        });
    }
    
    /**
     * @api {post} /api/v1/invites Invite Friends
     * @apiName Invite Friends
     * @apiGroup Sharing
     * @apiDescription Invite Friends
     * @apiPermission Customer
     * 
     * @apiParam {String="x1@gmail.com,x2@gmail.com,x3@gmail.com,x4@burpy.com"} emails Email address separated by commas are required.
     * 
     * @apiError {Object[]} EmailAddressesRequired The <code>emails</code> is required.
     * @apiError {Object[]} EmailAddressesInvalid  The <code>emails</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": "Your invitation has been sent succcessfully.",
     *    "meta": {
     *         "http_code": 200
     *     }
     *   }
     * 
     * @apiSuccess (200) {Object} Success Invitation has been sent successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (431) {Object} Email can not be sent.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function invite(Request $request) {
        $authUser           =   Auth::user();
        $userId             =   $authUser->id;
        $messages = [
            'emails.required'     =>  trans('user.recipients_email_required'),
            'emails.emails'       =>  trans('user.recipients_email_invalid')
        ];
        $rules = [
            'emails'     =>  'required'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        $yesterday = \Carbon\Carbon::yesterday();
        $tomorrow = \Carbon\Carbon::tomorrow();
        $inviteLimit = \App\Models\ReferralInvite::where('user_id', '=', $userId)
                ->where('invited_at', '>', $yesterday)
                ->where('invited_at', '<', $tomorrow)
                ->count();
        $emailString = trim($request->input('emails'), ',');
        $emails = trim($emailString, ' ');
        $to = explode(',', $emails);
        $count = $inviteLimit + count($to);
        $validator->after(function($validator) use ($count, $inviteLimit) {
            if( $count > config('constant.invite_per_day_limit') ) {
                $remaining = config('constant.invite_per_day_limit')-$inviteLimit;
                $msg = ($remaining>0)?($remaining.trans('user.remaining_invitation')):'';
                $validator->errors()->add('invite.emails.invalid', $msg.trans('user.invite_exceeded'));
            }
        });
            
        if( $validator->fails() ) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $invites = [];
        $now    = \Carbon\Carbon::now();
        foreach ($to as $email) {
            array_push($invites, ['user_id'=>$userId, 'recipient_email'=>$email,'invited_at'=>$now]);
        }
        
        //Insert all invites to DB for this user
        \App\Models\ReferralInvite::insert($invites);
        //$sender          =   $this->getSenderDetails(100);
        $params = array(
            'userId'    =>  $userId,
            'to'        =>  $to,
            'subject'   =>  trans('mail.invite_subject'),
            'from'      =>  $authUser->email,//TODO, email will not be for social user
            'sender'    =>  $authUser->firstname.' '.$authUser->lastname);
        //Event::fire(new \App\Events\SendInviteMail($params));
        $this->dispatch(new SendInviteEmail($params));
        return $this->response->withArray(['message'=>  trans('user.invitation_sent_successfully'), 'meta'=>['http_code'=>200]]);        
    }

     /**
     * @api {post} /api/v1/messages Message To Friend
     * @apiName Message To Friend
     * @apiGroup Sharing
     * @apiDescription Message To Friend
     * @apiPermission Customer
     * 
     * @apiParam {String="+919015613680"}  number Mobile Number is required.
     * 
     * @apiError {Object[]} MobileNumberRequired The <code>number</code> is required.
     * @apiError {Object[]} MobileNumberInvalid  The <code>number</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": "Your message has been sent to your recipient successfully",
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     * @apiSuccess (200) {Object} Success Message has been sent successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (431) {Object} Message can not be sent.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function sendMessage(Request $request) {
        $authUser                =   Auth::user();
        $userId                  =   $authUser->id;
        $messages = [
            'number.required'    =>  trans('user.recipients_mobile_numbers_required'),
            'number.phone'       =>  trans('user.phone_invalid')
        ];
        $rules = [
            'number'             =>  'required|phone:US,IN'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        $yesterday = \Carbon\Carbon::yesterday();
        $tomorrow = \Carbon\Carbon::tomorrow();
        $messageLimit = \App\Models\ReferralMessage::where('user_id', '=', $userId)
                ->where('messaged_at', '>', $yesterday)
                ->where('messaged_at', '<', $tomorrow)
                ->count();
        $mobile     =   $request->input('number');
        $count  =   $messageLimit + 1;
        $validator->after(function($validator) use ($count, $messageLimit) {
            if( $count > config('constant.message_per_day_limit') ) {
                $remaining = config('constant.message_per_day_limit')-$messageLimit;
                $msg = ($remaining>0)?($remaining.trans('user.remaining_message')):'';
                $validator->errors()->add('invite.numbers.invalid', $msg.trans('user.message_send_exceeded'));
            }
        });
            
        if( $validator->fails() ) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        //$sender          =   $this->getSenderDetails(100);
        
        $referralCode   =   \App\Models\UserReferralCode::where('user_id', '=', $userId)->first();
        $response       =   $this->sendInviteMessage($mobile, $referralCode->short_url_sms);
        if( $response['error']  ) {
            return $this->response->errorUnwillingToProcess($response['message']);
        }
        
        $now    = \Carbon\Carbon::now();
        $messages   =   ['user_id'=>$userId, 'recipient_mobile'=>'+'.$mobile, 'messaged_at'=>$now];        
        //Insert the message referral to DB for this user
        \App\Models\ReferralMessage::insert($messages);
        
        return $this->response->withArray(['message'=>  $response['message'], 'meta'=>['http_code'=>200]]);        
    }
      
}