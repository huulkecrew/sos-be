<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;
use App\Serializer\ArrayIncludeSerializer;

class SearchesController extends BaseController
{   
    
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    /**
     * @api {get} /api/v1/searches/ Search Products
     * @apiName Product Search
     * @apiGroup Search
     * @apiDescription Get Listing of products
     * @apiPermission Customer,Guest
     * 
     * @apiParam {Interger} warehouse_id Warehouse ID is required.
     * @apiParam {Interger} zone_id Zone ID is required
     * @apiParam {String} term Term to search in products
     * @apiParam {Interger} department_id Optional
     * @apiParam {Interger} aisle_id Optional
     * @apiParam {Interger} page Page No is required
     * @apiParam {Interger..100} limit Per page limit required
     * 
     * @apiSuccess (200) {Object[]} Success 
     * 
     */ 
    public function findProducts(Request $request){
        $messages = [
            'term'                     => trans('products.term_required'),
            'warehouse_id.integer'     => trans('products.warehouse_id_integer'),
            'zone_id.integer'          => trans('products.zone_id_integer'),
            'department_id.integer'    => trans('products.department_id_integer'),
            'aisle_id.integer'         => trans('products.aisle_id_integer'),
            'page.integer'             => trans('products.page_integer'),
            'page.min'                 => trans('products.page_min'),
            'limit.integer'            => trans('products.limit_integer'),
            'limit.max'                => trans('products.limit_max'),
        ];
        $validator = Validator::make($request->all(), [
            'term'           => 'required|string',
            'warehouse_id'   => 'integer',
            'zone_id'        => 'integer',
            'department_id'  => 'integer',
            'aisle_id'       => 'integer',
            'page'           => 'required|integer|min:1',
            'limit'          => 'required|integer|max:100',
            'includes'       => 'alphanum'
        ], $messages);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $params                         = $request->only([
            'term', 'warehouse_id', 'zone_id', 'department_id', 'aisle_id', 'brand_name', 'organic', 'vegan', 'kosher', 
            'gluten_free', 'sort', 'page'
        ]);
        $params['limit']                = $request->get('limit', 25);
        $params['start']                = ($params['page']-1)*$params['limit'];
        $zonalProducts                  = \App\Models\ZonalProduct::search($params['term']);
        $zonalProductsCount             = $zonalProducts->totalHits();
        $nextPage                       = ($zonalProductsCount > $params['page']*$params['limit']) ? $params['page']+1 : NULL;
        $prevPage                       = ($params['page'] > 1 ? $params['page']-1 : NULL);
        $pagination                     = ['page'=>$params['page'], 'limit'=>$params['limit'], 'prev_page'=>$prevPage, 'next_page'=>$nextPage, 'total'=>$zonalProductsCount];
        return $this->response->withArray(['data'=> ['products'=>$zonalProducts, 'pagination'=>$pagination], 'meta'=>['http_code'=>200]]);
    }
    
}
