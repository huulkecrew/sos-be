<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;

class ProductsController extends BaseController
{   
    protected $model;
    
    public function __construct(Response $response) {
        $this->model    = new \App\Models\Product();
        parent::__construct($response);
    }
    
    /**
     * @api {post} /api/v1/products/special Add Special Product
     * @apiName AddSpecialProduct
     * @apiGroup Product
     * @apiDescription Add Special Product in warehouse
     * @apiPermission Customer,Guest
     * 
     * @apiParam {Interger} product[zipcode] Zipcode is required.
     * @apiParam {String} product[name] Product name is required.
     * @apiParam {Integer} product[warehouse_id] Warehouse ID is valid required. 
     * @apiParam {String="lb","each"} product[unit] of the product quantity only (lb or each)
     * @apiParam {Number{0.25 to 20 with unit lb, 1 to 99 with unit each}} product[quantity] of the product in only range(0.25 to 99.99 with unit lb, 1 to 99 with unit each).
     * @apiParam {Integer} product[department_id] Department ID is optional If case user do not know
     * @apiParam {Integer} product[zone_id] Zone Id optional.
     * 
     * @apiError {Object[]} ZipcodeRequired The <code>zipcode</code> is required.
     * @apiError {Object[]} ZipcodeInvalid  The <code>zipcode</code> is invalid.
     * @apiError {Object[]} ProductNameRequired The <code>name</code> is required.
     * @apiError {Object[]} WarehouseIdRequired The <code>warehouse_id</code> is required.
     * @apiError {Object[]} WarehouseIdInvalid The <code>warehouse_id</code> is invalid.
     * @apiError {Object[]} UnitRequired The <code>unit</code> is required.
     * @apiError {Object[]} UnitInvalid The <code>unit</code> is invalid.
     * @apiError {Object[]} QuantityRequired The <code>quantity</code> is required.
     * @apiError {Object[]} QuantityInvalid The <code>quantity</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     *  {
     *      "data": {
     *          "name": "My First Product",
     *              "created_at": {
     *                  "date": "2016-05-27 11:56:06",
     *                  "timezone_type": 3,
     *                  "timezone": "UTC"
     *              },
     *              "updated_at": {
     *                  "date": "2016-05-27 11:56:06",
     *                  "timezone_type": 3,
     *                  "timezone": "UTC"
     *              },
     *          "id": 78
     *      }
     *  }
     * 
     * @apiSuccess (200) {Object[]} Success Special Product created in the store.
     * @apiError (404) {Object[]} ProductNotCreated The product can not be created.
     * 
     */    
    public function addSpecialProduct(Request $request) {
        $messages = [
            'product.name.required'         => trans('product.name_required'),
            'product.zipcode'               => trans('product.zipcode_required'),
            'product.zipcode.exists'        => trans('product.zipcode_invalid'),
            'product.zone_id.required'      => trans('product.zone_id_required'),
            'product.zone_id.exists'        => trans('product.zone_id_invalid'),
            'product.warehouse_id.required' => trans('product.warehouse_id_required'),
            'product.warehouse_id.exists'   => trans('product.warehouse_id_invalid'),
            'product.department_id.exists'  => trans('product.department_id_invalid'),
            'product.quantity.required'     => trans('product.quantity_required'),
            'product.quantity.numeric'      => trans('product.quantity_invalid'),
            'product.unit.required'         => trans('product.unit_required'),
            'product.unit.in'               => trans('product.unit_invalid')
        ];
        
        $rules = [
            'product.name'          => 'required|max:255',
            'product.zipcode'       => 'required|digits:5',
            'product.warehouse_id'  => 'required|integer',
            'product.department_id' => 'exists:department,id,parent_id,0',
            'product.unit'          => 'required|in:'.config('constant.product_unit_pound').','.config('constant.product_unit_container'),
            'product.quantity'      => 'required|'.($request->only('product')['product']['unit'] === config('constant.product_unit_pound') ? 'numeric|min:.25|max:20' : 'integer|min:1|max:99'),
            
        ];
        $validator   = Validator::make($request->all(), $rules, $messages);
        $productData = $request->only('product')['product'];
        if($request->has('product.zipcode')){
            $zipcodeInfo = \App\Models\Zipcode::where('zipcode', '=', $productData['zipcode'])->first();
            $validator->after(function($validator) use ($zipcodeInfo) {
                if( !$zipcodeInfo ) {
                    $validator->errors()->add('product.zipcode.invalid', trans('product.zipcode_invalid'));
                }
            });
            if($request->has('product.warehouse_id')){
                $validator->after(function($validator) use ($productData) {
                    $warehouseInfo = \App\Models\ZipcodeWarehouse::where('zipcode', '=', $productData['zipcode'])
                            ->where('warehouse_id', '=', $productData['warehouse_id'])->first();
                    if( !$warehouseInfo ) {
                        $validator->errors()->add('product.warehouse_id.invalid', trans('product.warehouse_id_invalid'));
                    }
                });
            }
        }
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        //Add special product to product table
        $product             = new \App\Models\Product();
        $product->fill($request->product);
        if( $product->save() ) {
            $product->productNutritionFact()->save(new \App\Models\ProductNutritionFact());
            $productType        = config('constant.product_type_to_unit')[$productData['unit']];
            $zonalProductParams = ['product_id'=>$product->id, 'product_type'=>$productType, 'unit'=>$productData['unit'], 
                'zone_id'=>$zipcodeInfo->zone_id, 'warehouse_id'=>$productData['warehouse_id'], 'unlisted'=>1];
            $zonalProduct = $product->zonalProduct()->save(new \App\Models\ZonalProduct($zonalProductParams));
            //check department_id is in request or not then save in department
            if( $request->has('product.department_id') ) {
                $zonalProduct->departments()->attach($productData['department_id'], ['is_default'=>1]); //new \App\Models\ZonalProductDepartment($zonalProductDepartmentPrarams)
            }
            //two objects for small and large images to the same product
            $zonalProductImageParams = [
                new \App\Models\ZonalProductImage(['zonal_product_id'=>$zonalProduct->id, 'image_url'=> config('constant.product_image')['small'], 'image_type'=>1]),
                new \App\Models\ZonalProductImage(['zonal_product_id'=>$zonalProduct->id, 'image_url'=> config('constant.product_image')['large'], 'image_type'=>2])
            ];
            $zonalProduct->zonalProductImages()->saveMany($zonalProductImageParams);
            $zonalProductPriceParams = ['currency_id'=> 1, 'markup'=>settings('product.special_request_markup')]; //TODO fetch from configuration 
            $zonalProduct->zonalProductPrice()->save(new \App\Models\ZonalProductPrice($zonalProductPriceParams));
            $zonalProduct->zonalProductRelevance()->save(new \App\Models\zonalProductRelevance());
            return $this->fetch($zonalProduct->id);
        } else {
            return $this->response->errorNotFound(trans('product.special_product_not_created'));
        }

    }
    
    /**
     * @api {get} /api/v1/products/{id} Product Detail
     * @apiName Product Detail
     * @apiGroup Product
     * @apiDescription Get Detail of a product
     * @apiPermission Customer,Guest
     * 
     * 
     * @apiSuccessExample {json} Success-Response:
      {
        "data": {
          "id": 1147364,
          "product_id": 161422,
          "visible": 1,
          "available": 1,
          "featured": 0,
          "unlisted": 0,
          "unit": "each",
          "discrete_unit_count": null,
          "size": "5 oz",
          "display_size": "5 oz",
          "zone_id": 1,
          "warehouse_id": 2,
          "product_type": "normal",
          "counter_name": null,
          "brand_name": "Lightlife",
          "name": "Lightlife Smart Bacon Style Strips",
          "name_wa": "Tofu & Meat Alternatives Lightlife Smart Bacon Style Strips",
          "department_id": 1,
          "departments": [
            1
          ],
          "aisle_id": 16,
          "aisles": [
            16
          ],
          "details": "2 grams of protein per strip. Low saturated fat; naturally cholesterol free. Same great taste. We do not use soybeans that were produced using biotechnology. Veggie Goodness for you and the planet. The tasty and nutritious way to add sizzle to breakfast, burgers and BLTs! At Lightlife, our passion is delicious food. And because veggie food takes less from the land, each meal is your own, simple way of doing the world, and yourself, a little extra good. Contains 2 grams of protein per strip. No nitrates. Certified vegan.",
          "directions": "Must be kept refrigerated to maintain safety. Heating: Skillet (Our Favorite): Lightly spray nonstick skillet with nonstick cooking spray or brush with vegetable oil over medium heat until hot. Place strips in single layer and heat 3 to 3 1/2 minutes, turning once. Conventional or Toaster Oven: Preheat oven to 450 degrees F. Lightly spray baking sheet with nonstick cooking spray. Place strips in single layer. Lightly spray top side of strips. Bake 5 minutes, turning strips over after 3 minutes. Preparation Tip: For best results, peel Smart Bacon strips from the center of the package first. Gently separate individual strips.",
          "warning": null,
          "ingredients": "Water, Soy Protein Isolate, Wheat Gluten, Soybean Oil, Textured Soy Protein Concentrate, Textured Wheat Gluten, Less than 2% of: Natural Smoke Flavor, Natural Flavor (from Vegetable Sources), Grill Flavor (from Sunflower Oil), Carrageenan, Evaporated Cane Juice, Paprika Oleoresin (for Flavor & Color), Potassium Chloride, Sesame Oil, Fermented Rice Flour, Tapioca Dextrin, Citric Acid, Salt.",
          "special_instructons": null,
          "alcoholic": 0,
          "organic": 1,
          "vegan": 1,
          "kosher": 2,
          "gluten_free": 1,
          "in_season": 0,
          "serving_size": "1 slice",
          "servings_per_container": "14",
          "fat": "1",
          "saturated_fat": "0",
          "trans_fat": "0",
          "polysaturated_fat": null,
          "monounsaturated_fat": "0",
          "calories": "20",
          "fat_calories": "10",
          "cholestorol": null,
          "sodium": "140",
          "potassium": "40",
          "carbohydrate": "0",
          "fiber": "0",
          "sugars": "0",
          "protein": "2",
          "images": null,
          "ordered_count": "0",
          "found_rate_pcnt": "0.00",
          "frequently_found": 0,
          "frequently_not_found": 0,
          "price": "4.57",
          "full_price": "4.57",
          "price_per_unit": null,
          "on_sale": false,
          "pricing": {
            "price": "$4.57",
            "full_price": "$4.57",
            "full_price_label": "Reg: ",
            "badge": null
          },
          "currency": {
            "id": 1,
            "name": "United States Dollar",
            "iso_code": "USD",
            "iso_code_numeric": "840",
            "html_entity": "$",
            "symbol": "$",
            "symbol_first": "1",
            "other_display": "UD$",
            "smallest_denomination": "1",
            "subunit": "Cent",
            "subunit_to_unit": "100",
            "decimal_mark": ".",
            "thousands_seperator": ","
          },
          "related_product_ids": [
            1147338,
            1147344,
            1147370
          ],
          "related_products": [
            {
              "id": 1147338,
              "product_id": 148371,
              "visible": 0,
              "available": 0,
              "featured": 0,
              "unlisted": 0,
              "unit": "each",
              "discrete_unit_count": null,
              "size": "12 oz",
              "display_size": "12 oz",
              "zone_id": 1,
              "warehouse_id": 2,
              "product_type": "normal",
              "counter_name": null,
              "brand_name": "Lightlife",
              "name": "Lightlife Smart Dogs",
              "name_wa": "Tofu & Meat Alternatives Lightlife Smart Dogs",
              "department_id": 1,
              "departments": [
                1
              ],
              "aisle_id": 16,
              "aisles": [
                16
              ],
              "details": null,
              "directions": "Keep refrigerated. Use or freeze within 3-5 days of opening. Important! Smart Dogs cook quickly, do not overcook. Preferred Cooking Method - Stovetop: Boil water, turn off heat and put in Smart Dogs for 2 minutes. Microwave: Cut 3-4 diagonal slits in dogs, heat on High for 30-45 seconds per dog.",
              "warning": null,
              "ingredients": "Water, Soy Protein Isolate, Wheat Gluten, Evaporated Cane Juice, Less than 2% of: Natural Flavor (from Vegetable Sources), Natural Smoke Flavor, Garlic Powder, Paprika Oleoresin (Color and Flavor), Yeast Extract, Xanthan Gum, Guar Gum, Carrageenan, Fermented Rice Flour, Salt, Potassium Chloride.",
              "special_instructons": null,
              "alcoholic": 0,
              "organic": 1,
              "vegan": 1,
              "kosher": 1,
              "gluten_free": 1,
              "in_season": 0,
              "serving_size": "1 link",
              "servings_per_container": "8",
              "fat": "0",
              "saturated_fat": "0",
              "trans_fat": "0",
              "polysaturated_fat": null,
              "monounsaturated_fat": null,
              "calories": "45",
              "fat_calories": "0",
              "cholestorol": null,
              "sodium": "310",
              "potassium": "270",
              "carbohydrate": "2",
              "fiber": "1",
              "sugars": "1",
              "protein": "8",
              "images": null,
              "ordered_count": "0",
              "found_rate_pcnt": "0.00",
              "frequently_found": 0,
              "frequently_not_found": 0,
              "price": "5.74",
              "full_price": "5.74",
              "price_per_unit": null,
              "on_sale": false,
              "pricing": {
                "price": "$5.74",
                "full_price": "$5.74",
                "full_price_label": "Reg: ",
                "badge": null
              },
              "currency": {
                "id": 1,
                "name": "United States Dollar",
                "iso_code": "USD",
                "iso_code_numeric": "840",
                "html_entity": "$",
                "symbol": "$",
                "symbol_first": "1",
                "other_display": "UD$",
                "smallest_denomination": "1",
                "subunit": "Cent",
                "subunit_to_unit": "100",
                "decimal_mark": ".",
                "thousands_seperator": ","
              },
              "related_product_ids": [],
              "related_products": [],
              "suggested_replacement_product_ids": []
            },
            {
              "id": 1147344,
              "product_id": 161403,
              "visible": 0,
              "available": 0,
              "featured": 0,
              "unlisted": 0,
              "unit": "each",
              "discrete_unit_count": null,
              "size": "13.5 oz",
              "display_size": "13.5 oz",
              "zone_id": 1,
              "warehouse_id": 2,
              "product_type": "normal",
              "counter_name": null,
              "brand_name": "Lightlife",
              "name": "Lightlife Jumbo Veggie Protein Smart Dogs Links",
              "name_wa": "Tofu & Meat Alternatives Lightlife Jumbo Veggie Protein Smart Dogs Links",
              "department_id": 1,
              "departments": [
                1
              ],
              "aisle_id": 16,
              "aisles": [
                16
              ],
              "details": "Good source of protein. This is a low fat product; naturally cholesterol free. We do not use soybeans that were produced using biotechnology. No MSG; no nitrites. Certified vegan.",
              "directions": "Keep refrigerated. Important: Smart Dogs Jumbo cook quickly, do not overcook. Preferred Cooking Method: Stove Top: Boil water, turn off heat and put in Smart Dogs for 2 minutes. Microwave: Cut 3-4 diagonal slits in dogs, heat on High for 30-45 seconds per dog. Use or freeze within 3-5 days of opening.",
              "warning": null,
              "ingredients": "Water, Soy Protein Isolate, Wheat Gluten, Evaporated Cane Juice, Less than 2% of: Natural Flavor (from Vegetable Sources), Natural Flavor (from Vegetable Sources), Natural Smoke Flavor, Garlic Powder, Paprika Oleoresin (Color and Flavor), Yeast Extract, Xanthan Gum, Guar Gum, Carrageenan, Fermented Rice Flour, Salt, Potassium Chloride.",
              "special_instructons": null,
              "alcoholic": 0,
              "organic": 1,
              "vegan": 1,
              "kosher": 2,
              "gluten_free": 1,
              "in_season": 0,
              "serving_size": "76.00 g",
              "servings_per_container": "5",
              "fat": "1",
              "saturated_fat": "0",
              "trans_fat": "0",
              "polysaturated_fat": null,
              "monounsaturated_fat": null,
              "calories": "80",
              "fat_calories": "5",
              "cholestorol": null,
              "sodium": "560",
              "potassium": "490",
              "carbohydrate": "3",
              "fiber": "2",
              "sugars": "1",
              "protein": "15",
              "images": null,
              "ordered_count": "0",
              "found_rate_pcnt": "0.00",
              "frequently_found": 0,
              "frequently_not_found": 0,
              "price": "5.28",
              "full_price": "5.28",
              "price_per_unit": null,
              "on_sale": false,
              "pricing": {
                "price": "$5.28",
                "full_price": "$5.28",
                "full_price_label": "Reg: ",
                "badge": null
              },
              "currency": {
                "id": 1,
                "name": "United States Dollar",
                "iso_code": "USD",
                "iso_code_numeric": "840",
                "html_entity": "$",
                "symbol": "$",
                "symbol_first": "1",
                "other_display": "UD$",
                "smallest_denomination": "1",
                "subunit": "Cent",
                "subunit_to_unit": "100",
                "decimal_mark": ".",
                "thousands_seperator": ","
              },
              "related_product_ids": [],
              "related_products": [],
              "suggested_replacement_product_ids": []
            },
            {
              "id": 1147370,
              "product_id": 200429,
              "visible": 1,
              "available": 1,
              "featured": 0,
              "unlisted": 0,
              "unit": "each",
              "discrete_unit_count": null,
              "size": "6 oz",
              "display_size": "6 oz",
              "zone_id": 1,
              "warehouse_id": 2,
              "product_type": "normal",
              "counter_name": null,
              "brand_name": "Lightlife",
              "name": "Lightlife Fakin' Bacon Organic Smoky Tempeh Strips",
              "name_wa": "Tofu & Meat Alternatives Lightlife Fakin' Bacon Organic Smoky Tempeh Strips",
              "department_id": 1,
              "departments": [
                1
              ],
              "aisle_id": 16,
              "aisles": [
                16
              ],
              "details": "",
              "directions": "Keep refrigerated. To Cook: Pan fry in a moderate amount of hot oil until crispy around edges. Will keep for 5-7 days tightly wrapped and refrigerated after opening.",
              "warning": null,
              "ingredients": "Organic Soy Tempeh (Cultured Organic Soybeans, Water, Organic Brown Rice), Water, Organic Soy Sauce (Water, Organic Soybeans, Organic Wheat, Salt), Organic Vinegar, Organic Evaporated Cane Juice, Less than 2% of: Spices, Beet Powder, Organic Onion Powder, Natural Vegetable Flavor, Salt, Natural Hickory Flavor, Yeast Extract.",
              "special_instructons": null,
              "alcoholic": 0,
              "organic": 2,
              "vegan": 1,
              "kosher": 2,
              "gluten_free": 1,
              "in_season": 0,
              "serving_size": "3 slices",
              "servings_per_container": "3",
              "fat": "3",
              "saturated_fat": null,
              "trans_fat": null,
              "polysaturated_fat": null,
              "monounsaturated_fat": null,
              "calories": "100",
              "fat_calories": "25",
              "cholestorol": null,
              "sodium": "470",
              "potassium": "135",
              "carbohydrate": "10",
              "fiber": "4",
              "sugars": "2",
              "protein": "8",
              "images": null,
              "ordered_count": "0",
              "found_rate_pcnt": "0.00",
              "frequently_found": 0,
              "frequently_not_found": 0,
              "price": "5.49",
              "full_price": "5.49",
              "price_per_unit": null,
              "on_sale": false,
              "pricing": {
                "price": "$5.49",
                "full_price": "$5.49",
                "full_price_label": "Reg: ",
                "badge": null
              },
              "currency": {
                "id": 1,
                "name": "United States Dollar",
                "iso_code": "USD",
                "iso_code_numeric": "840",
                "html_entity": "$",
                "symbol": "$",
                "symbol_first": "1",
                "other_display": "UD$",
                "smallest_denomination": "1",
                "subunit": "Cent",
                "subunit_to_unit": "100",
                "decimal_mark": ".",
                "thousands_seperator": ","
              },
              "related_product_ids": [],
              "related_products": [],
              "suggested_replacement_product_ids": []
            }
          ],
          "suggested_replacement_product_ids": [
            1147370
          ],
     *    "cart_qty"              :  0.0
        },
        "meta": {
          "http_code": 200
        }
      }
     * 
     * @apiSuccess (200) {Object[]} Success 
     * 
     * @apiError (404) {Object} Product not found.
     * 
     */ 
    public function fetch($id){
        $zonalProduct = \App\Models\ZonalProduct::getItem($id); 
        if($zonalProduct){
            return $this->response->withArray(['data'=> \App\Transformer\ZonalProductTransformer::transformWithDetailContext($zonalProduct), 'meta'=>['http_code'=>200]]);
        } else {
            return $this->response->errorNotFound(trans('product.product_not_found'));
        }
    }
    
    /**
     * @api {get} /api/v1/products/ Product Listing
     * @apiName Product Listing
     * @apiGroup Product
     * @apiDescription Get Listing of products
     * @apiPermission Customer,Guest
     * 
     * @apiParam {Interger} warehouse_id Warehouse ID is required.
     * @apiParam {Interger} zone_id Zone ID is required
     * @apiParam {Interger} department_id Optional
     * @apiParam {Interger} aisle_id Optional
     * @apiParam {Interger} page Page No is required
     * @apiParam {Interger..50} limit Per page limit required
     * 
     * @apiSuccess (200) {Object[]} Success 
     * 
     */ 
    public function listProducts(Request $request){
        $messages = [
            'warehouse_id.integer'     => trans('products.warehouse_id_integer'),
            'zone_id.integer'          => trans('products.zone_id_integer'),
            'department_id.integer'    => trans('products.department_id_integer'),
            'aisle_id.integer'         => trans('products.aisle_id_integer'),
            'page.integer'             => trans('products.page_integer'),
            'page.min'                 => trans('products.page_min'),
            'limit.integer'            => trans('products.limit_integer'),
            'limit.max'                => trans('products.limit_max'),
        ];
        $validator = Validator::make($request->all(), [
            'warehouse_id'   => 'integer',
            'zone_id'        => 'integer',
            'department_id'  => 'integer',
            'aisle_id'       => 'integer',
            'page'           => 'required|integer|min:1',
            'limit'          => 'required|integer|max:50',
            'includes'       => 'alphanum'
        ], $messages);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $params                         = $request->only([
            'warehouse_id', 'zone_id', 'department_id', 'aisle_id', 'brand_name', 'organic', 'vegan', 'kosher', 
            'gluten_free', 'sort', 'page'
        ]);
        $params['limit']                = $request->get('limit', 25);
        $params['start']                = ($params['page']-1)*$params['limit'];
        $qparams = [];
        $qparams['body']['filter']['bool']['must'][]['term']['zone_id']                = $params['zone_id'];
        $qparams['body']['filter']['bool']['must'][]['term']['warehouse_id']           = $params['warehouse_id'];
        $qparams['body']['filter']['bool']['must'][]['term']['available']              = 1;
        if (!empty($params['aisle_id'])) {
            $qparams['body']['filter']['bool']['must'][]['term']['aisle_id']           = $params['aisle_id'];
        }
        if (!empty($params['department_id'])) {
            $qparams['body']['filter']['bool']['must'][]['term']['department_id']      = $params['department_id'];
            //$qparams['body']['filter']['bool']['must'][]['term']['featured']           = 1;
        }
        if (!empty($params['brand_name'])) {
            $qparams['body']['filter']['bool']['must'][]['term']['brand_name']         = $params['brand_name'];
        }
        $filter_array = ['organic', 'vegan', 'kosher', 'gluten_free'];
        foreach ($filter_array as $filter) {
            if (!empty($params[$filter])) {
                $qparams['body']['filter']['bool']['must'][]['term'][$filter] = 1;
            }
        }
        $qparams['body']['from'] = $params['start'];
        $qparams['body']['size'] = $params['limit'];
        if (!empty($params['sort'])) {
            $qparams['body']['sort'] = $params['sort'];
        }
        $zonalProducts = \App\Models\ZonalProduct::complexSearch($qparams);
        /*$includes                       = ['product.productNutritionFact', 'product.manufacturer', 'departments', 'aisles',
                                          'zonalProductImages', 'zonalProductPrice', 'zonalProductPrice.currency', 'zonalProductRelevance',
                                          'zonalProductDiscounts', 'zonalProductRelatedProducts', 'zonalProductReplacements'];
        
        $params['limit']                = $request->get('limit', 25);
        $params['start']                = ($params['page']-1)*$params['limit'];
        $params['available']            = 1;
        $zonalProductsCount             = \App\Models\ZonalProduct::getItemsCount($params, $includes);
        $zonalProducts                  = [];
        if($zonalProductsCount){
            $zonalProducts                  = \App\Models\ZonalProduct::getItems($params, $includes);
            $zonalProducts = $zonalProducts->map(function($v){
                return \App\Transformer\ZonalProductTransformer::transformWithDetailContext($v);
            });
        }  
        $nextPage                       = ($zonalProductsCount > $params['page']*$params['limit']) ? $params['page']+1 : NULL;
        $prevPage                       = ($params['page'] > 1 ? $params['page']-1 : NULL);
        $pagination                     = ['page'=>$params['page'], 'limit'=>$params['limit'], 'prev_page'=>$prevPage, 'next_page'=>$nextPage, 'total'=>$zonalProductsCount];
       */
        $zonalProductsCount             = $zonalProducts->totalHits();
        $nextPage                       = ($zonalProductsCount > $params['page']*$params['limit']) ? $params['page']+1 : NULL;
        $prevPage                       = ($params['page'] > 1 ? $params['page']-1 : NULL);
        $pagination                     = ['page'=>$params['page'], 'limit'=>$params['limit'], 'prev_page'=>$prevPage, 'next_page'=>$nextPage, 'total'=>$zonalProductsCount];
        return $this->response->withArray(['data'=> ['products'=>$zonalProducts, 'pagination'=>$pagination], 'meta'=>['http_code'=>200]]);
         
    }
    
}