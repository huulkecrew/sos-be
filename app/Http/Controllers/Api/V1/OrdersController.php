<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;
use App\Serializer\ArrayIncludeSerializer;

class OrdersController extends BaseController
{   
    public function __construct(Response $response) {
        parent::__construct($response);
    }

    /**
     * @api {get} /orders/open Get Open orders
     * @apiName GetCustomerOpenOrders
     * @apiGroup Orders
     * @apiDescription Get Customer's Open Order List
     * @apiPermission Customer
     * 
     * 
     * @apiSuccess (200) {Object[]} Success The Order list, Empty list if no Open or list .
     * 
     * 
     */
    public function getOpenOrders(Request $request) {
        $authUser         =   Auth::user();
        $userId           =   $authUser->id;
        //Active orders If the service status is Pending and Payment status is Complete and Chargedback and Processed
        $paymentStatus    =   [config('constant.order_status')['payment']['complete'], config('constant.order_status')['payment']['chargedback'], config('constant.order_status')['payment']['processed']];
        $serviceStatus    =   config('constant.order_status')['service']['pending'];
        $orders           =   \App\Models\Order::openOrders($userId, $paymentStatus, $serviceStatus);
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($orders, new \App\Transformer\OrderTransformer(), 'data', null, ['http_code'=>200]);
    }
    
    /**
     * @api {get} /orders/past Get Past orders
     * @apiName GetCustomerPastOrders
     * @apiGroup Orders
     * @apiDescription Get Customer's Past Order List
     * @apiPermission Customer
     * 
     * 
     * @apiSuccess (200) {Object[]} Success The Order list, Empty list if no Past or list .
     * 
     * 
     */
    public function getPastOrders(Request $request) {
        $authUser       =   Auth::user();
        $userId         =   $authUser->id;
        //Past orders If the service status is Complete or Cancelled
        $serviceStatus  =   [config('constant.order_status')['service']['complete'], config('constant.order_status')['service']['canceled']];
        $orders         =   \App\Models\Order::pastOrders($userId, $serviceStatus);
        
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($orders, new \App\Transformer\OrderTransformer(), 'data', null, ['http_code'=>200]);
    }


    /**
     * @api {post} /customer/feedback Add Customer Feedback
     * @apiName Add Customer Feedback
     * @apiGroup Orders
     * @apiDescription Add Customer Feedback
     * @apiPermission Customer
     * 
     * @apiParam {String="...128"}  feedback[comment] Comment on order is required without Rating.
     * @apiParam {Integer=1/2/3/4/5}   feedback[rating] Rating on the order is required without comment.
     * @apiParam {Integer}  feedback[sub_order_id] The sub_order_id is required.
     * 
     * @apiError {Object[]} CommentTextRequired The <code>feedback[comment]</code> is required If rating is present there.
     * @apiError {Object[]} RatingRequired  The <code>feedback[rating]</code> is required If rating is present there.
     * @apiError {Object[]} SubOrderIdRequired The <code>feedback[sub_order_id]</code> is required. 
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": {
     *     "id": 5,
     *     "sub_order_id": 4,
     *     "comment": "This is my successful order",
     *     "rating": 4,
     *     "created_at": "2016-06-05 06:22:21",
     *     "updated_at": "2016-06-05 06:22:21"
     *   },
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     * @apiSuccess (200) {Object} Success Address updated successfully.
     * @apiError (400) {Object} Validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function addFeedbackOnSubOrder(Request $request) {
        $messages = [
            'feedback.comment.required_without'     =>  trans('order.comment_required_without_rating'),
            'feedback.comment'                      =>  trans('order.comment_is_too_long'),
            'feedback.rating.required_without'      =>  trans('order.rating_required_without_comment'),
            'feedback.rating.in'                    =>  trans('order.rating_invalid'),
            'feedback.sub_order_id.required'        =>  trans('order.suborder_id_required'),
            'feedback.sub_order_id.integer'         =>  trans('order.suborder_id_invalid')
        ];
        $rules = [
            'feedback.comment'      =>   'required_without:feedback.rating|max:128',
            'feedback.rating'       =>   'required_without:feedback.comment|in:1,2,3,4,5',
            'feedback.sub_order_id' =>   'required|integer'
        ];
        
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $requestData        =   $request->only('feedback')['feedback'];
        $customerFeedback   =   new \App\Models\CustomerFeedback();
        $customerFeedback->fill($requestData);
        if( !$customerFeedback->save() ) {
            return $this->response->errorInternalError(trans('order.feedback_can_not_be_added'));   
        }
        return $this->response->withArray(['message'=> \App\Transformer\OrderTransformer::customerFeedback($customerFeedback), 'meta'=>['http_code'=>200]]);
    }
    
    public function orderSummary() {
        echo "Summary"; exit;
    }
    
    public function redeemCoupen(Request $request) {
        $userId     = Auth::user()->id;
        $messages   =   [
            'coupon.code.required'   => trans('order.coupon_code_required'),
            'coupon.code.exists'     => trans('order.coupon_code_invalid')
        ];
        $rules      =   [
            'coupon.code'           =>  'required|exists:coupon,code',
            'coupon.cart_amount'    =>  'required'
        ];
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $requestData    =   $request->only('coupon')['coupon'];
        $result         =   $this->validateCoupon($userId, $requestData['cart_amount'], $requestData['code']);   
        if( !$result['status'] )  {
            return $this->response->errorUnwillingToProcess(trans($result['message']));
        } 
        return $this->response->withArray(['data'=> $result['message'], 'meta'=> ['http_code'=>200]]);
    }

    /**
     * @api {patch} /customer/feedback Update Customer Feedback
     * @apiName Update Customer Feedback
     * @apiGroup Orders
     * @apiDescription Update Customer Feedback
     * @apiPermission Customer
     * 
     * @apiParam {Integer}  feedback[id] Feedback ID is required. 
     * @apiParam {String="...128"}  feedback[comment] Comment on order is required without Rating.
     * @apiParam {Integer=1/2/3/4/5}  feedback[rating] Rating on the order is required without comment.
     * @apiParam {Integer}  feedback[sub_order_id] The sub_order_id is required.
     * 
     * @apiError {Object[]} FeedbackIdRequired The <code>feedback[id]</code> is required.
     * @apiError {Object[]} FeedbackIdInvalid The <code>feedback[id]</code> is invalid.
     * @apiError {Object[]} CommentTextRequired The <code>feedback[comment]</code> is required If rating is present there.
     * @apiError {Object[]} RatingRequired  The <code>feedback[rating]</code> is required If rating is present there.
     * @apiError {Object[]} SubOrderIdRequired The <code>feedback[sub_order_id]</code> is required. 
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": {
     *     "id": 5,
     *     "sub_order_id": 4,
     *     "comment": "Update rating",
     *     "rating": 5,
     *     "created_at": "2016-06-05 06:22:21",
     *     "updated_at": "2016-06-05 06:29:48"
     *   },
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     * @apiSuccess (200) {Object} Success Address updated successfully.
     * @apiError (400) {Object} Validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function updateFeedbackOnSubOrder(Request $request) {
        $messages = [
            'feedback.id.required'                  =>  trans('order.feedback_id_required'),
            'feedback.id.exists'                    =>  trans('order.feedback_id_invalid'),
            'feedback.comment.required_without'     =>  trans('order.comment_required_without_rating'),
            'feedback.comment'                      =>  trans('order.comment_is_too_long'),
            'feedback.rating.required_without'      =>  trans('order.rating_required_without_comment'),
            'feedback.rating.in'                    =>  trans('order.rating_invalid'),
            'feedback.sub_order_id.required'        =>  trans('order.suborder_id_required'),
            'feedback.sub_order_id.integer'         =>  trans('order.suborder_id_invalid')
        ];
        $rules = [
            'feedback.id'           =>   'required|exists:customer_feedback,id',
            'feedback.comment'      =>   'required_without:feedback.rating|max:128',
            'feedback.rating'       =>   'required_without:feedback.comment|in:1,2,3,4,5',
            'feedback.sub_order_id' =>   'required|integer'
        ];
        
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $requestData        =   $request->only('feedback')['feedback'];
        $customerFeedback   = \App\Models\CustomerFeedback::where('id', '=', $requestData['id'])
                ->where('sub_order_id', '=', $requestData['sub_order_id'])->first();
        $customerFeedback->comment      =   $request->has('feedback.comment')?$requestData['comment']:$customerFeedback->comment;
        $customerFeedback->rating       =   $request->has('feedback.rating')?$requestData['rating']:$customerFeedback->rating;
        $customerFeedback->updated_at   = \Carbon\Carbon::now();
        if( !$customerFeedback->save() ) {
            return $this->response->errorInternalError(trans('order.feedback_can_not_be_updated'));   
        }
        
        return $this->response->withArray(['message'=> \App\Transformer\OrderTransformer::customerFeedback($customerFeedback), 'meta'=>['http_code'=>200]]);
        
    }
    
    private function validateCoupon($userId, $cart_amount, $couponCode) {      
        $result =   [];
        $now    = \Carbon\Carbon::now();
        //How apply for case sentsitive
        $coupon = \App\Models\Coupon::where('code', '=', $couponCode)
                ->where('active', '=', 1)
                ->where('date_start', '<', $now)
                ->where('date_end', '>', $now)->first();
        if(!$coupon) {
            $result['status']   =   false;
            $result['message']  =   trans('order.coupon_code_invalid');
        } else if($cart_amount < $coupon->min_total) {
            $result['status']   =   false;
            $result['message']  =   trans('order.coupon_code_can_not_be_used_at_less_amount').$coupon->min_total;
        } else {
            $result['status']   =   true;
            $result['message']  =   $coupon;
        }
        return $result;
    }
}
    