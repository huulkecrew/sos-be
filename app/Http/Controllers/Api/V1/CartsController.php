<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Warehouse;

class CartsController extends BaseController
{   
    protected $model;
    
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    /**
     * @api {post} /api/v1/carts/summary Summary For Cart View 
     * @apiName Cart Summary
     * @apiGroup Cart
     * @apiDescription Summary for Cart View http://betaapi.burpy.com/api/v1/carts/summary?cart_id=2&cart[0][product_id]=1654116&cart[0][customer_note]=xyz&cart[0][source_type]=store_root&cart[0][source_value]=walmart&cart[0][quantity]=2.00&cart[1][product_id]=1654115&cart[1][customer_note]=abc&cart[1][source_type]=store_root&cart[1][source_value]=heb&cart[1][quantity]=1.00&zone_id=1&zipcode=78705
     * @apiPermission Customer|guest
     * 
     * @apiParam {Integer}  cart_id Cart Id, Bootstrap cart of the user or the shared cart id.
     * @apiParam {Integer}  zone_id
     * @apiParam {Integer}  zipcode
     * @apiParam {Array[]}  cart
     * @apiParam {Array}  cart[0]   Fields are [zonal_product_id, quantity, customer_note]
     * 
     * 
     * @apiSuccessExample {json} Success-Response:
     *  {
        "data": {
          "items_total": [
            {
              "zonal_product_id": 1654115,
              "display_total_full": "$8.90"
            },
            {
              "zonal_product_id": 1654116,
              "display_total_full": "$9.34"
            }
          ],
          "warehouse_totals": {
            "1": "$18.24"
          },
          "display_total": "$18.24",
          "warehouses": {
            "1": {
              "name": "Walmart",
              "background_color": "#00508f",
              "pickup": 0,
              "delivery": 1
            }
          },
          "cart_fields": {
            "1": {
              "error": {
                "message": "Delivery from this store isn't available in your area yet",
                "not_in_zone": 1
              }
            }
          }
        }
      }
     * 
     * @apiSuccess (200) {Object} .
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function summary(Request $request) {
        $authUser = Auth::user();
        $messages = [
            'zone_id.required'            => trans('carts.zone_id_required'),
            'zipcode.required'            => trans('carts.zipcode_required'),
            'zipcode.digits'              => trans('carts.zipcode_digits'),
            'cart_id.required'            => trans('carts.cart_id_required'),
            'cart_id.integer'             => trans('carts.cart_id_integer'),
            'cart_id.exists'              => trans('carts.cart_id_exists'),
            'cart.*.zonal_product_id_required'  => trans('carts.*.zonal_product_id_required'),
            'cart.*.zonal_product_id_integer'   => trans('carts.*.zonal_product_id_integer'),
            'cart.*.quantity_required'    => trans('carts.*.quantity_required'),
            'cart.*.quantity_numeric'     => trans('carts.*.quantity_numeric'),
            'cart.*.customer_note.max'   => trans('cart.*.customer_note.max'),
            'cart.*.source_type.max'     => trans('cart.*.source_type.max'),
            'cart.*.source_value.max'    => trans('cart.*.source_value.max')
        ];
        $validator = Validator::make($request->all(), [
            'cart_id'                     => 'required|integer|exists:cart,id,user_id,'.$authUser->id,
            'zone_id'                     => 'required|integer',
            'zipcode'                     => 'required|digits:5',
            //'cart_id'                     => 'required|integer|exists:cart,id',
            'cart.*.zonal_product_id'     => 'required|integer',
            'cart.*.quantity'             => 'required|numeric',
            'cart.*.customer_note'        => 'string|max:150',
            'cart.*.source_type'          => 'string|max:50',
            'cart.*.source_value'         => 'string|max:50'
        ], $messages);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $cartId         = $request->input('cart_id');
        $zoneId         = $request->input('zone_id');
        $cartProducts   = $request->input('cart');
        foreach($cartProducts as $cartProduct){
            if($cartProduct['quantity'] == 0){
                CartProduct::where('cart_id', '=', $cartId)
                        ->where('zonal_product_id', '=', $cartProduct['zonal_product_id'])->delete();
            }else{
                $product = CartProduct::firstOrNew(['cart_id'=>$cartId, 'zonal_product_id'=>$cartProduct['zonal_product_id']]);
                $product->fill($cartProduct);
                $product->save();
            }
        }
        $cart = Cart::where('id', '=', $cartId)->with(['zonalProducts'=>function($q){
            $q->with(['zonalProductPrice'=>function($q){
               $q->with('currency'); 
            }])->with(['zonalProductDiscounts'=>function($q){
                $q->where('active', '=', 1);
            }])->withPivot('quantity');
        }])->first(); 
        $warehouses           = collect($cart->zonalProducts)->pluck('warehouse_id')->unique()->values()->all();
        $cart->warehouses = Warehouse::whereIn('id', $warehouses)->with(['warehouseZonalServices'=>function($q) use ($zoneId){
            $q->where('zone_id', '=', $zoneId);
        }])->get();
        
        return $this->response->withArray(['data'=> \App\Transformer\CartTransformer::transformWithCartSummaryContext($cart)]);
        //Now fetch create the price list
    }
    
}
