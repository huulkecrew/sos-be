<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\UserUtils;
use App\Serializer\ArrayIncludeSerializer;

class AddressesController extends BaseController
{   
    use UserUtils;
    
    public function __construct(Response $response) {
        $this->model    = new \App\Models\CustomerAddress();
        parent::__construct($response);
    }
    
    /**
     * @api {get} /api/v1/user/addresses Address List
     * @apiName GetCustomerAddresses
     * @apiGroup Address
     * @apiDescription Get Customer's Address List
     * @apiPermission Customer
     * 
     * 
     * @apiSuccess (200) {Object[]} Success The address list, Empty list if no address .
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": [
     *     {
     *      "id": 11,
            "user_id": "74",
            "address_type": "R",
            "label": "Home",
            "company": "",
            "address_1": "11760 Freguson Road",
            "address_2": "",
            "city": "Dallas",
            "state_id": "41",
            "state": "TX",
            "zipcode": "75228",
            "country_id": "223",
            "country": "US",
            "lat": "32.85",
            "lng": "-96.65",
            "instructions": "",
            "created_at": "2016-06-03 06:46:57",
            "updated_at": "2016-06-03 06:46:57"
     *     }
     *     ...
     *   ],
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     */
    public function getAddressList() {
        $authUser   =   Auth::user();
        $userId     =   $authUser->id;
        $addresses  = \App\Models\CustomerAddress::getAddressList($userId);
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($addresses, new \App\Transformer\AddressTransformer, 'data', null, ['http_code'=>200]);
    }

    /**
     * @api {post} /api/v1/user/address Add new address
     * @apiName Add Address
     * @apiGroup Address
     * @apiDescription Add New Address
     * @apiPermission Customer
     * 
     *  
     * @apiParam {String="...150"}  address[label] Address Label is required.
     * @apiParam {String="R","B"}   address[address_type] Address type is required(B=> Bussiness, R=>Residential).
     * @apiParam {String}           address[company] Company address is required for Business type of address.
     * @apiParam {String}           address[address_1] Address1 is required.
     * @apiParam {String}           address[address_2] Apartment name is optional.
     * @apiParam {String}           address[zipcode] Valid Zipcode is required.
     * @apiParam {Integer}          address[attempt_count] Set the attempt count when api returns 431 code, that covers the case when
     *                                                     address is not validated via google api.
     * @apiParam {String}           address[instructions] The address instructions is optional.
     * 
     * @apiError {Object[]} ZipcodeRequired The <code>zipcode</code> is required.
     * @apiError {Object[]} ZipcodeInvalid  The <code>zipcode</code> is invalid.
     * @apiError {Object[]} AddressTypeRequired The <code>address_type</code> is required. 
     * @apiError {Object[]} AddressTypeInvalid  The <code>address_type</code> is invalid. 
     * @apiError {Object[]} AddressLabelRequired The <code>label</code> is required. 
     * @apiError {Object[]} AddressLabelInvalid  The <code>label</code> is invalid.
     * @apiError {Object[]} CompanyAddressRequired The <code>conpany</code> is required with B=>Business type address.
     * @apiError {Object[]} Address1Required The (Street Address)<code>address_1</code> is required. 
     * @apiError {Object[]} AttemptCountInvalid  The <code>attempt_count</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *  "data": {
     *      "id": 11,
            "user_id": "74",
            "address_type": "R",
            "label": "Home",
            "company": "",
            "address_1": "11760 Freguson Road",
            "address_2": "",
            "city": "Dallas",
            "state_id": "41",
            "state": "TX",
            "zipcode": "75228",
            "country_id": "223",
            "country": "US",
            "lat": "32.85",
            "lng": "-96.65",
            "instructions": "",
            "created_at": "2016-06-03 06:46:57",
            "updated_at": "2016-06-03 06:46:57"
     *      "id": 6
     *    }
     * }
     * 
     * @apiSuccess (200) {Object} Success Address has been added successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (431) {Object} Address not validated via google api.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function addAddress(Request $request) {
        $messages = [
            'address.label.required'            => trans('address.address_lable_required'),
            'address.lable.max'                 => trans('address_lable_max'),
            'address.address_1.required'        => trans('address.address1_required'),
            'address.zipcode.required'          => trans('address.zipcode_required'),
            'address.zipcode.exists'            => trans('address.zipcode_invalid'),
            'address.address_type.required'     => trans('address.address_type_required'),
            'address.address_type.in'           => trans('address.address_type_invalid'),            
            'address.company.required_if'       => trans('address.company_required'),
            'address.instructions.required'     => trans('address.address_instructions_required'),
            'address.attempt_count.in'          => trans('address.address_add_attempt_must__1')
            
        ];
        
        $rules = [
            'address.label'             =>  'required|max:150',
            'address.address_type'      =>  'required|in:'.config('constant.address')['residential'].','.config('constant.address')['business'],
            'address.company'           =>  'required_if:address.address_type,'.  config('constant.address')['business'].'|max:60',
            'address.address_1'         =>  'required|max:128',
            'address.address_2'         =>  'max:128',
            'address.zipcode'           =>  'required|exists:zipcode,zipcode',
            'address.attempt_count'     =>  'in:1'
            ];
        
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $addressData        =   $request->only('address')['address'];
        $zipcodeInfo        = \App\Models\Zipcode::getItem($addressData['zipcode']);
        $params = array(
            "address"      => $addressData['address_1'], 
            "zipcode"      => $addressData['zipcode'], 
            "country"      => $zipcodeInfo->city->state->country->iso_code_2,
            "attempt_count" => $request->has('address.attempt_count') ? $addressData['attempt_count'] : 0
        );
        $geoResponse = $this->validateAddress($params);
        if ($geoResponse['error']) {
            return $this->response->errorUnwillingToProcess(trans($geoResponse['error_code']));
        } 
        $addressData['zone_id'] = $zipcodeInfo->zone_id;
        $authUser               =   Auth::user();
        $userId                 =   $authUser->id;
        $address                = new \App\Models\CustomerAddress();
        $address->fill($addressData);
        $address->user_id       =   $userId;
        $address->state_id      =   $zipcodeInfo->city->state->id;
        $address->state         =   $zipcodeInfo->city->state->code;
        $address->country_id    =   $zipcodeInfo->city->state->country->id;
        $address->country       =   $zipcodeInfo->city->state->country->iso_code_2;
        $address->city          =   $geoResponse['city'];
        $address->lat           =   $geoResponse['geoloc']['geometry']['location']['lat'];
        $address->lng           =   $geoResponse['geoloc']['geometry']['location']['lng'];
        if( $address->save() ) {
            return $this->response->withItem($address, new \App\Transformer\AddressTransformer, 'data', ['http_code'=>200]);
        } else {
            return $this->response->errorInternalError(trans ('address.something_went_wrong'));
        }
    }
    
    /**
     * @api {put} /api/v1/user/address Update Address
     * @apiName Update Address
     * @apiGroup Address
     * @apiDescription Update Customer Address
     * @apiPermission Customer
     * 
     *  
     * @apiParam {String="...150"}  address[label] Address Label is required.
     * @apiParam {String="R","B"}   address[address_type] Address type is required(B=> Bussiness, R=>Residential).
     * @apiParam {String}           address[company] Company address is required for Business type of address.
     * @apiParam {String}           address[address_1] Address1 is required.
     * @apiParam {String}           address[address_2] Apartment name is optional.
     * @apiParam {String}           address[zipcode] Valid Zipcode is required.
     * @apiParam {Integer}          address[attempt_count] Set the attempt count when api returns 431 code, that covers the case when
     *                                                     address is not validated via google api.
     * @apiParam {String}           address[instructions] The address instructions is optional.
     * 
     * @apiError {Object[]} ZipcodeRequired The <code>zipcode</code> is required.
     * @apiError {Object[]} ZipcodeInvalid  The <code>zipcode</code> is invalid.
     * @apiError {Object[]} AddressTypeRequired The <code>address_type</code> is required. 
     * @apiError {Object[]} AddressTypeInvalid  The <code>address_type</code> is invalid. 
     * @apiError {Object[]} AddressLabelRequired The <code>label</code> is required. 
     * @apiError {Object[]} AddressLabelInvalid  The <code>label</code> is invalid.
     * @apiError {Object[]} CompanyAddressRequired The <code>conpany</code> is required with B=>Business type address.
     * @apiError {Object[]} Address1Required The (Street Address)<code>address_1</code> is required. 
     * @apiError {Object[]} AttemptCountInvalid  The <code>attempt_count</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *  "data": {
     *      "id": 11,
            "user_id": "74",
            "address_type": "R",
            "label": "Home",
            "company": "",
            "address_1": "11760 Freguson Road",
            "address_2": "",
            "city": "Dallas",
            "state_id": "41",
            "state": "TX",
            "zipcode": "75228",
            "country_id": "223",
            "country": "US",
            "lat": "32.85",
            "lng": "-96.65",
            "instructions": "",
            "created_at": "2016-06-03 06:46:57",
            "updated_at": "2016-06-03 06:46:57"
     *      "id": 6
     *    }
     * }
     * 
     * @apiSuccess (200) {Object} Success Address updated successfully.
     * @apiError (400) {Object} Validation errors.
     * @apiError (431) {Object} Address not validated via google api.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function updateAddress(Request $request) {
        $userId                                 =  Auth::user()->id;
        $messages = [
            'address.id.required'               => trans('address.id_required'),
            'address.id.exists'                 => trans('address.id_invalid'),
            'address.zipcode.required'          => trans('address.zipcode_required'),
            'address.zipcode.exists'            => trans('address.zipcode_invalid'),
            'address.address_type.required'     => trans('address.address_type_required'),
            'address.address_type.in'           => trans('address.address_type_invalid'),
            'address.label.required'            => trans('address.address_lable_required'),
            'address.lable.max'                 => trans('address_lable_max'),
            'address.address_1.required'        => trans('address.address1_required'),
            'address.company.required_if'       => trans('address.company_required'),
            'address.attempt_count.in'             => trans('address.address_add_attempt_count_required')
            
        ];
        $rules = [
            'address.id'                =>  'required|integer|exists:customer_address,id,user_id,'.$userId,
            'address.label'             =>  'required|max:150',
            'address.address_type'      =>  'required|in:'.config('constant.address')['residential'].','.config('constant.address')['business'],
            'address.company'           =>  'required_if:address.address_type,'.  config('constant.address')['business'].'|max:60',
            'address.address_1'         =>  'required|max:128',
            'address.address_2'         =>  'max:128',
            'address.zipcode'           =>  'required|exists:zipcode,zipcode',
            'address.attempt_count'     =>  'in:1'
            ];
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $addressData        = $request->only('address')['address'];
        $zipcodeInfo        = \App\Models\Zipcode::getItem($addressData['zipcode']);
        $params = array(
            "address"      => $addressData['address_1'], 
            "zipcode"      => $addressData['zipcode'], 
            "country"      => $zipcodeInfo->city->state->country->iso_code_2,
            "attempt_count" => $request->has('address.attempt_count') ? $addressData['attempt_count'] : 0
        );
        $geoResponse = $this->validateAddress($params);
        if ($geoResponse['error']) {
            return $this->response->errorUnwillingToProcess(trans($geoResponse['error_code']));
        }
        $address            = \App\Models\CustomerAddress::where('id', '=', $addressData['id'])->first();
        $address->fill($addressData);
        $address->state_id      =   $zipcodeInfo->city->state->id;
        $address->state         =   $zipcodeInfo->city->state->code;
        $address->country_id    =   $zipcodeInfo->city->state->country->id;
        $address->country       =   $zipcodeInfo->city->state->country->iso_code_2;
        $address->city          =   $geoResponse['city'];
        $address->lat           =   $geoResponse['geoloc']['geometry']['location']['lat'];
        $address->lng           =   $geoResponse['geoloc']['geometry']['location']['lng'];
        if( $address->save() ) {
            return $this->response->withItem($address, new \App\Transformer\AddressTransformer, 'data', ['http_code'=>200]);
        } else {
            return $this->response->errorInternalError(trans ('address.something_went_wrong'));
        }
        
    }
    
    /**
     * @api {delete} /api/v1/user/address Address Delete
     * @apiName Delete Address
     * @apiGroup Address
     * @apiDescription Delete Customer Address
     * @apiPermission Customer
     * 
     * @apiParam {Interger} address_id Address ID is required.
     * 
     * @apiError {Object} AddressIdRequired The <code>address_id</code> is required.
     * @apiError {Object} AddressIdInvalid The <code>address_id</code> is Invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     *  {
     *      "message": "Your address has been deleted successfully"
     *  }
     * 
     * @apiSuccess (200) {Object} The address has been deleted successfully.
     * @apiError (500) {Object} AddressNotDeleted The address can not be deleted.
     */
    public function deleteAddress(Request $request) {
        $authUser               = Auth::user(); 
        $userId                 = $authUser->id;
        
        $messages = [
            'id.required'       => trans('address.id_required'),
            'id.exists'         => trans('address.id_invalid'),
            'id.integer'        => trans('address.id_integer')
        ];
        $rules = [
            'id'        =>  'required|integer|exists:customer_address,id,user_id,'.$userId
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }     
        $customerAddress = \App\Models\CustomerAddress::where('id', '=', $request->input('id'))->first();
        if( $customerAddress->delete() ) {
            return $this->response->withArray(['message'=>  trans('address.address_deleted_successfully'), 'meta'=>['http_code'=>200]]);
        } else {
            return $this->response->errorInternalError(trans('address.something_went_wrong'));
        }
    }
    
    /**
     * @api {get} /api/v1/customers/addresses/warehouses Address With Warehouse
     * @apiName GetAddressesWithWarehouses
     * @apiGroup Warehouse
     * @apiDescription Get Warehouse's Ids list wich provide the service to the customer's address
     * @apiPermission Customer
     * 
     * @apiSuccess (200) {Object[]} Success The address list, Empty list if no address .
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": [
     *     {
     *       "2": [
     *           1,
     *           2,
     *           3,
     *           5,
     *           9,
     *           4
     *         ]
     *     }
     *     .......
     *   ],
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * @apiError (500) {Object} AddressNotFound The address can not be fetched with warehouses.
     * 
     */
    public function getAddressWarehouse(Request $request) {
        $userId     =   Auth::user()->id;
        $addresses  = \App\Models\CustomerAddress::getAddressWithWarehouse($userId);
        if($addresses) {
            return $this->response->withArray(['data'=> \App\Transformer\AddressTransformer::addressesWithWarehouses($addresses), 'meta'=> ['http_code'=>200]]);
        } else {
            return $this->response->errorInternalError(trans('address.something_went_wrong'));
        }
    }
    
}
