<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Validator;
use App\Serializer\ArrayIncludeSerializer;


class ZonesController extends BaseController
{   
    protected $model;
    
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    /**
    * @api {get} /api/v1/zones/availability Zipcode Services
    * @apiDescription Check Service Availability in Zipcode
    * @apiPermission none
    * 
    * @apiGroup Zone
    * @apiName  availability 
    *
    * @apiParam {Integer} zipcode  Zipcode (5 digits).
    * 
    * @apiError ZipcodeRequired The <code>zipcode</code> is required.
    * @apiError ZipcodeInvalid  The <code>zipcode</code> is invalid.
    * @apiError ZipcodeNotFound The <code>zipcode</code> was not found.
    * 
    * @apiSuccess {Boolean} service_status  Service Staus, 0=>Unavailable, 1=>available.
    * @apiSuccessExample {json} Success-Response:
    * {
            "data": {
                "zipcode": 78705,
                "zone_id": 1,
                "city": "Austin",
                "state": "Texas",
                "state_code": "TX",
                "service_status": "1"
            },
            "meta": {
                "http_code": 200
            }
        }
    */
    public function availability(Request $request) {
        $messages = [
            'zipcode.required'    => trans('zone.zipcode_required'),
            'zipcode.digits'      => trans('zone.zipcode_invalid'),
            'zipcode.exists'      => trans('zone.zipcode_invalid')
        ];
        
        $validator = Validator::make($request->all(), [
            'zipcode' => 'required|digits:5|exists:zipcode,zipcode'
        ], $messages);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $zipcode        = $request->input('zipcode');
        $zipcodeInfo    = \App\Models\Zipcode::serviceAvailable($zipcode);
        
        if(!$zipcodeInfo){
            return $this->response->errorNotFound(trans('zone.zipcode_not_found'));
        }
        return $this->response->withArray(['data'=>\App\Transformer\ZipcodeTransformer::transformWithAvailabilityContext($zipcodeInfo), 'meta'=>['http_code'=>200]]);
    }
    
    
    /**
    * @api {get} /api/v1/zones/warehouses Zonal Warehouses
    * @apiHeader {String} Authorization AuthToken.
    * @apiHeaderExample Authorization Header Example:
    *       "Authorization"="Bearer fAKSFJRER84SFSD.sdfsdf*#$BBFI"
    * @apiDescription Warehouse list for the zipcode with zonal settings
    * @apiPermission customer,guest
    * 
    * @apiGroup Zone
    * @apiName  warehouses 
    *
    * @apiParam {Integer}  zipcode Zipcode
    * 
    * @apiError ZipcodeRequired The <code>zipcode</code> is required.
    * @apiError ZipcodeInvalid  The <code>zipcode</code> is invalid.
    * @apiError ZipcodeNotFound The <code>Zipcode</code> was not found.
    * @apiError WarehousesNotFound The <code>Warehouse</code> was not found.
    * @apiSuccessExample {json} Success-Response:
    *  {
        "data": {
          "zipcode": 78705,
          "zone_id": 1,
          "warehouses": [
            {
              "id": 2,
              "name": "H-E-B",
              "slug": "h-e-b",
              "description": "",
              "description_html": "",
              "logo_url": "",
              "light_logo_url": "https://d2lnr5mha7bycj.cloudfront.net/warehouse/light_logo/45/2a6f7736-9ea6-43e3-9a91-afdfc05ccf05.png",
              "background_color": "#db1f18",
              "is_default": 1,
              "warehouseZonalServiceSchedule": {
                "1": [
                  {
                    "title": "Sunday",
                    "schedule": "9:00 AM-10:00 PM"
                  },
                  {
                    "title": "Monday",
                    "schedule": "9:00 AM-10:00 PM"
                  },
                  {
                    "title": "Tuesday",
                    "schedule": "9:00 AM-10:00 PM"
                  },
                  {
                    "title": "Wednesday",
                    "schedule": "9:00 AM-10:00 PM"
                  },
                  {
                    "title": "Thursday",
                    "schedule": "9:00 AM-10:00 PM"
                  },
                  {
                    "title": "Friday",
                    "schedule": "9:00 AM-10:00 PM"
                  }
                ],
                "2": [
                  {
                    "title": "Saturday",
                    "schedule": "9:00 AM-10:00 PM"
                  }
                ]
              },
              "warehouseZonalServices": [
                {
                  "warehouse_service_option_id": 1,
                  "is_default": 1,
                  "service_name": "Delivery",
                  "service_value": "delivery",
                  "service_description": "Delivery Service"
                },
                {
                  "warehouse_service_option_id": 2,
                  "is_default": 0,
                  "service_name": "Pickup",
                  "service_value": "pickup",
                  "service_description": "Pickup Service"
                }
              ]
            },
            {
              "id": 3,
              "name": "Whole Foods Market",
              "slug": "whole-foods",
              "description": "",
              "description_html": "",
              "logo_url": "",
              "light_logo_url": "https://d2lnr5mha7bycj.cloudfront.net/warehouse/light_logo/3/3f6a4e53-7449-4af8-96bc-c07fe2bc09b4.png",
              "background_color": "#83c341",
              "is_default": 0,
              "warehouseZonalServiceSchedule": [],
              "warehouseZonalServices": [
                {
                  "warehouse_service_option_id": 2,
                  "is_default": 1,
                  "service_name": "Pickup",
                  "service_value": "pickup",
                  "service_description": "Pickup Service"
                },
                {
                  "warehouse_service_option_id": 1,
                  "is_default": 0,
                  "service_name": "Delivery",
                  "service_value": "delivery",
                  "service_description": "Delivery Service"
                }
              ]
            },
            {
              "id": 5,
              "name": "Central Market",
              "slug": "central-market",
              "description": "",
              "description_html": "",
              "logo_url": "",
              "light_logo_url": "https://d2lnr5mha7bycj.cloudfront.net/warehouse/light_logo/49/b82573b0-1bd8-4b05-9ab6-7a558e4ff1f1.png",
              "background_color": "#0c6238",
              "is_default": 0,
              "warehouseZonalServiceSchedule": [],
              "warehouseZonalServices": []
            },
            {
              "id": 9,
              "name": "Costco",
              "slug": "costco",
              "description": "",
              "description_html": "",
              "logo_url": "",
              "light_logo_url": "https://d2lnr5mha7bycj.cloudfront.net/warehouse/light_logo/5/7c75d387-54ff-4295-a0dd-461d9dd13bc6.png",
              "background_color": "#0060a9",
              "is_default": 0,
              "warehouseZonalServiceSchedule": [],
              "warehouseZonalServices": []
            },
            {
              "id": 1,
              "name": "Walmart",
              "slug": "walmart",
              "description": "",
              "description_html": "",
              "logo_url": "",
              "light_logo_url": "",
              "background_color": "#00508f",
              "is_default": 0,
              "warehouseZonalServiceSchedule": [],
              "warehouseZonalServices": [
                {
                  "warehouse_service_option_id": 1,
                  "is_default": 1,
                  "service_name": "Delivery",
                  "service_value": "delivery",
                  "service_description": "Delivery Service"
                }
              ]
            }
          ]
        },
        "meta": {
          "http_code": 200
        }
      }
    * 
    * @apiSuccess (Zipcode) {Integer}    zipcode Zipcode
    * @apiSuccess (Zipcode) {Integer}    zone_id Zone id of the zipcode
    * @apiSuccess (Zipcode) {Object[]} warehouses List of warehouses in the zipcode zone
    * @apiSuccess (Zipcode) {Integer}  warehouses.id  Warehouse ID
    * @apiSuccess (Zipcode) {String}   warehouses.name  Warehouse Name
    * @apiSuccess (Zipcode) {String}   warehouses.slug  Warehouse Slug
    * @apiSuccess (Zipcode) {String}   warehouses.description Warehouse Description
    * @apiSuccess (Zipcode) {String}   warehouses.description_html Warehouse Description HTML
    * @apiSuccess (Zipcode) {String}   warehouses.logo_url  Warehouse Logo
    * @apiSuccess (Zipcode) {String}   warehouses.light_logo_url Warehouse Light Logo
    * @apiSuccess (Zipcode) {Object[]} warehouses.warehouseZonalServices  Services within the warehouse
    * @apiSuccess (Zipcode) {Integer}  warehouses.warehouseZonalServices.warehouse_service_option_id Service Option ID  
    * @apiSuccess (Zipcode) {Boolean}  warehouses.warehouseZonalServices.is_default Is default service 
    * @apiSuccess (Zipcode) {Object}   warehouses.warehouseZonalServices.warehouseServiceOption Service Option Detail
    * @apiSuccess (Zipcode) {String}   warehouses.warehouseZonalServices.warehouseServiceOption.name Service Option Name
    * @apiSuccess (Zipcode) {String}   warehouses.warehouseZonalServices.warehouseServiceOption.value Service Option Value
    * @apiSuccess (Zipcode) {Object[]} warehouses.warehouseZonalServices.warehouseZonalServiceSchedule Service Option Schedule    
    */
    public function warehouses(Request $request) {
        $messages = [
            'zipcode.required'    => trans('zone.zipcode_required'),
            'zipcode.digits'      => trans('zone.zipcode_invalid')
        ];
        $validator = Validator::make($request->all(), [
            'zipcode' => 'required|digits:5'
        ], $messages);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $zipcode            = $request->input('zipcode');
        $zipcodeInfo        = \App\Models\Zipcode::where('zipcode', '=', $zipcode)->where('service_status', '=', 1)->first();
        if(!$zipcodeInfo){
            return $this->response->errorNotFound(trans('zone.zipcode_not_found'));
        }
        $zipcodeWarehouse = \App\Models\Zipcode::getZonalWarehouses($zipcode, $zipcodeInfo->zone_id);
        if( !$zipcodeWarehouse ) {
            return $this->response->errorNotFound(trans('zone.warehouse_not_found'));
        }
        return $this->response->withArray(['data'=>\App\Transformer\ZipcodeWarehouseTransformer::transformWithWarehouseServiceContext($zipcodeWarehouse), 'meta'=>['http_code'=>200]]);  
    }
    
    /**
    * @api {get} /api/v1/zones/departments Zonal Warehouse Department
    * @apiHeader {String} Authorization AuthToken.
    * @apiHeaderExample Authorization Header Example:
    *       "Authorization"="Bearer fAKSFJRER84SFSD.sdfsdf*#$BBFI" 
    * @apiDescription Warehouse Department list for the zone
    * @apiPermission customer,guest
    * 
    * @apiGroup Zone
    * @apiName  departments 
    *
    * @apiParam {Integer}  zone_id Zipcode
    * @apiParam {Integer}  warehouse_id Warehouse Id
    * 
    * @apiError ZoneIDRequired The <code>zone_id</code> is required.
    * @apiError ZoneIDInvalid  The <code>zone_id</code> is invalid.
    * @apiError ZoneNotFound The <code>zone</code> was not found.
    * @apiError WarehouseIDRequired The <code>warehouse_id</code> is required.
    * @apiError WarehouseIDInvalid  The <code>warehouse_id</code> is invalid.
    * @apiError WarehouseNotFound The <code>warehouse</code> was not found. 
    * @apiSuccessExample {json} Success-Response
    * {
                "data": [
                  {
                    "departments": [
                        {
                          "id": 69,
                          "name": "Breakfast",
                          "warehouse_id": "2",
                          "image_url": null,
                          "parent_id": "0",
                          "number_of_column": "1",
                          "item_per_column": null,
                          "sort_order": "10",
                          "rank_offset": "0",
                          "status": "1",
                          "created_at": "2016-05-12 00:00:00",
                          "updated_at": "2016-05-12 00:00:00",
                          "sort_order_zone": "-1",
                          "rank_offset_zone": "-1",
                          "zone_aisles": [
                            {
                              "id": 73,
                              "name": "Breakfast Bars & Pastries",
                              "warehouse_id": "2",
                              "image_url": null,
                              "parent_id": "69",
                              "number_of_column": "1",
                              "item_per_column": null,
                              "sort_order": "73",
                              "rank_offset": "0",
                              "status": "1",
                              "created_at": "2016-05-12 00:00:00",
                              "updated_at": "2016-05-12 00:00:00",
                              "sort_order_zone": "-1",
                              "rank_offset_zone": "-1"
                            },
                            {
                              "id": 72,
                              "name": "Granola",
                              "warehouse_id": "2",
                              "image_url": null,
                              "parent_id": "69",
                              "number_of_column": "1",
                              "item_per_column": null,
                              "sort_order": "72",
                              "rank_offset": "0",
                              "status": "1",
                              "created_at": "2016-05-12 00:00:00",
                              "updated_at": "2016-05-12 00:00:00",
                              "sort_order_zone": "-1",
                              "rank_offset_zone": "-1"
                            }
                           ]
                          }
                        },
                        {
                          "id": 74,
                          "name": "Snacks",
                          "warehouse_id": "2",
                          "image_url": null,
                          "parent_id": "0",
                          "number_of_column": "1",
                          "item_per_column": null,
                          "sort_order": "13",
                          "rank_offset": "0",
                          "status": "1",
                          "created_at": "2016-05-12 00:00:00",
                          "updated_at": "2016-05-12 00:00:00",
                          "sort_order_zone": "-1",
                          "rank_offset_zone": "-1",
                          "zone_aisles": [
                            .... 
                          ]
                        }
                        ....
                    ]
                  }
                ],
                "meta": {
                  "http_code": 200
                }
              }
    * 
    * @apiSuccess (Zipcode) {Object[]} departments List of departments in the zone for the warehouse
    * @apiSuccess (Zipcode) {Integer}  departments.id    Department ID
    * @apiSuccess (Zipcode) {String}   departments.name  Department Name
    * @apiSuccess (Zipcode) {Integer}  departments.warehouse_id  Department Warehouse ID
    * @apiSuccess (Zipcode) {String}   departments.image_url Department Image
    * @apiSuccess (Zipcode) {Integer=0}  departments.parent_id Department Parent ID
    * @apiSuccess (Zipcode) {String}   departments.number_of_column  Department Columns Layout
    * @apiSuccess (Zipcode) {String}   departments.item_per_column Aisles per column
    * @apiSuccess (Zipcode) {String}   departments.sort_order Department Sort Order
    * @apiSuccess (Zipcode) {String}   departments.rank_offset Department Rankoffset
    * @apiSuccess (Zipcode) {String}   departments.sort_order_zone Department Zonal Sort Order
    * @apiSuccess (Zipcode) {String}   departments.rank_offset_zone Department Zonal Rankoffset
    * @apiSuccess (Zipcode) {Object[]} departments.zonalAisles  Aisles within the zone for the warehouse department
    * @apiSuccess (Zipcode) {Integer}  departments.zonalAisles.id Aisle ID  
    * @apiSuccess (Zipcode) {String}   departments.zonalAisles.name  Aisle Name
    * @apiSuccess (Zipcode) {Integer}  departments.zonalAisles.warehouse_id  Aisle Warehouse ID
    * @apiSuccess (Zipcode) {String}   departments.zonalAisles.image_url Aisle Image
    * @apiSuccess (Zipcode) {Integer}  departments.zonalAisles.parent_id Aisle Parent ID
    * @apiSuccess (Zipcode) {String}   departments.zonalAisles.number_of_column  Aisle Columns Layout
    * @apiSuccess (Zipcode) {String}   departments.zonalAisles.item_per_column Aisles per column
    * @apiSuccess (Zipcode) {String}   departments.zonalAisles.sort_order Aisle Sort Order
    * @apiSuccess (Zipcode) {String}   departments.zonalAisles.rank_offset Aisle Rankoffset
    * @apiSuccess (Zipcode) {String}   departments.zonalAisles.sort_order_zone Aisle Zonal Sort Order
    * @apiSuccess (Zipcode) {String}   departments.zonalAisles.rank_offset_zone Aisle Zonal Rankoffset
    */
    public function departments(Request $request) { 
        
        $messages = [
            'zone_id.required'          => trans('zone.zone_id_required'),
            'zone_id.integer'           => trans('zone.zone_id_invalid'),
            'warehouse_id.required'     => trans('zone.warehouse_id_required'),
            'warehouse_id.integer'      => trans('zone.warehouse_id_invalid')
        ];
        $validator = Validator::make($request->all(), [
            'zone_id'       => 'required|integer',
            'warehouse_id'  => 'required|integer'
        ], $messages);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $zoneId      = $request->input('zone_id');
        $warehouseId = $request->input('warehouse_id');
        
        $options                        = [];
        $options['departmentOrderBy']   = [['sort_order_zone', 'desc']];
        $departments                    = \App\Models\Zone::getDepartments($zoneId, $warehouseId, $options);
        if( !$departments ) {
            return $this->response->errorNotFound(trans('zone.department_not_found'));
        }
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($departments, new \App\Transformer\ZoneDepartmentTransformer,
                'data', null, ['http_code'=>200]);
    }
    
    /**
    * @api {get} /api/v1/zones/warehouses/locations Warehouse Zonal Locations
    * @apiHeader {String} Authorization AuthToken.
    * @apiHeaderExample Authorization Header Example:
    *       "Authorization"="Bearer fAKSFJRER84SFSD.sdfsdf*#$BBFI" 
    * @apiDescription Warehouse Zonal Location List
    * @apiPermission customer,guest
    * 
    * @apiGroup Zone
    * @apiName  warehouseLocations 
    *
    * @apiParam {Integer}  zone_id Zone ID
    * @apiParam {Integer}  warehouse_id Warehouse Id
    * 
    * @apiError ZoneIDRequired The <code>zone_id</code> is required.
    * @apiError ZoneIDInvalid  The <code>zone_id</code> is invalid.
    * @apiError WarehouseIDRequired The <code>warehouse_id</code> is required.
    * @apiError WarehouseIDInvalid  The <code>warehouse_id</code> is invalid.
    * @apiError WarehouseLocationNotFound The <code>warehouse location</code> was not found. 
    * @apiSuccessExample {json} Success-Response 
    * {
        "data": [
          {
            "id": 19,
            "title": "16900 N. FM 620",
            "address_1": "16900 N. FM 620",
            "address_2": "",
            "zipcode": "78681",
            "city": "Round Rock",
            "state": "Texas",
            "country": "US",
            "lat": "30.48240155",
            "lng": "-97.65994303",
            "sort_order": "17",
            "warehouseZonalLocationServices": [
                  {
                    "warehouse_service_option_id": "1",
                    "service_name": "Delivery",
                    "service_value": "delivery",
                    "service_description": "Delivery Service"
                  }
            ]
          },
          {
            "id": 3,
            "title": "6900 Brodie Lane",
            "address_1": "6900 Brodie Lane",
            "address_2": "",
            "zipcode": "78745",
            "city": "Austin",
            "state": "Texas",
            "country": "US",
            "lat": "30.21728325",
            "lng": "-97.83182526",
            "sort_order": "1",
            "warehouseZonalLocationServices": [
                 {
                    "warehouse_service_option_id": "1",
                    "service_name": "Delivery",
                    "service_value": "delivery",
                    "service_description": "Delivery Service"
                 }
            ]
          }
        ],
        "meta": {
          "http_code": 200
        }
      }
    * 
    * @apiSuccess (Warehouse Location) {Object[]} warehouseLocation Warehouse Location List 
    * @apiSuccess (Warehouse Location) {Integer}  warehouseLocation.id    Warehouse Location ID
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.title  Warehouse Location Title
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.adddress_1  Warehouse Location Address 1
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.address_2 Warehouse Location Address 2
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.zipcode Warehouse Location Zipcode
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.city  Warehouse Location City
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.state Warehouse Location State
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.country Warehouse Location Country
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.lat Warehouse Location latitude
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.lng Warehouse Location longitude 
    * @apiSuccess (Warehouse Location) {String}   warehouseLocation.sort_order Warehouse Location Sort Order
    * @apiSuccess (Warehouse Location) {Object[]} warehouseLocation.warehouseZonalLocationServices  Warehouse Location Services
    */
    public function warehouseLocations(Request $request) {
        
        $messages = [
            'zone_id.required'          => trans('zone.zone_id_required'),
            'zone_id.integer'           => trans('zone.zone_id_invalid'),
            'warehouse_id.required'     => trans('zone.warehouse_id_required'),
            'warehouse_id.integer'      => trans('zone.warehouse_id_invalid')
        ];
        $validator = Validator::make($request->all(), [
            'zone_id'       => 'required|integer',
            'warehouse_id'  => 'required|integer'
        ], $messages);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $zoneId         = $request->input('zone_id');
        $warehouseId    = $request->input('warehouse_id');
        
        $zonalLocations = \App\Models\WarehouseZonalLocation::getAllZonalLocation($zoneId, $warehouseId);
        
        if(!$zonalLocations) {
            $this->response->errorNotFound( trans('warehouse.location_not_found'));
        }
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($zonalLocations, new \App\Transformer\WarehouseZonalLocationTransformer,
                'data', null, ['http_code'=>200]);
    }
    
}
