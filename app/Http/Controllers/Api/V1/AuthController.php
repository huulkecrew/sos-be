<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use EllipseSynergie\ApiResponse\Contracts\Response;
use URL;
use \Illuminate\Support\Facades\Hash;
use Socialite;
use App\Http\Traits\UserUtils;
use App\Serializer\ArrayIncludeSerializer;

class AuthController extends BaseController {

    use UserUtils;

    public function __construct(Response $response) {
        parent::__construct($response);
    }

    /**
     * @api {post} /login User Login
     * @apiDescription User Login Authentication of User
     * @apiPermission none
     *
     * @apiGroup User
     * @apiName  login
     *
     * @apiParam {String{..96}}    user[email]    Email
     * @apiParam {String}          user[password] Password
     * @apiParam {String}          _token         CSRF token
     *
     *
     * @apiError EmailRequired    <code>Email</code> address is required.
     * @apiError EmailInvalid     <code>Email</code> address is invalid.
     * @apiError EmailInvalidMax  <code>Email</code> address can be of xx characters.
     * @apiError PasswordRequired <code>Password</code> is required.
     * @apiError InvlalidCredentials Invalid email or password.
     *
     * @apiSuccessExample {json} Success-Response:
     * {
      "data": {
      "user_id": 74,
      "firstname": "Ap",
      "lastname": "Pa",
      "email": "abhiveeru85@gmail.com",
      "contact_email": "abhiveeru85@gmail.com",
      "country_code": null,
      "phone": "7753011886",
      "newsletter": 0,
      "avatar_url": null,
      "user_status_id": 3,
      "approved": 0,
      "last_login_at": "2016-10-07 08:21:14",
      "registered_at": "2016-05-18 11:57:11",
      "updated_at": "2016-10-07 08:21:14"
      },
      "meta": {
      "http_code": 200,
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjk3LCJpc3MiOiJodHRwOlwvXC93d3cuYmV0YWJ1cnB5LmNvbVwvcmVnaXN0ZXIiLCJpYXQiOjE0NjQ3MDc0MDYsImV4cCI6MTQ2NDcxMTAwNiwibmJmIjoxNDY0NzA3NDA2LCJqdGkiOiJjYWNmNWQ2ZWY2NTI5ZGQ1ODNlZWI3YjUzZDVhYjZiMyJ9.N4sJnxGQeKeTi6dvvZkZEUXKrYWNk9Mx2WtrH_ZCLU0"
      }
      }
     *
     * @apiSuccess {string}  token Auth Token
     * @apiSuccess {Object}  user  Information of the logged in user
     */
    public function login(Requests\AuthWithEmailRequest $request) {
        $authParams = $request->only('user');
        try {
            //verify the credentials and create a token for the user
            $credentials = ['email' => $authParams['user']['email'],
                'password' => $authParams['user']['password'],
                'user_type_id' => config('constant.user_type')['customer'],
                'user_status_id' => 3];
            if (!$token = JWTAuth::attempt($credentials)) {
                //FIX FOR LEGACY SYSTEM
                $legacyUser = \App\Models\User::where('email', $authParams['user']['email'])
                                ->whereNull('password')
                                ->where('legacy_password', md5($authParams['user']['password']))
                                ->first();
                if ($legacyUser && $legacyUser->id <= config('constant.legacy_user_id')) {
                    $user           = new \App\Models\User;
                    $user->password = Hash::make($authParams['user']['password']);
                    $user->save();
                    if (!$token = JWTAuth::attempt($credentials)) {
                        return $this->response->errorUnauthorized(trans('auth.invalid_credentials'));
                    }
                } else {
                    return $this->response->errorUnauthorized(trans('auth.invalid_credentials'));
                }
            }
        } catch (JWTException $e) {
            return $this->response->errorInternalError(trans('auth.could_not_create_token'));
        }
        $authUser = Auth::user();
        $this->postLoginActions($authUser); //It's not an event observer
        return $this->response->withItem($authUser, new \App\Transformer\UserTransformer, 'data', ['http_code' => 200, 'token' => $token]);
    }
    
    /**
     * @api {post} /socail_callback User Social Login
     * @apiDescription User Login Authentication of User
     * @apiPermission none
     *
     * @apiGroup User
     * @apiName  login
     *
     * @apiParam {String{..96}}    user[email]    Email
     * @apiParam {String}          user[password] Password
     * @apiParam {String}          _token         CSRF token
     *
     *
     * @apiError EmailRequired    <code>Email</code> address is required.
     * @apiError EmailInvalid     <code>Email</code> address is invalid.
     * @apiError EmailInvalidMax  <code>Email</code> address can be of xx characters.
     * @apiError PasswordRequired <code>Password</code> is required.
     * @apiError InvlalidCredentials Invalid email or password.
     *
     * @apiSuccessExample {json} Success-Response:
     * {
      "data": {
      "user_id": 74,
      "firstname": "Ap",
      "lastname": "Pa",
      "email": "abhiveeru85@gmail.com",
      "contact_email": "abhiveeru85@gmail.com",
      "country_code": null,
      "phone": "7753011886",
      "newsletter": 0,
      "avatar_url": null,
      "user_status_id": 3,
      "approved": 0,
      "last_login_at": "2016-10-07 08:21:14",
      "registered_at": "2016-05-18 11:57:11",
      "updated_at": "2016-10-07 08:21:14"
      },
      "meta": {
      "http_code": 200,
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjk3LCJpc3MiOiJodHRwOlwvXC93d3cuYmV0YWJ1cnB5LmNvbVwvcmVnaXN0ZXIiLCJpYXQiOjE0NjQ3MDc0MDYsImV4cCI6MTQ2NDcxMTAwNiwibmJmIjoxNDY0NzA3NDA2LCJqdGkiOiJjYWNmNWQ2ZWY2NTI5ZGQ1ODNlZWI3YjUzZDVhYjZiMyJ9.N4sJnxGQeKeTi6dvvZkZEUXKrYWNk9Mx2WtrH_ZCLU0"
      }
      }
     *
     * @apiSuccess {string}  token Auth Token
     * @apiSuccess {Object}  user  Information of the logged in user
     */
    public function social_callback(Request $request) {
        $socialUser = $request->only('user')['user'];
        try {
            $socialUser['ip'] = $request->ip();
            $provider         = $socialUser['provider'];
            $this->findOrCreateUser($socialUser, $provider);
            $credentials      = [$provider.'_uid' => $socialUser['id'],
                                'user_type_id'    => config('constant.user_type')['customer'],
                                'user_status_id'  => 3];
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->response->errorUnauthorized(trans('auth.could_not_create_token'));
            }
        } catch (Exception $e) {
            return $this->response->errorInternalError(trans('auth.could_not_create_token'));
        }
        $authUser = Auth::user();
        return $this->response->withItem($authUser, new \App\Transformer\UserTransformer, 'data', ['http_code' => 200, 'token' => $token]);
    }

    /**
     * @api {get} /auth/facebook Facebook Auth
     * @apiDescription User Auth from Facebook [Not to be used from app, Use js skd instead]
     * @apiPermission none
     *
     * @apiGroup User
     * @apiName  /auth/facebook
     */
    public function facebook() {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * @api {post} /auth/facebook/callback Facebook Auth-Callback
     * @apiDescription Facebook Auth Callback after authentication from client
     * @apiPermission none
     *
     * @apiGroup User
     * @apiName  /auth/facebook/callback
     */
    public function facebook_callback(Request $request) {
        $socialUser = Socialite::with('facebook')->user();
        if (!$socialUser || !$socialUser->id) {
            return $this->response->errorInternalError(trans('auth.could_not_create_token'));
        }
        try {
            $socialUser->ip = $request->ip();
            $user           = $this->findOrCreateUser($socialUser, 'facebook');
            $credentials = ['facebook_uid' => $socialUser->id,
                            'user_type_id' => config('constant.user_type')['customer'],
                            'user_status_id' => 3];
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->response->errorUnauthorized(trans('auth.could_not_create_token'));
            }
        } catch (Exception $e) {
            return $this->response->errorInternalError(trans('auth.could_not_create_token'));
        }
        $authUser = Auth::user();
        return $this->response->withItem($authUser, new \App\Transformer\UserTransformer, 'data', ['http_code' => 200, 'token' => $token]);
    }

    /**
     * @api {get} /auth/twitter Twitter Auth
     * @apiDescription User Auth from Twitter [Not to be used from app, Use js skd instead]
     * @apiPermission none
     *
     * @apiGroup User
     * @apiName  /auth/twitter
     */
    public function twitter() {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * @api {post} /auth/twitter/callback Twitter Auth-Callback
     * @apiDescription Twitter Auth Callback  after authentication from client
     * @apiPermission none
     *
     * @apiGroup User
     * @apiName  /auth/twitter/callback
     */
    public function twitter_callback(Request $request) {

        $socialUser = Socialite::with('twitter')->user();
        if (!$socialUser || !$socialUser->id) {
            return $this->response->errorInternalError(trans('auth.could_not_create_token'));
        }
        try {
            $socialUser->ip = $request->ip();
            $user = $this->findOrCreateUser($socialUser, 'twitter');
             $credentials = ['twitter_uid' => $socialUser->id,
                            'user_type_id' => config('constant.user_type')['customer'],
                            'user_status_id' => 3];
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->response->errorUnauthorized(trans('auth.could_not_create_token'));
            }
        } catch (Exception $e) {
            return $this->response->errorInternalError(trans('auth.could_not_create_token'));
        }
        $authUser = Auth::user();
        return $this->response->withItem($authUser, new \App\Transformer\UserTransformer, 'data', ['http_code' => 200, 'token' => $token]);
    }

    /**
     * @api {post} /register User Registration
     * @apiDescription User Registration
     * @apiPermission none
     *
     * @apiGroup User
     * @apiName  registration
     *
     * @apiParam {String{..32}}    user[firstname] Firstname
     * @apiParam {String{..32}}    user[lastname]  Lastname
     * @apiParam {String{..96}}    user[email]     Email
     * @apiParam {String{6..20}}   user[password]  Password
     * @apiParam {String="web","app","mobile-web"}    user[soruce] Platform source
     * @apiParam {String}          user[zipcode] Optional, send if zipcode has been taken earlier from user
     * @apiParam {String}          _token         CSRF token
     *
     * @apiError EmailRequired <code>Email</code> address is required.
     * @apiError EmailInvalid  <code>Email</code> address is invalid.
     * @apiError EmailInvalidMax  <code>Email</code> address can be of 96 characters.
     * @apiError PasswordRequired <code>Password</code> is required.
     * @apiError FirstnameRequired <code>Firstname</code> is required.
     * @apiError FirstnameInvalid  <code>Firstname</code> is invalid.
     * @apiError LastnameRequired <code>Lastname</code> is required.
     * @apiError LastnameInvalid  <code>Lastname</code> is invalid.
     *
     * @apiSuccess {string}  token Auth Token
     * @apiSuccess {Object}  user  Information of the logged in user
     *
     * * @apiSuccessExample {json} Success-Response:
     * {
      "data": {
      "user_id": 97,
      "firstname": "Abhishek",
      "lastname": "Pandey",
      "email": "abhiveerdu854qq@gmail.com",
      "contact_email": "abhiveerdu854qq@gmail.com",
      "country_code": null,
      "phone": null,
      "newsletter": 0,
      "avatar_url": null,
      "user_status_id": 3,
      "approved": 0,
      "last_login_at": null,
      "registered_at": "2016-05-31 15:10:00",
      "updated_at": "2016-05-31 15:10:00"
      },
      "meta": {
      "http_code": 200,
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjk3LCJpc3MiOiJodHRwOlwvXC93d3cuYmV0YWJ1cnB5LmNvbVwvcmVnaXN0ZXIiLCJpYXQiOjE0NjQ3MDc0MDYsImV4cCI6MTQ2NDcxMTAwNiwibmJmIjoxNDY0NzA3NDA2LCJqdGkiOiJjYWNmNWQ2ZWY2NTI5ZGQ1ODNlZWI3YjUzZDVhYjZiMyJ9.N4sJnxGQeKeTi6dvvZkZEUXKrYWNk9Mx2WtrH_ZCLU0"
      }
      }
     */
    public function register(Requests\RegistrationRequest $request) {
        $user                 = new \App\Models\User;
        $user->fill($request->user);
        $user->contact_email  = $request->user['email'];
        $user->ip             = $request->ip();
        $user->password       = Hash::make($request->user['password']);
        $user->user_type_id   = config('constant.user_type')['customer'];
        $user->user_status_id = config('constant.user_default_status')['customer'];
        $params               = $request->has('user') ? $request->user : [];
        $userType             = 'customer';
        $profileParams        = $this->getUserProfileParams($params);
        $referralParams       = $this->getReferralParams($user);
        try {
            $user = $this->bootstrapUser($user, $userType, $profileParams, $referralParams);
        } catch (Exception $e) {
            return $this->response->errorInternalError(trans('auth.could_not_create_user'));
        }
        $credentials = ['email' => $params['email'],
                        'password' => $params['password'],
                        'user_type_id' => config('constant.user_type')['customer'],
                        'user_status_id' => config('constant.user_default_status')['customer']];
        if (!$token = JWTAuth::attempt($credentials)) {
            return $this->response->errorUnauthorized(trans('auth.could_not_create_token'));
        }
        event(new \App\Events\UserRegistered($user));
        $authUser = Auth::user();
        return $this->response->withItem($authUser, new \App\Transformer\UserTransformer, 'data', ['http_code' => 200, 'token' => $token]);
    }

    /**
     * @api {post} /guestregister Guest Registration
     * @apiDescription User Registration as guest, Will be called when user select browse first feature
     * @apiPermission none
     *
     * @apiGroup User
     * @apiName  guestregistration
     *
     * @apiParam {String="web","app","mobile-web"}    user[soruce] Platform source
     * @apiParam {String}          user[zipcode] Optional, send if zipcode has been taken earlier from user
     * @apiParam {String}          _token         CSRF token
     *
     *
     * @apiSuccess {string}  token Auth Token
     * @apiSuccess {Object}  guest  Information
     */
    public function registerGuest(Request $request) {

        $user                   = new \App\Models\User;
        $unique_token           = md5(uniqid('GU_' . rand(10, 999) . '_' . $request->ip() . '_' . rand(10, 999), true));
        $user->firstname        = 'Guest';
        $user->lastname         = 'User';
        $user->email            = 'guest_' . $unique_token . '@burpyguest.com';
        $user->contact_email    = 'guest_' . $unique_token . '@burpyguest.com';
        $user->password         = Hash::make($unique_token);
        $user->ip               = $request->ip();
        $user->source           = $request->user['source'];
        $user->user_type_id     = config('constant.user_type')['customer'];
        $user->user_status_id   = config('constant.user_default_status')['guest'];
        $user->is_guest         = 1;
        $params                 = $request->has('user') ? $request->user : [];
        $userType               = 'guest';
        $profileParams          = $this->getUserProfileParams($params);
        try {
            $this->bootstrapUser($user, $userType, $profileParams);
        } catch (Exception $e) {
            return $this->response->errorInternalError(trans('auth.could_not_create_user'));
        }
        $credentials = ['email'          => $params['email'],
                        'password'       => $params['password'],
                        'user_type_id'   => config('constant.user_type')['guest'],
                        'user_status_id' => config('constant.user_default_status')['guest']];
        if (!$token = JWTAuth::attempt($credentials)) {
            return $this->response->errorUnauthorized(trans('auth.could_not_create_token'));
        }
        $authUser = Auth::user();
        return $this->response->withItem($authUser, new \App\Transformer\UserTransformer, 'data', ['http_code' => 200, 'token' => $token]);
    }

    /**
     * @api {post} /guesttocustomer Guest to Customer
     * @apiName Guest To Customer
     * @apiGroup User
     * @apiDescription Convert Guest to Customer
     * @apiPermission Guest
     *
     * @apiParam {String}  user[firstname] Firstname is required.
     * @apiParam {String}  user[lastname] Lasstname is required.
     * @apiParam {String}  user[username] Username is optional.
     * @apiParam {String}  user[email] Email address is required.
     * @apiParam {String}  user[password] Password is required.
     * @apiParam {String}  user[confirm_password] ConfirmPassword is required.
     * @apiParam {String}  user[phone] Phone is optional
     * @apiParam {String}  user[zipcode] Zipcode is required.
     *
     * @apiError {Object[]} FirstnameRequired The <code>firstname</code> is required.
     * @apiError {Object[]} FirstnameInvalid  The <code>firstname</code> is invalid.
     * @apiError {Object[]} LastnameRequired The <code>lastname</code> is required.
     * @apiError {Object[]} LastnameInvalid  The <code>lastname</code> is invalid.
     * @apiError {Object[]} EmailRequired The <code>email</code> is required.
     * @apiError {Object[]} EmailInvalid  The <code>email</code> is invalid.
     * @apiError {Object[]} PasswordRequired The <code>password</code> is required.
     * @apiError {Object[]} PasswordInvalid  The <code>password</code> is invalid.
     * @apiError {Object[]} ConfirmPasswordRequired The <code>confirm_password</code> is required.
     * @apiError {Object[]} ConfirmPasswordInvalid  The <code>confirm_password</code> is not same as password.
     * @apiError {Object[]} ZipcodeRequired The <code>zipcode</code> is required.
     * @apiError {Object[]} ZipcodeInvalid  The <code>zipcode</code> is invalid.
     *
     * @apiSuccessExample {json} Success-Response:
     * {
      "data": {
      "user_id": 77,
      "firstname": "Prem",
      "lastname": "Baboo",
      "email": "abc2016@gmail.com",
      "contact_email": "abc2016@gmail.com",
      "country_code": null,
      "phone": "9015613691",
      "newsletter": 0,
      "avatar_url": null,
      "user_status_id": 3,
      "approved": 0,
      "last_login_at": "2016-06-09 19:23:15",
      "registered_at": "2016-06-09 19:23:15",
      "updated_at": "2016-06-09 19:23:15"
      },
      "meta": {
      "http_code": 200,
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjc3LCJpc3MiOiJodHRwOlwvXC9iZXRhYnVycHkuY29tXC9hcGlcL3YxXC9ndWVzdHRvY3VzdG9tZXIiLCJpYXQiOjE0NjU1MDAxOTksImV4cCI6MTQ2NTUwMzc5OSwibmJmIjoxNDY1NTAwMTk5LCJqdGkiOiI2MDY4M2FiZjk2MDRlMTRlNDQ2ZGFmZjBmZDZiNzc1NCJ9.U2d_3m58A-DKXYQ6fLcmQMaaySOjrrpx7QEpRMbsE7k"
      }
      }
     * @apiSuccess (200) {Object} Success Guest converted to customer successfully.
     * @apiError (401) {Object} Not authorised.
     * @apiError (500) {Object} Internal Server Error.
     */
    public function convertGuestToCustomer(Requests\RegistrationRequest $request) {
        $authUser = Auth::user();
        $userId = $authUser->id;
        $user = \App\Models\User::find($userId);
        $requestData = $request->only('user')['user'];
        $user->email = $requestData['email'];
        $user->firstname = $requestData['firstname'];
        $user->lastname = $requestData['lastname'];
        $user->contact_email = $requestData['email'];
        $user->ip = $request->ip();
        $user->password = Hash::make($requestData['password']);
        $user->user_type_id = config('constant.user_type')['customer'];
        $user->user_status_id = config('constant.user_default_status')['customer'];
        $user->is_guest = 0;
        $user->token_invalidated_at = \Carbon\Carbon::now()->getTimestamp();
        $userType = 'customer';
        $referralParams = $this->getReferralParams($user);
        try {
            DB::transaction(function() use ($user, $userType, $referralParams) {
                $user->save();
                $user->roles()->sync([config('constant.user_default_role')[$userType]]);
                $user->userReferralCode()->save(new \App\Models\UserReferralCode($referralParams));
            });
        } catch (Exception $e) {
            return $this->response->errorInternalError(trans('auth.could_not_create_user'));
        }
        if (!$token = JWTAuth::fromUser($user)) {
            return $this->response->errorUnauthorized(trans('auth.could_not_create_token'));
        }
        event(new \App\Events\UserRegistered($user));
        $authUser = Auth::user();
        return $this->response->withItem($authUser, new \App\Transformer\UserTransformer, 'data', ['http_code' => 200, 'token' => $token]);
    }
    
      /**
    * @api {get} /csrf-token CSRF Token
    * @apiSampleRequest /csrf-token
    * @apiDescription Get CSRF Token
    * @apiPermission none
    * @apiGroup User
    * @apiName  csrfToken
    *
    *
    *
    * @apiSuccess {string}  token New CSRF Token
    */
    public function csrfToken(){
        return $this->response->withArray(['token'=> csrf_token()]);
    }

    /**
     * @api {get} /api/v1/user User Detail
     * @apiDescription Get Auth User Details
     * @apiPermission customer,guest
     *
     * @apiGroup User
     * @apiName  user
     *
     * @apiHeader {String} Authorization AuthToken.
     * @apiHeaderExample Authorization Header Example:
     *       "Authorization"="Bearer fAKSFJRER84SFSD.sdfsdf*#$BBFI"
     * @apiError TokenInvalid Invalid Token.
     *
     * @apiSuccess {Object}  User  Information
     */
    public function fetch() {

        $authUser = Auth::user();
        $userProfile = \App\Models\User::getUserWithProfile($authUser->id);
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer())->parseIncludes('customerProfile,bootstrapCart,userReferralCode,roles');
        if ($userProfile) {
            return $this->response->withArray(['data' => \App\Transformer\UserTransformer::transformWithCustomerContext($userProfile), 'meta' => ['http_code' => 200]]);
        } else {
            return $this->response->errorNotFound(trans('auth.user_not_found'));
        }
    }

    /**
     * @api {post} /auth/refresh-token Token Refresh
     * @apiDescription Refresh Token, Tokens are short live, api needs to be called each time app is opened
     *                 and each hour
     *
     * @apiGroup User
     * @apiName  refresh-token
     *
     * @apiHeader {String} Authorization AuthToken.
     * @apiHeaderExample Authorization Header Example:
     *       "Authorization"="Bearer fAKSFJRER84SFSD.sdfsdf*#$BBFI"
     * @apiError TokenInvalid Invalid Token.
     *
     * @apiSuccess {string}  token New Auth Token
     */
    public function refreshToken() {
        try {
            $token = JWTAuth::getToken();
            if (!$refresh_token = JWTAuth::refresh($token)) {
                return $this->response->errorUnauthorized(trans('auth.token_invalid'));
            }
        } catch (JWTException $e) {
            return $this->response->errorInternalError(trans('auth.could_not_create_token'));
        }
        return $this->response->withArray(['token' => $refresh_token]);
    }

    /**
     * @api {post} /api/v1/firebase-token Firebase Auth Token
     * @apiHeader {String} Authorization AuthToken.
     * @apiHeaderExample Authorization Header Example:
     *       "Authorization"="Bearer fAKSFJRER84SFSD.sdfsdf*#$BBFI"
     * @apiDescription Get Firebase Auth Token, Used to login to firebase via custom auth and save data
     * @apiPermission customer,guest
     * @apiGroup User
     * @apiName  firebaseToken
     *
     *
     *
     * @apiSuccess {string}  token New Firebase Token
     */
    public function firebaseToken() {
        $authUser = Auth::user();
        return $this->response->withArray(['firebaseToken' => $this->generateFirebaseAuthToken($authUser->id, strtotime('+15 days'))]);
    }

    /**
     * Method to create or update the socail user
     *
     */
    public function findOrCreateUser($authUser, $provider) {
        $userInfo = \App\Models\User::where($provider . '_uid', '=', $authUser['id'])
                ->where('user_type_id', '=', config('constant.user_type')['customer'])
                ->first();
        if (!$userInfo && $authUser['email']) {
            $userInfo = \App\Models\User::where('email', '=', $authUser['email'])
                    ->where('user_type_id', '=', config('constant.user_type')['customer'])
                    ->first();
        }
        if ($userInfo && $userInfo->user_status_id != 3) {
            throwException(new Exception(trans('auth.user_blocked')));
        }
        $name = $authUser['name'];
        list($firstname, $lastname) = explode(' ', trim($name), 2);
        if (!$userInfo) {
            $user                       = new \App\Models\User();
            $user->{$provider . '_uid'} = $authUser['id'];
            $user->firstname            = $authUser['firstname'];
            $user->lastname             = $authUser['lastname'];
            $user->contact_email        = $authUser['email'];
            $user->avatar_url           = $authUser['avatar'];
            $user->ip                   = $authUser['ip'];
            $user->password             = Hash::make(uniqid(rand(10, 999) . '_' . $request->ip() . '_' . rand(10, 999), true));
            $user->user_type_id         = config('constant.user_type')['customer'];
            $user->user_status_id       = config('constant.user_default_status')['customer'];
            $user->approved             = 1;
            $user->is_social            = 1;
        } else {
            $userInfo->avatar_url           = $authUser['avatar'];
            $userInfo->{$provider . '_uid'} = $authUser['id'];
            $userInfo->token_invalidated_at = NULL;
            $userInfo->last_login_at        = \Carbon\Carbon::now();
        }
        if (!$userInfo) {
            //TO DO Find way to pass zipcode & timezone
            $userType       = 'customer';
            $profileParams  = $this->getUserProfileParams();
            $referralParams = $this->getReferralParams($user);
            $this->bootstrapUser($user, $userType, $profileParams, $referralParams);
            event(new \App\Events\UserRegistered($user));
        } else {
            $userInfo->save();
            return $userInfo;
        }
        return $user;
    }

    /*
     * Method to bootstrap the user(customer|guest
     * @param type User $user
     * @param type String customer|guest
     * @param type array $params
     */

    public function bootstrapUser($user, $userType, $profileParams, $referralParams = []) {
        return \DB::transaction(function() use ($user, $userType, $profileParams, $referralParams) {
            $user->save();
            $user->roles()->attach(config('constant.user_default_role')[$userType]);
            $user->customerProfile()->save(new \App\Models\CustomerProfile($profileParams));
            $this->createCustomerCart($user);
            if ($userType == 'customer') {
                $user->userReferralCode()->save(new \App\Models\UserReferralCode($referralParams));
            }
            return $user;
        });
    }

    /**
     * Method to create User Bootstrap Cart
     * @param type User $user
     */
    public function createCustomerCart($user) {
        $cartParams                     = ['user_id' => $user->id];
        $cartModel                      = \App\Models\Cart::create($cartParams);
        if ($cartModel) {
            $cartModel->token           = md5(uniqid($cartModel->id . '_', true));
            $cartModel->firebase_url    = config('services.firebase')['base_url'] . '/carts/' . $cartModel->id . '_' . $cartModel->token;
            $cartModel->save();
        }
    }

    /**
     * Method to generate the user referral code
     * @param type User $user
     */
    public function getUserProfileParams($params) {
        $profileParams = ['zipcode' => array_has($params, 'zipcode') ? $params['zipcode'] : NULL,
            'active_zipcode' => array_has($params, 'zipcode') ? $params['zipcode'] : NULL,
            'timezone' => array_has($params, 'timezone') ? $params['timezone'] : config('constant.timezone')];
        if ($profileParams['active_zipcode']) {
            $activeZipcode = \App\Models\Zipcode::find($profileParams['active_zipcode']);
            if ($activeZipcode->zone_id) {
                $profileParams['active_zone_id'] = $activeZipcode->zone_id;
            }
        }
        return $profileParams;
    }

    public function postLoginActions($authUser) {
        $user = \App\Models\User::find($authUser->id);
        $user->token_invalidated_at = NULL;
        $user->last_login_at = \Carbon\Carbon::now();
        $user->save();
    }

    /**
     * Method to generate the user referral code
     * @param type User $user
     */
    public function getReferralParams($user) {
        //save referral code and config
        $code           = $this->generateUniquePromoCode($user->firstname, $user->lastname);
        $shortUrl       = URL::to('/store?code=' . $code . '&utm_campaign=cref&utm_source=web&utm_medium=copy');
        $facebookUrl    = URL::to('/store?code=' . $code . '&utm_campaign=cref&utm_source=web&utm_medium=facebook');
        $twitterUrl     = URL::to('/store?code=' . $code . '&utm_campaign=cref&utm_source=web&utm_medium=twitter');
        $smsUrl         = URL::to('/store?code=' . $code . '&utm_campaign=cref&utm_source=web&utm_medium=sms');
        $referralParams = ['user_id' => $user->id, 'code' => $code,
            'coupon_value' => settings('customer_referral.coupon_value'), 'referer_value' => settings('customer_referral.referer_value'),
            'uses_total' => settings('customer_referral.max_referral_conversion'), 'short_url' => $shortUrl,
            'url_facebook' => $facebookUrl, 'short_url_twitter' => $twitterUrl, 'short_url_sms' => $smsUrl];
        return $referralParams;
    }

    /**
     * Method to create the Customer at Braintree
     * @param type $user
     */
    /* public function saveCustomerAtBraintree($user) {
      \Illuminate\Support\Facades\Event::fire(new \App\Events\SaveBraintreeCustomer($user));
      } */
}
