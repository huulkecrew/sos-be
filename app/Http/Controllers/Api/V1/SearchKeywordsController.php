<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Serializer\ArrayIncludeSerializer;

class SearchKeywordsController extends BaseController
{   
    
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    /**
     * @api {get} /api/v1/searchkeywords Get Keyword List
     * @apiName GetKeywordsList
     * @apiGroup Search Keyword
     * @apiDescription Get Search Keywords
     * @apiPermission Customer
     * 
     * @apiParam {Integer}  zone_id Valid ZoneId is required.
     * @apiParam {Integer}  warehouse_id Valid WarehouseId is required.
     * 
     * @apiError {Object[]} ZoneIdRequired The <code>zone_id</code> is required.
     * @apiError {Object[]} ZoneIdInvalid  The <code>zone_id</code> is invalid.
     * @apiError {Object[]} WarehouseIdRequired The <code>warehouse_id</code> is required.
     * @apiError {Object[]} WarehouseIdInvalid  The <code>warehouse_id</code> is invalid.
     * 
     * @apiSuccess (200) {Object[]} Success The keyword list, Empty list if no keyword.
     * @apiError (400) {Object} Normal validation errors.
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": [
     *     {
     *       "id": 3,
     *       "zone_id": 1,
     *       "warehouse_id": 2,
     *       "label": "Seafood",
     *       "type": "product"
     *     },
     *     {
     *       "id": 4,
     *       "zone_id": 1,
     *       "warehouse_id": 2,
     *       "label": "Seafood",
     *       "type": "product"
     *     }
     *   ],
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     * @param Request $request
     * @return type
     */
    public function get(Request $request) {
        $messages   =   [
            'zone_id.required'      =>  trans('search.zone_id_required'),
            'zone_id.integer'       =>  trans('search.zone_id_invalid'),
            'zone_id.min'           =>  trans('search.zone_id_invalid'),
            'warehouse_id.required' =>  trans('search.warehouse_id_required'),
            'warehouse_id.integer'  =>  trans('search.warehouse_id_invalid'),
            'warehouse_id.min'      =>  trans('search.warehouse_id_invalid')
        ];
        $rules      =   [
            'zone_id'               =>  'required|integer|min:1',
            'warehouse_id'          =>  'required|integer|min:1'
        ];
        $validator          =   Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $keywords    = \App\Models\SearchKeyword::where('zone_id', '=', $request->input('zone_id'))
                ->where('warehouse_id', '=', $request->input('warehouse_id'))->get();
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($keywords, new \App\Transformer\SearchKeywordTransformer(), 'data', null, ['http_code'=>200]);
    }
    
    /**
     * @api {post} /searchkeywords Add New Keyword
     * @apiName Add New Keyword
     * @apiGroup Search Keyword
     * @apiDescription Add New Keyword
     * @apiPermission Customer
     *  
     * @apiParam {String="...128" Characters}  keyword[label] Keyword Label is required.
     * @apiParam {Integer}   keyword[zone_id] ZoneId is required.
     * @apiParam {Integer}    keyword[warehouse_id] WarehouseId is required.
     * @apiParam {String="product"}    keyword[type] Keyword type is optional parameter.
     * 
     * @apiError {Object[]} KeywordLabelRequired The <code>label</code> is required.
     * @apiError {Object[]} KeywordLabelInvalid  The <code>label</code> is invalid.
     * @apiError {Object[]} ZoneIdRequired The <code>zone_id</code> is required. 
     * @apiError {Object[]} ZoneIdInvalid  The <code>zone_id</code> is invalid. 
     * @apiError {Object[]} WarehouseIdRequired The <code>warehouse_id</code> is required. 
     * @apiError {Object[]} WarehouseIdInvalid  The <code>warehouse_id</code> is invalid.
     * @apiError {Object[]} KeywordTypeInvalid The <code>type</code> is invalid, It must be in defined values
     * 
     * @apiSuccess (200) {Object} Success New Keyword has been added successfully.
     * @apiSuccessExample {json} Success-Response:
     *   {
     *     "data": {
     *       "id": 5,
     *       "zone_id": 1,
     *       "warehouse_id": 2,
     *       "label": "Costco",
     *       "type": "warehouse"
     *     },
     *     "meta": {
     *       "http_code": 200
     *     }
     *   }
     * 
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function add(Request $request) {
        $messages   =   [
            'keyword.label.required'        =>  trans('search.keyword_label_required'),
            'keyword.label.min'             =>  trans('search.keyword_label_invalid'),
            'keyword.label.max'             =>  trans('search.keyword_label_invalid'),
            'keyword.zone_id.required'      =>  trans('search.zone_id_required'),
            'keyword.zone_id.integer'       =>  trans('search.zone_id_invalid'),
            'keyword.zone_id.min'           =>  trans('search.zone_id_invalid'),
            'keyword.warehouse_id.required' =>  trans('search.warehouse_id_required'),
            'keyword.warehouse_id.integer'  =>  trans('search.warehouse_id_invalid'),
            'keyword.warehouse_id.min'      =>  trans('search.warehouse_id_invalid'),
            'keyword.type.in'               =>  trans('search.keyword_type_must_be_in').config('constant.search_keyword_type')['product']
        ];
        $rules      =   [
            'keyword.label'                 =>  'required|min:2,max:128',
            'keyword.zone_id'               =>  'required|integer|min:1',
            'keyword.warehouse_id'          =>  'required|integer|min:1',
            'keyword.type'                  =>  'in:'.config('constant.search_keyword_type')['product']
        ];
        
        $validator          =   Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $requestData            =   $request->only('keyword')['keyword'];
        $searchKeyword          =   new \App\Models\SearchKeyword();
        $data                   =   json_encode(['value'=>$requestData['label']]);
        $searchKeyword->fill($requestData);
        $searchKeyword->data    = $data;
        $searchKeyword->type    =   $request->has('keyword.type')? $requestData['type'] : config('constant.search_keyword_type')['product'];

        if( !$searchKeyword->save() ) {
            return $this->response->errorInternalError(trans('search.keyword_can_not_be_created'));
        }
        
        return $this->response->withItem($searchKeyword, new \App\Transformer\SearchKeywordTransformer(), 'sata', ['http_code' => 200]);
    }
    
}
