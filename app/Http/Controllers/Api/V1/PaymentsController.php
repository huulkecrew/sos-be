<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\UserUtils;
use App\Serializer\ArrayIncludeSerializer;

class PaymentsController extends BaseController
{   
    use UserUtils;
    
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    public function token() {
        echo \App\Payments\BurpyPayment::getBrainTreeToken();exit;
    }
    
    /**
     * @api {get} /customer/card Card List
     * @apiName GetCustomerCards
     * @apiGroup Payments
     * @apiDescription Get Customer's Card List
     * @apiPermission Customer
     * 
     * @apiSuccess (200) {Object[]} Success The card list, Empty list if no card added yet.
     * 
     * @apiSuccessExample {json} Success-Response:
     *   {
     *     "data": [
     *       {
     *         "id": 19,
     *         "user_id": 76,
     *         "cardholdername": "Prem baboo",
     *         "type": "MasterCard",
     *         "number": "555555******4444",
     *         "month": "03",
     *         "year": 2017,
     *         "image_url": "https://assets.braintreegateway.com/payment_method_logo/visa.png?environment=sandbox",
     *         "address_1": House No 08,
     *         "city": Austin,
     *         "state": TX,
     *         "country": United States of America,
     *         "zipcode": "78705",
     *         "created_at": "2016-06-12 12:18:28",
     *         "updated_at": "2016-06-12 12:18:28"
     *       }
     *       .....
     *     ],
     *     "meta": {
     *       "http_code": 200
     *     }
     *   }
     * 
     */
    public function get() {
        $userId   = Auth::user()->id;
        $cards    = \App\Models\CustomerCard::where('user_id', '=', $userId)->where('status', '=', 1)->get();
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($cards, new \App\Transformer\CustomerCardTransformer, 'data', null, ['http_code'=>200]);
    }
    
    /**
     * @api {post} /customer/card Add/Verify Card
     * @apiName Add Card
     * @apiGroup Payments
     * @apiDescription Add Card
     * @apiPermission Customer
     * 
     * @apiParam {String}  card[payment_method_nonce] PaymentMethodNonce is required.
     * @apiParam {String}  card[device_data] DeviceData is required.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *  "data": {
     *     "id": 19,
     *     "user_id": 76,
     *     "cardholdername": "Prem baboo",
     *     "type": "MasterCard",
     *     "number": "555555******4444",
     *     "month": "03",
     *     "year": "2017",
     *     "image_url": "https://assets.braintreegateway.com/payment_method_logo/mastercard.png?environment=sandbox",
     *     "address_1": "House No 08",
     *     "city": "Austin",
     *     "state": "TX",
     *     "country": "United States of America",
     *     "zipcode": "78705",
     *     "created_at": "2016-06-12 12:18:28",
     *     "updated_at": "2016-06-12 12:18:28"
     *   },
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * @apiSuccess (200) {Object} Success Card has been added successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (431) {Object} Card not validated via Braintree api.
     * @apiError (500) {Object} Internal Server Error.
     */
    public function add(Request $request) {
        $messages = [
            'card.payment_method_nonce.required'    =>  trans('card.payment_method_nonce_required'),
            'card.device_data.required'             =>  trans('card.payment_data_device_required')
        ];
        $rules = [
            'card.payment_method_nonce'             =>  'required',
            'card.device_data'                      => 'required'
        ];
        $userId                                     =  Auth::user()->id;
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $requestData = $request->only('card')['card'];
        //Get Braintree customer ID which is created and updated at the time registration
        $customerProfile = \App\Models\CustomerProfile::where('user_id', '=', $userId)->first();
        //Post the payment_method_nonce to Brain tree to add the card in vault
        $paymentMethod = \App\Payments\BurpyPayment::getPaymentMethod($customerProfile->braintree_user_id, $requestData);
        
        if( !$paymentMethod['status'] ) {
            return $this->response->errorUnwillingToProcess(trans($paymentMethod['response']));
        }
        $btResponse = $paymentMethod['response'];
        
        //verify the card is already exist or not in case If it deleted from Burpy
        $customerCard = \App\Models\CustomerCard::firstOrNew(
                array('user_id'=>$userId, 'type'=>$btResponse->cardType, 
                    'cardholdername'=>$btResponse->cardholderName, 
                    'number'=>$btResponse->maskedNumber,
                    'month'=>$btResponse->expirationMonth, 'year'=>$btResponse->expirationYear));
        if($customerCard->exists) {
            $customerCard->status           = 1; //Enabled 
            $customerCard->save();

            return $this->response->withItem($customerCard, new \App\Transformer\CustomerCardTransformer(), 'data', ['http_code'=>200]);
        }
        //Add the card to __customer_card table
        return $this->addCard($userId, $customerCard, $btResponse); 
    }
    
    /**
     * @api {delete} /customer/card Delete Card
     * @apiName Delete Card
     * @apiGroup Payments
     * @apiDescription Delete Customer Card
     * @apiPermission Customer
     * 
     * @apiParam {Interger} id Card ID is required.
     * 
     * @apiError {Object} CardIdRequired The <code>id</code> is required.
     * @apiError {Object} CardIdInvalid The <code>id</code> is Invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     *  {
     *      "message": "Your card has been deleted successfully"
     *  }
     * 
     * @apiSuccess (200) {Object} The card has been deleted successfully.
     * @apiError (500) {Object} CardNotDeleted The card can not be deleted.
     */
    public function delete(Request $request) {
        $userId     =   Auth::user()->id;
        $messages   =   [
            'id.required'       =>  trans('card.card_id_required'),
            'id.exists'         =>  trans('card.card_id_invalid')
        ]; 
        $validator = Validator::make($request->all(), 
            ['id' => 'required|integer|exists:customer_card,id,user_id,'.$userId], 
            $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $customerCard               =   \App\Models\CustomerCard::where('id', '=', $request->input('id'))->where('user_id', '=', $userId)->first();
        $customerCard->status       =   0; //Disable the card
        $customerCard->deleted_at   =   \Carbon\Carbon::now();
        if( $customerCard->save() ) {
            return $this->response->withArray(['message'=>  trans('card.card_deleted_successfully'), 'meta'=>['http_code'=>200]]);
        } else {
            return $this->response->errorInternalError(trans('card.something_went_wrong'));
        }   
    }

    /**
     * @api {post} /customer/card/payment Make Payment
     * @apiName Make Payment
     * @apiGroup Payments
     * @apiDescription Make Payment
     * @apiPermission Customer
     * 
     * @apiParam {String}  payment[card_id] PaymentCardId is required.
     * @apiParam {String}  payment[amount] PaymentAmount is required.
     *  
     * @apiSuccess (200) {Object} Success Payment has been Authorised successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (431) {Object} Payment not proccesed via Braintree API successfully.
     * @apiError (500) {Object} Internal Server Error.
     *
     */
    public function makePayment(Request $request) {
        $userId         =   76;//Auth::user()->id;
        $messages       =   [
            'payment.card_id.required'  =>  trans('card.cardid_required'),
            'payment.card_id.exists'    =>  trans('card.cardid_invalid'),
            'payment.amount.required'   =>  trans('card.payment_amount_required')
        ];
        $rules          =   [
            'payment.card_id'   =>  'required|exists:customer_card,id,user_id,'.$userId,
            'payment.amount'    =>  'required'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $requestData = $request->only('payment')['payment'];
        $customerCard   = \App\Models\CustomerCard::where('id', '=', $requestData['card_id'])->where('user_id', '=', $userId)->first();
        $payemnt = ['paymentMethodToken'=>$customerCard->token, 'amount'=>$requestData['amount']];
        
        $btResponse = \App\Payments\BurpyPayment::makeTransaction($payemnt);
        //TODO with payemnt table and its product
        print_r($btResponse); exit;
        
    }

    //Private function to add the card in the table
    private function addCard($userId, $customerCard, $btResponse) {
        $customerCard->user_id          = $userId;
        $customerCard->token            = $btResponse->token;
        $customerCard->type             = $btResponse->cardType;
        $customerCard->cardholdername   = $btResponse->cardholderName;
        $customerCard->number           = $btResponse->maskedNumber;
        $customerCard->month            = $btResponse->expirationMonth;
        $customerCard->year             = $btResponse->expirationYear;
        $customerCard->image_url        = $btResponse->imageUrl;
        $customerCard->address_1        = $btResponse->billingAddress->streetAddress;
        $customerCard->city             = $btResponse->billingAddress->locality;
        $customerCard->state            = $btResponse->billingAddress->region;
        $customerCard->country          = $btResponse->billingAddress->countryName;
        $customerCard->zipcode          = $btResponse->billingAddress->postalCode;

        if( !$customerCard->save() ) {
            return $this->response->errorInternalError(trans('card.something_went_wrong'));
        }

        return $this->response->withItem($customerCard, new \App\Transformer\CustomerCardTransformer(), 'data', ['http_code'=>200]);
    }

}