<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use JWTAuth;
use Auth;
use Illuminate\Mail\Message;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

class UsersController extends BaseController
{
    use ResetsPasswords;
    protected $model;
    
    public function __construct(Response $response) {
        $this->model = new \App\Models\User();
        parent::__construct($response);
    }
    
    /**
     * @api {get} /api/v1/users/profile User profile
     * @apiDescription Get User Profile
     * @apiPermission customer
     * 
     * @apiName  profile
     * @apiGroup User
     * 
     * 
     * @apiSuccess (200) {Object[]} Success.
     * 
     * @apiError (404) {Object[]} Error.
     * @apiErrorExample {json} Success-Response:
     * {
     * "error": {
     *  "code": "GEN-NOT-FOUND",
     *  "http_code": 404,
     *  "message": "The password can not be changed."
     *  }
     * }
     * 
     */
    public function profile(Requests $request) {
        $userId = Auth::user()->id;
        $userProfile                = \App\Models\User::getUserWithProfile($userId);
        if(!$userProfile){
            return $this->response->errorInternalError(trans('auth.something_went_wrong'));
        }
        return $this->response->withArray(['data'=> \App\Transformer\UserTransformer::transformWithCustomerContext($userProfile), 'meta'=>['http_code'=>200]]);
    }
    
    /**
     * @api {patch} /api/v1/user Update User
     * @apiDescription Update User
     * @apiPermission customer
     * 
     * @apiName  updateProfile
     * @apiGroup User
     * 
     * @apiParam {String} user[current_password] Required(User Current Password)
     * @apiParam {String} user[firstname]        Optional(Alphanumeric)
     * @apiParam {String} user[lastname]         Optional(Alphanumeric)
     * @apiParam {String} user[phone]            Optional(Numbers only)
     * @apiParam {String} user[email]            Optional
     * @apiParam {String} user[confirm_email]    Required (Only with email update requested)
     * 
     * @apiSuccess (200) {Object[]} Success.
     * @apiSuccessExample {json} Success-Response:
     * {
     * "message": "Updated successfully."
     * }
     * @apiError (422) {Object[]} Error.
     * @apiErrorExample {json} Success-Response:
     * {
     * "error": {
     *  "user.current_password": [
     *  "The current_password field is required."
     *  ]
     * },
     * "meta": {
     *  "http_code": 422
     *  }
     * }
     * 
     */
    public function updateProfile(Request $request) {
        
        $authUser               = Auth::user(); //Get current user          
        $messages = [
            'user.email.email'                       => trans('user.email_invalid'),
            'user.email.Unique'                      => trans('user.email_already_taken'),
            'user.confirm_email.required_with'       => trans('user.confirm_email_with_email'),
            'user.confirm_email.same'                => trans('user.email_confirm_email_different'),
            'user.contact_email.email'               => trans('user.contact_email_invalid'),
            'user.current_password.required'         => trans('user.current_password_required')
        ];
        $rules = [
            'user.email'             => 'email|max:96|Unique:user,email,'.$authUser->id.',id,user_type_id,'.config('constant.user_type')['customer'],
            'user.confirm_email'     => 'required_with:user.email|same:user.email',
            'user.firstname'         => 'AlphaNum|max:32',
            'user.lastname'          => 'AlphaNum|max:32',
            'user.phone'             => 'phone:US',
            'user.current_password'  => 'required'
        ];
        if(!$authUser->is_social){
            $rules['user.current_password'] = 'required';
        }
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        if(!$authUser->is_social){
            $validator->after(function($validator) use ($request, $authUser) {
                $check = auth()->validate([
                    'email'    => $authUser->email,
                    'password' => $request->user['current_password'],
                    'user_type_id'      => $authUser->user_type_id
                ]);
                if (!$check){
                    $validator->errors()->add('user.current_password', 
                        trans('user.current_password_incorrect'));
                }
            });
        }
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $userProfile = \App\Models\User::where('id', '=', $authUser->id)->first();
        if( !$userProfile ) {
            return $this->response->errorNotFound(trans('user.user_not_found'));
        }
        //below individual column upate
        if(!$authUser->is_social) {
            $userProfile->email         = $request->has('user.email') ? $request->user['email']: $userProfile->email;
            $userProfile->contact_email = $userProfile->email;
        }else{
            $userProfile->contact_email = $request->has('user.email')?  $request->user['email']: $userProfile->contact_email;
        }        
        $userProfile->firstname         = $request->has('user.firstname') ? $request->user['firstname'] : $userProfile->firstname;
        $userProfile->lastname          = $request->has('user.lastname') ?  $request->user['lastname'] : $userProfile->lastname;
        $userProfile->phone             = $request->has('user.phone') ? $request->user['phone'] : $userProfile->phone;
        $userProfile->updated_at        = \Carbon\Carbon::now();
        if( !$userProfile->save() ) {
            return $this->response->errorNotFound(trans('user.profile_can_not_be_updated'));
        }
        return $this->response->withArray(['message'=>  trans('user.updated_successfully')], ['http_code'=>200]);
        
    }
    
    /**
     * @api {patch} /api/v1/user/password Update Password
     * @apiDescription Update user password
     * @apiPermission customer
     * 
     * @apiName updatePassword
     * @apiGroup User
     * 
     * @apiParam {String} user[current_password] User current Password
     * @apiParam {String} user[new_password] New password in length range 6...32)
     * @apiParam {String} user[confirm_password] Confirm Password
     *  
     * @apiSuccess (200) {Array} Success.
     * @apiSuccessExample {json} Success-Response:
     * {
     * "message": "Updated successfully."
     * }
     * @apiError (404) {Object[]} Error.
     * @apiErrorExample {json} Success-Response:
     * {
     * "error": {
     *  "code": "GEN-NOT-FOUND",
     *  "http_code": 404,
     *  "message": "The password can not be changed."
     *  }
     * }
     * 
     */
    public function updatePassword(Request $request) {
        
        $authUser               = Auth::user(); //Get current user        
        $messages = [
            'user.current_password.required' => trans('user.current_password_required'),
            'user.new_password.required' => trans('user.new_password_required'),
            'user.new_password.min'      => trans('user.current_password_short'),
            'user.new_password.max'      => trans('user.current_password_longer'),
            'user.confirm_password.same' => trans('user.old_new_password_different')
        ];
        
        $rules = [
            'user.current_password'          => 'required',
            'user.new_password'              => 'required|min:6|max:20',
            'user.confirm_password'          => 'required|same:user.new_password'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->after(function($validator) use ($request, $authUser) {
            $check = auth()->validate([
                'email'             => $authUser->email,
                'password'          => $request->user['current_password'],
                'user_type_id'      => $authUser->user_type_id
            ]);
            if (!$check){
                $validator->errors()->add('user.current_password', 
                    trans('user.current_password_incorrect'));
            }
        });
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $userProfile = \App\Models\User::where('id', '=', $authUser->id)->first();
        if( !$userProfile ) {
            return $this->response->errorNotFound(trans('user.user_not_found'));
        }
        $userProfile->password             = Hash::make($request->user['new_password']);
        //Save the timestamp when the older token invalidated
        $userProfile->token_invalidated_at = \Carbon\Carbon::now()->getTimestamp();
        if($userProfile->save()) {
            try {
                // verify the credentials and create a new token for the user after change password
                $credentials = [
                    'email'             => $userProfile->email, 
                    'password'          => $request->user['new_password'],
                    'user_type_id'      => $userProfile->user_type_id,
                    'user_status_id'    => $userProfile->user_status_id
                ];
                if (!$token = JWTAuth::attempt($credentials)) {
                    return $this->response->errorUnauthorized(trans('auth.invalid_credentials'));
                }
                $authUser = Auth::user();
                //TODO send email to user that he has changed his password
                return $this->response->withArray(['token' => $token, 'message'=>trans('user.password_update_success')], ['http_code'=>200]);
                           
            } catch (JWTException $e) {
                // something went wrong
                return $this->response->errorInternalError(trans('auth.could_not_create_token'));
            }
        } else {
            return $this->response->errorInternalError(trans('user.password_can_not_be_updated'));
        }
    }

    /**
     * @api {post} /api/v1/users/location Update Location on availability
     * @apiDescription Update user location via zipcode or address id
     * @apiPermission customer,guest
     * 
     * @apiGroup User
     * @apiName  updateLocation 
     *
     * @apiParam {String}  zipcode Zipcode
     * @apiParam {Integer} address_id User Saved Address ID
     * @apiParam {Integer} warehouse_id Active Warehouse ID
     * 
     * 
     * @apiSuccess (200) {Object} Success.
     * @apiError (404)  Error Zipcode not found.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
        "data": {
          "zipcode": 78705,
          "zone_id": 1,
          "city": "Austin",
          "state": "Texas",
          "state_code": "TX",
          "service_status": "1"
        },
        "meta": {
          "http_code": 200
        }
     * }
}
     * 
     */
    public function updateLocation(Requests\UpdateCustomerZipcodeRequest $request) {
        
        $user                   = Auth::user();
        $params                 = $request->all();
        if($request->has('address_id')){
            $userAddress            = \App\Models\CustomerAddress::where('id', '=', $params['address_id'])
                    ->where('user_id', '=', $user->id)->first();
            if( !$userAddress ) {
                return $this->response->errorNotFound(trans ('user.address_not_found'));
            }else{
                $params['zipcode'] = $userAddress->zipcode;
            }
        }
        $customerProfile = \App\Models\CustomerProfile::where('user_id', '=', $user->id)->first();
        if( !$customerProfile ) {
            $customerProfile          = new \App\Models\CustomerProfile();
            $customerProfile->user_id = $user->id;
        }
        $zipcodeInfo        = \App\Models\Zipcode::serviceAvailable($params['zipcode']);
        if(!$zipcodeInfo){
            return $this->response->errorNotFound(trans('zone.zipcode_not_found'));
        }
        /*$zipcodeWarehouse = \App\Models\Zipcode::getZonalWarehousesWithZip($zipcodeInfo->zipcode, $zipcodeInfo->zone_id);
        if($zipcodeWarehouse->warehouses){
            $defaultWarehouse = null;
            $defaultUpdated   = false;
            foreach($zipcodeWarehouse->warehouses as $warehouse){
                if($warehouse->is_default == 1){
                    $defaultWarehouse = $warehouse;
                }
                if($warehouse->id == $params['warehouse_id']){
                    $warehouse->is_default                  = 1;
                    $defaultUpdated                         = true;
                    $customerProfile->active_warehouse_id   = $warehouse->id; 
                }else{
                    $warehouse->is_default                  = 0;
                }
            }
            if(!$defaultUpdated && $defaultWarehouse){
                $defaultWarehouse->is_default           = 1;
                $customerProfile->active_warehouse_id   = $defaultWarehouse->id; 
            }
            $customerProfile->active_zipcode = $params['zipcode'];
            if(!$customerProfile->save()) {
                return $this->response->errorInternalError(trans ('user.location_update_failed'));
            } 
        }*/
        if($zipcodeInfo->service_status == 1){
            $customerProfile->active_zipcode = $params['zipcode'];
            if(!$customerProfile->save()) {
                return $this->response->errorInternalError(trans ('user.location_update_failed'));
            }        
        }
        return $this->response->withArray(['data'=>\App\Transformer\ZipcodeTransformer::transformWithAvailabilityContext($zipcodeInfo), 'meta'=>['http_code'=>200]]);
    }
    
    /**
     * @api {put} /api/v1/users/setting Update Settings
     * @apiName Update Settings
     * @apiGroup User
     * 
     * @apiParam {Integer} warehouse_id 
     *
     * @apiSuccess (200) {Object} Success.
     * @apiError (404)   {Object} UserNotFound The <code>id</code> of the User was not found.
     * @apiError (404)   {Object} WarehouseNotFound The <code>warehouse_id</code> was not found.
     * 
     */
    public function updateSetting(Requests\UpdateCustomerSettingRequest $request) {
        
        $userId                       = Auth::user()->id;
        $warehouseId                  = $request->input('warehouse_id');
        $customerProfile              = \App\Models\CustomerProfile::where('user_id', '=', $userId)->first();
        if( !$customerProfile ) {
            $customerProfile          = new \App\Models\CustomerProfile();
            $customerProfile->user_id = $userId;
        }
        $customerProfile->active_warehouse_id          = $warehouseId;
        $customerProfile->active_warehouse_location_id = NULL;
        if(!$customerProfile->save() ) {
            return $this->response->errorNotFound(trans ('user.user_update_failed'));
        } 
        return $this->response->withArray(['data'=>[], 'meta'=>['http_code'=>200]]);
    }

    /**
     * @api {post} /api/v1/users/password/forgot ResetPassword Request
     * @apiName Forgot Password
     * @apiGroup User
     * @apiDescription Request to reset password
     * @apiPermission None
     * 
     * @apiParam {String}  email Email address is required.
     * 
     * @apiError {Object[]} EmailRequired The <code>email</code> is required.
     * @apiError {Object[]} EmailInvalid  The <code>email</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": "The reset password instructions mail sent to your mail"
     * }
     * 
     * @apiSuccess (200) {Object} Success Reset email sent successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     */
    public function forgotPassword(Request $request) {
        $validator = Validator::make($request->all(), 
                ['email'=>'required|exists:user,email,user_type_id,'.config('constant.user_type')['customer']], 
                [
                    'email.required'=> trans('user.email_required'),
                    'email.exists'  => trans('user.email_invalid')
                ]);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject(trans('mail.reset_password_subject'));
        });
        
        switch ($response) {
           case Password::RESET_LINK_SENT:
               return $this->response->withArray(['message'=>trans('mail.password_link_send')], ['http_code'=>200]);

           case Password::INVALID_USER:
               return $this->response->errorNotFound(trans('user.email_invalid'));
       }
    }
    
    public function verifyToken(Request $request) {
        $validator = Validator::make($request->all(), 
                ['token'=>'required'], 
                ['token.required'=> trans('user.token_required')]);
        
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        echo "Need to check from DB". $request->input('token'); 
        echo Password::tokenExists($request->input('token'));
        exit;
    }

    /**
     * @api {post} /api/v1/users/password/reset ResetPassword
     * @apiName Reset Password
     * @apiGroup User
     * @apiDescription Update the password
     * @apiPermission None
     * 
     * @apiParam {String}  user[token] Email address is required.
     * @apiParam {String}  user[email] Email address is required.
     * @apiParam {String}  user[password] Email address is required.
     * @apiParam {String}  user[password_confirmation] Email address is required.
     * 
     * @apiError {Object[]} TokenRequired The <code>token</code> is required.
     * @apiError {Object[]} TokenInvalid  The <code>token</code> is invalid.
     * @apiError {Object[]} EmailRequired The <code>email</code> is required.
     * @apiError {Object[]} EmailInvalid  The <code>email</code> is invalid.
     * @apiError {Object[]} PasswordRequired The <code>password</code> is required.
     * @apiError {Object[]} PasswordInvalid  The <code>password</code> is invalid.
     * @apiError {Object[]} PasswordConfirmationRequired The <code>empassword_confirmationail</code> is required.
     * @apiError {Object[]} PasswordConfirmationInvalid  The <code>password_confirmation</code> does not match with password.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": "The password has been changed successfully"
     * }
     * 
     * @apiSuccess (200) {Object} Success Password updated successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     */
    public function resetPassword(Request $request) {
        $messages = [
            'user.password.required'                => trans('registration.password_required'),
            'user.password_confirmation.required'        => trans('registration.confirm_password_required'),
            'user.password_confirmation.same'       => trans('registration.password_confirm_password_must_match'),
            'user.token.required'                   => trans('user.token_required')
            
        ];
        $rules = [
            'user.token'                    =>  'required',
            'user.email'                    =>  'required',
            'user.password'                 =>  'required|min:6|max:20',
            'user.password_confirmation'    =>  'required|same:user.password'
            ];
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $requestData = $request->only('user')['user'];
        $credentials = $requestData;
        $response = Password::reset($credentials, function () {
            return true;  
        });
        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->response->withArray(['message'=>trans('user.password_updated_succefully')], ['http_code'=>200]); 
            default:
                return $this->response->errorNotFound(trans('user.token_expired'));
        }
        
    }
    
    /**
     * @api {post} /api/v1/users/device/token Add Device Token
     * @apiName Add Device Token
     * @apiGroup User
     * @apiDescription Add Device Token
     * @apiPermission Customer
     * 
     * @apiParam {String}  user[device_token] Device token is required.
     * @apiParam {String="ios","android"}  user[device_type] Device type is required.
     * @apiParam {String}  user[device_model] Device model is required.
     * @apiParam {String}  user[os_version] OS version is required.
     * 
     * @apiError {Object[]} DeviceTokenRequired The <code>device_token</code> is required.
     * @apiError {Object[]} DeviceTypeRequired The <code>device_type</code> is required.
     * @apiError {Object[]} DeviceTypeInvalid The <code>device_type</code> is invalid.
     * @apiError {Object[]} DeviceModelRequired  The <code>device_model</code> is required.
     * @apiError {Object[]} OsVersionRequired  The <code>os_version</code> is required.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": true
     * }
     * 
     * @apiSuccess (200) {Object} Success Notification updated successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     */
    public function addDeviceToken(Request $request) {
        $userId = Auth::user()->id;
        $messages = [
            'user.device_id.required'        =>  trans('user.device_id_required'),
            'user.device_token.required'     =>  trans('user.device_token_required'),
            'user.device_type.required'      =>  trans('user.device_type_required'),
            'user.device_type.in'            =>  trans('device_type_invalid'),
            'user.device_model.required'     =>  trans('user.device_model_required'),
            'user.os_version.required'       =>  trans('user.device_os_version_required')
        ];
        $rules = [
            'user.device_id'     =>  'required',
            'user.device_token'  =>  'required',
            'user.device_type'   =>  'required|in:ios,android',
            'user.device_model'  =>  'required',
            'user.os_version'    =>  'required'
        ];
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $requestData                    =   $request->only('user')['user'];
        $notificationToken              =   \App\Models\UserNotificationToken::firstOrNew(['user_id'=>$userId, 'device_token'=>$requestData['device_token']]);
        $notificationToken->device_id   =   $requestData['device_id'];
        $notificationToken->device_type =   $requestData['device_type'];
        $notificationToken->user_id     =   $userId;
        $notificationToken->created_at  =   \Carbon\Carbon::now();
        $notificationToken->device_meta =   json_encode(['device_model'=>$requestData['device_model'], 'os_version'=>$requestData['os_version']]);
        if( $notificationToken->save() ) {
            return $this->response->withArray(['message'=>true], ['http_code'=>200]); 
        }
        return $this->response->errorInternalError(trans('user.notification_can_not_be_updated'));
    }
    
    /**
     * @api {patch} /api/v1/users/notifications Notification Setting
     * @apiName Notification Setting Update
     * @apiGroup User
     * @apiDescription Enable/Disable the text or push Notification
     * @apiPermission None
     * 
     * @apiParam {String=1 0}  notification[text_notification] TestNotification is required.
     * @apiParam {String=0 1}  notification[push_notification] PushNotification is required.
     * 
     * @apiError {Object[]} TestNotificationRequired The <code>text_notification</code> is required.
     * @apiError {Object[]} TestNotificationInvalid  The <code>text_notification</code> is invalid.
     * @apiError {Object[]} PushNotificationRequired The <code>push_notification</code> is required.
     * @apiError {Object[]} PushNotificationInvalid  The <code>push_notification</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": "The notification setting updated successfully"
     * }
     * 
     * @apiSuccess (200) {Object} Success Notification setting updated successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     */
    public function notificationSetting(Request $request) {
        $userId = Auth::user()->id;
        $messages = [
            'notification.text_notification.required'   =>  trans('user.text_notification_required'),
            'notification.text_notification.in'         =>  trans('user.text_notification_invalid'),
            'notification.push_notification.required'   =>  trans('user.push_notification_required'),
            'notification.push_notification.in'         =>  trans('user.push_notification_invalid')
        ];
        $rules = [
            'notification.text_notification'     =>  'required|in:1,0',
            'notification.push_notification'     =>  'required|in:1,0'
        ];
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $requestData                        =   $request->only('notification')['notification'];
        $customerProfile                    =   \App\Models\CustomerProfile::where('user_id', '=', $userId)->first();
        //One doubt
        $customerProfile->text_notification =   $requestData['text_notification'];
        $customerProfile->push_notification =   $requestData['push_notification'];
        if( $customerProfile->save() ) {
            return $this->response->withArray(['message'=>  trans('user.notification_setting_updated')], ['http_code'=>200]); 
        }
        return $this->response->errorInternalError(trans('user.notification_can_not_be_updated'));
        
    }
    
    /**
     * @api {get} /api/v1/users/device/verify Verify Device Token Status
     * @apiName Verify Device Token Status
     * @apiGroup User
     * @apiDescription Verify either the device token is added or not If not then add,
     * This api response will return true=> If device token exist, false=>If the token is not exist
     * @apiPermission Customer
     * 
     * @apiParam {String}  device_token Device token is required.
     * @apiParam {String="ios","android"}  device_type Device type is required.
     * 
     * @apiError {Object[]} DeviceTokenRequired The <code>device_token</code> is required.
     * @apiError {Object[]} DeviceTypeRequired The <code>device_type</code> is required.
     * @apiError {Object[]} DeviceTypeInvalid The <code>device_type</code> is invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "message": true //If token added of this device 
     * }
     * OR
     * {
     *   "message": false //If token not added of this device 
     * }
     * 
     * @apiSuccess (200) {Object} Success true/false token exist or now.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     */
    public function verifyDeviceTokenStatus(Request $request) {
        $userId = Auth::user()->id;
        $messages = [
            'device_token.required'     =>  trans('user.device_token_required'),
            'device_type.required'      =>  trans('user.device_type_required'),
            'device_type.in'            =>  trans('device_type_invalid')
        ];
        $rules = [
            'device_token'  =>  'required',
            'device_type'   =>  'required|in:ios,android'
        ];
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $deviceToken    =   $request->input('device_token');
        $deviceType     =   $request->input('device_type');
        
        $isToken        = \App\Models\UserNotificationToken::where('user_id', '=', $userId)
                ->where('device_token', '=', $deviceToken)
                ->where('device_type', '=', $deviceType)->first();
        
        return $this->response->withArray(['message'=>$isToken?true:false], ['http_code'=>200]);        
    }

    protected function getResetCredentials(Request $request)
    {
        return $request->only(
            'user.email', 'user.password', 'user.password_confirmation', 'user.token'
        );
    }
    
    /**
     * Send email with reset password steps 
     * @param type $resource
     * @return type
     */
    private function sendResetPasswordEmail($resource) {
        return Mail::send('emails.resetpassword', ['resource'=>$resource], function($message) use ($resource) {
            $message->from($resource['from'], $resource['sender_name'])->to($resource['to'], $resource['to_name'])->subject($resource['subject']);
        });
    }
    
}
