<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Support\Facades\Auth;
use App\Serializer\ArrayIncludeSerializer;

class ProductComplainsController extends BaseController
{      
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    /**
     * @api {get} /api/v1/products/complain Product Complain List
     * @apiName GetProductComplains
     * @apiGroup Product
     * @apiDescription Get Product's Complain List
     * @apiPermission Customer
     * 
     * @apiSuccess (200) {Object[]} Success The complain list, Empty list if no complains.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": [
     *     {
     *       "id": 3,
     *       "user_id": 83,
     *       "warehouse_id": 2,
     *       "zipcode": "78705",
     *       "zone_id": 1,
     *       "zonal_product_id": 2,
     *       "product_complain_option_id": 1,
     *       "created_at": "2016-06-18 11:42:09",
     *       "updated_at": "2016-06-18 11:42:09"
     *     }
     *   ],
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     */
    public function get(Request $request) {
        $userId     =   Auth::user()->id;
        $complains  = \App\Models\ProductComplain::getProductComplainList($userId);
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($complains, new \App\Transformer\ProductComplainTransformer, 'data', null, ['http_code'=>200]);
    }

    /**
     * @api {post} /api/v1/products/complain Add Product Complain
     * @apiName AddProductComplain
     * @apiGroup Product
     * @apiDescription Add Product's Complain
     * @apiPermission Customer
     * 
     * @apiParam {String}  complain[zipcode] Zipcode is required.
     * @apiParam {Integer}  complain[zonal_product_id] Product Id is required.
     * @apiParam {Integer}  complain[warehouse_id] Warehouse Id is required.
     * @apiParam {Integer}  complain[zone_id] zone Id is required.
     * @apiParam {Integer}  complain[option] Opetion Id is required.
     * 
     * @apiError {Object[]} ZipcodeRequired The <code>zipcode</code> is required.
     * @apiError {Object[]} ZonalProductIdRequired The <code>zonal_product_id</code> is required.
     * @apiError {Object[]} WarehouseIdRequired The <code>warehouse_id</code> is required.
     * @apiError {Object[]} ZoneIdRequired The <code>zone_id</code> is required.
     * @apiError {Object[]} OptionRequired The <code>option</code> is required.
     * @apiError {Object[]} OptionInvalid  The <code>option</code> is invalid.
     * 
     * @apiSuccess (200) {Object[]} Success The complain list, Empty list if no complains.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *   "data": [
     *     {
     *       "id": 3,
     *       "user_id": 83,
     *       "warehouse_id": 2,
     *       "zipcode": "78705",
     *       "zone_id": 1,
     *       "zonal_product_id": 2,
     *       "product_complain_option_id": 1,
     *       "created_at": "2016-06-18 11:42:09",
     *       "updated_at": "2016-06-18 11:42:09"
     *     }
     *   ],
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     * @apiSuccess (200) {Object} Success Address has been added successfully.
     * @apiError (400) {Object} Normal validation errors.
     * @apiError (500) {Object} Internal Server Error.
     * 
     */
    public function add(Request $request) {
        $userId     = Auth::user()->id;  
        $messages   = [
            'complain.zipcode.required'          => trans('complain.zipcode_required'),
            'complain.zonal_product_id.required' => trans('complain.zonal_product_id_required'), 
            'complain.warehouse_id.required'     => trans('complain.warehouse_id_required'),
            'complain.zone_id.required'          => trans('complain.zonal_id_required'),
            'complain.option.required'           => trans('complain.option_required'),
            'complain.option.in'                 => trans('complain.option_invalid')
        ];
        $rules = [
            'complain.zipcode'          =>  'required',
            'complain.zonal_product_id' =>  'required', 
            'complain.warehouse_id'     =>  'required',
            'complain.zone_id'          =>  'required',
            'complain.option'           =>  'required|in:'.config('constant.product_complain_option')['incorrect_price'].','.config('constant.product_complain_option')['incorrect_description'].','.config('constant.product_complain_option')['incorrect_image']
        ];
        
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        
        $requestData        =   $request->only('complain')['complain'];
        $productComplain    =   new \App\Models\ProductComplain();
        $productComplain->fill($requestData);
        $productComplain->user_id                       =   $userId;  
        $productComplain->product_complain_option_id    =   $requestData['option'];
        
        if( !$productComplain->save() ) {
            return $this->response->errorInternalError(trans('complain.complain_can_not_be_submitted'));
        }
        
        return $this->response->withArray(['message'=>  trans('complain.product_complain_submitted_successfully'), 'meta'=>['http_code'=>200]]);        
    }
    
    /**
     * @api {delete} /api/v1/products/complain Address Delete
     * @apiName Delete Complain
     * @apiGroup Product
     * @apiDescription Delete Product Complain
     * @apiPermission Customer
     * 
     * @apiParam {Interger} id Complain ID is required.
     * 
     * @apiError {Object} IdRequired The <code>id</code> is required.
     * @apiError {Object} IdInvalid The <code>id</code> is Invalid.
     * 
     * @apiSuccessExample {json} Success-Response:
     *  {
     *      "message": "Your complain has been deleted successfully"
     *  }
     * 
     * @apiSuccess (200) {Object} The complain has been deleted successfully.
     * @apiError (500) {Object} ComplainNotDeleted The complain can not be deleted.
     */
    public function delete(Request $request) {
        $userId     = Auth::user()->id;
        $messages = [
            'id.required'               => trans('complain.id_required'),
            'id.exists'                 => trans('complain.id_invalid')
        ];
        $rules = ['id'  =>  'required|exists:product_complain,id'];
        $validator          = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->response->errorWrongArgs($validator->errors()->first());
        }
        $productComplain    = \App\Models\ProductComplain::where('id', '=', $request->input('id'))->where('user_id', '=', $userId)->first();
        if( !$productComplain->delete() ) {
            return $this->response->errorInternalError(trans('complain.complain_can_not_be_deleted'));
        }
        
        return $this->response->withArray(['message'=>  trans('complain.product_complain_deleted_successfully'), 'meta'=>['http_code'=>200]]);
    }
    
}