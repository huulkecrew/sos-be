<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use EllipseSynergie\ApiResponse\Contracts\Response;
use \Cache;

class BannerGroupsController extends BaseController {


    public function __construct(Response $response) {
        parent::__construct($response);
    }

    /**
    * @api {get} /api/v1/bannergroups/{id} Banner Groups
    * @apiSampleRequest /api/v1/bannergroups/1
    * @apiDescription Fetch Banner Screens For a Group
    * @apiPermission none
    * 
    * @apiGroup Banner
    * @apiName  fetchGroup 
    * 
    * @apiError BannerGroupNotFound   The <code>id</code> was not found.
    * 
    * @apiSuccess (Banner) {Object[]} Banner
    * @apiSuccess (Banner) {Integer}  Banner.id Banner ID.
    * @apiSuccess (Banner) {String}   Banner.image_url Image URL of banner.
    * @apiSuccess (Banner) {String}   Banner.description  Banner description.
    * @apiSuccess (Banner) {Object}  Banner.meta_info  Other meta info
    */
    public function fetch($id) {
        
        $banner_list      = Cache::remember('banner-group-'.$id, 14440, function() use ($id)
        {
            return \App\Models\Banner::where('banner_group_id', '=', $id)->orderBy('sort_order', 'asc')->get();
        });
        if(!$banner_list){
            return $this->response->errorNotFound(trans('banner.banner_group_not_found'));
        }
        return $this->response->withCollection($banner_list, new \App\Transformer\BannerTransformer, 'data', null, ['http_code'=>200]);
        
    }

}
