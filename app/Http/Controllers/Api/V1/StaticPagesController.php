<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Serializer\ArrayIncludeSerializer;

class StaticPagesController extends BaseController
{   
    public function __construct(Response $response) {
        parent::__construct($response);
    }
    
    /**
     * @api {get} /api/v1/pages Get Static pages
     * @apiName GetStaticPages
     * @apiGroup User
     * @apiDescription Get all page list
     * @apiPermission None
     * 
     * @apiSuccess (200) {Object[]} Success The pages list, Empty list if no page available.
     * 
     * @apiSuccessExample {json} Success-Response:
     * {
     *  "data": [
     *     {
     *       "id": 1,
     *       "name": "Payments ",
     *       "slug": "payment_info",
     *       "content": "What types of payment do you accept ?",
     *       "content_html": "Currently, Burpy accept all major US credit and debit cards. We apologize for the inconvenience as we do not accept store gift cards.",
     *       "meta_tag": "payments meta tag",
     *       "meta_description": "Payments description",
     *       "meta_keywords": "payment type",
     *       "created_at": "2016-06-10 05:12:17",
     *       "updated_at": "2016-06-10 06:15:23"
     *     },
     *     {
     *       "id": 2,
     *       "name": "About Us",
     *       "slug": "about-us",
     *       "content": "About uS",
     *       "content_html": "about Burpy",
     *       "meta_tag": "About | Burpy",
     *       "meta_description": "description",
     *       "meta_keywords": "What we work",
     *       "created_at": "2016-06-10 03:08:10",
     *       "updated_at": "2016-06-10 02:07:12"
     *     }
     *   ],
     *   "meta": {
     *     "http_code": 200
     *   }
     * }
     * 
     */
    public function get() {
        $pages = \App\Models\StaticPage::getPages();
        $this->response->getManager()->setSerializer(new ArrayIncludeSerializer());
        return $this->response->withCollection($pages, new \App\Transformer\StaticPageTransformer, 'data', null, ['http_code'=>200]);
    }
    
}
