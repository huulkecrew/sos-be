<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use App\Helpers\GlobalHelper as GlobalHelper;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests, Helpers;

    public function __construct(GlobalHelper $globalHelper){
        ini_set('display_errors', 1);
        ini_set("allow_url_fopen", 1);
    	header('Access-Control-Allow-Origin: *');

    	/* Initilizaing Global Helper */
    	$this->globalHelper = $globalHelper;

    	/* Rest API Response Codes */
	    $this->successParam 	= env('SUCCESS_PARAM');
	    $this->errorParam 		= env('ERROR_PARAM');
	    $this->missingParam 	= env('MISSING_PARAM');

        /* Email verification setting */
        $this->emailVerification = env('EMAIL_VERIFICATION');

        /* User types */
        $this->studentProfileType = env('STUDENT_USER_TYPE');
        $this->schoolProfileType = env('SCHOOL_USER_TYPE');
        $this->companyProfileType = env('COMPANY_USER_TYPE');
        $this->dateForDb = 'Y-m-d';
        $this->dateToDisplay = 'd-m-Y';
    }
}
