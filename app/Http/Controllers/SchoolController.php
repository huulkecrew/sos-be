<?php

namespace App\Http\Controllers;

use URL,
    Auth,
    Password,
    Mail,
    JWTAuth;
use App\User;
use App\Users;
use App\UserTypes;
use App\Students;
use App\StudentsWorkExperience;
use App\StudentsSchoolRecords;
use App\Company;
use App\Internship;
use App\InternshipCategories;
use App\InternshipJobCategories;
use App\InternshipRequest;
use App\School;
use App\Image;
use App\Email;
use App\AllModule;
use App\ModuleASchool;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;

class SchoolController extends Controller {

    /**
     * List schools
     * @param Request $request
     * @return $resp
     */
    public function listSchools(Request $request) {
        /* Fetching request param */
        $request = $request->only('keyword');

        /* Process Request */
        $schoolModel = new School();
        $schools = $schoolModel->getAllSchools($request);
        if (count($schools) > 0) {
            foreach ($schools as $val) {
                $user = Users::where(array('id' => $val->school_user_id))->first();
                if (isset($user->email)) {
                    $email = $user->email;
                    $phone = ($val->phone != null ) ? $val->phone : "";
                    $name = $val->name . " ";
                    $name .= (str_replace(' ', '', $val->type) != "") ? $val->type : "";
                    $name .= (str_replace(' ', '', $val->city) != "") ? ", " . $val->city : "";
                    $schoolData[] = array(
                        'school_id' => $val->id,
                        'school_user_id' => $val->school_user_id,
                        'phone' => $phone,
                        'email' => $email,
                        'name' => $name
                    );
                }
            }
            $resp = array('code' => $this->successParam, 'response' => array('schools' => $schoolData));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuna scuola trovata', 'error' => 'NOTHING_FOUND');
        }

        /* Return response */
        return $this->response->array($resp);
    }

    public function getSchoolDetail(Request $request) {
        /* Fetching request params */
        $request = $request->only('school_id');

        /* Checking for required parameters */
        $required_parameter = array('school_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid company user */
        $schoolData = School::where(array('id' => $request['school_id']))->first();

        if (empty($schoolData)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Scuola inesistente.', 'error' => 'SCHOOL_NOT_EXISTS');
            return $this->response->array($resp);
        }
        $userData = Users::where(array('id' => $schoolData->school_user_id))->first();
        $schoolData->email = $userData->email;
        $resp = array('code' => $this->successParam, 'response' => array('school' => $schoolData));

        return $this->response->array($resp);
    }

    /**
     * Create company profile
     * @param Request $rquest
     * @return $resp
     */
    public function createSchoolProfile(Request $request) {
        /* Fetching request params */
        $request = $request->only('school_user_id', 'name', 'type', 'code', 'country', 'province', 'zip', 'city', 'address', 'phone', 'website');

        /* Checking for required parameters */
        $required_parameter = array('school_user_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid company user */
        $checkCompanyUser = Users::where(array('id' => $request['school_user_id']))->first();
        if (empty($checkCompanyUser)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Company user is not exists.', 'error' => 'SCHOOL_USER_IS_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $school = new School();
        $school->school_user_id = $request['school_user_id'];
        $school->name = $request['name'];
        $school->type = $request['type'];
        $school->code = $request['code'];
        $school->country = $request['country'];
        $school->province = $request['province'];
        $school->zip = $request['zip'];
        $school->city = $request['city'];
        $school->address = $request['address'];
        $school->phone = $request['phone'];
        $school->website = $request['website'];
        $school->status = 1;
        $school->save();

        if (!empty($school->id)) {
            $resp = array('code' => $this->successParam, 'response' => array('profile_data' => $school));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Impossibile creare profilo Scuola, riprova.', 'error' => 'UNABLE_TO_CREATE_SCHOOL_PROFILE');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Complete school profile
     * @param Request $rquest
     * @return $resp
     */
    public function completeShoolProfile(Request $request) {
        /* Fetching request params */
        $request = $request->only('school_id', 'name', 'type', 'code', 'country', 'province', 'zip', 'city', 'address', 'phone', 'website');

        /* Checking for required parameters */
        $required_parameter = array('school_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid school */
        $checkCompany = School::where(array('id' => $request['school_id']))->first();
        if (empty($checkCompany)) {
            $resp = array('code' => $this->errorParam, 'message' => 'School profile is not exists.', 'error' => 'SCHOOL_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $updateData = $request;
        unset($updateData['school_id']);

        /* Updating records */
        School::where(array('id' => $request['school_id']))->update($updateData);

        /* Fetching profile info */
        $profileData = School::where(array('id' => $request['school_id']))->first();
        $profileData->school_logo = (!empty($profileData->school_logo)) ? URL::to('/') . env('SCHOOL_LOGO_PATH') . $profileData->school_logo : URL::to('/') . env('SCHOOL_DEFAULT_PIC');

        $resp = array('code' => $this->successParam, 'message' => 'Profilo aggiornato correttamente.', 'response' => array('profile_data' => $profileData));
        return $this->response->array($resp);
    }

    /**
     * Response to the internship
     * STATUS = 0 PENDIG
     * STATUS = 1 APPROVED
     * STATUS = 2 REJECTED
     * @param type $param
     * @return type
     */
    public function responseInternshipAppliers(Request $request) {
        /* Fetching request params */
        $request = $request->only('school_id', "internship_request_id", "status");


        /* Checking for required parameters */
        $required_parameter = array('school_id', "internship_request_id", "status");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        $internship = new InternshipRequest();
        /* Check for already apply */
        if ($request["status"] != Internship::$CONFIRMED && $request["status"] != Internship::$REJECTED) {
            $resp = array('code' => $this->errorParam, 'message' => 'Status wrong ', 'error' => 'WORNG_STATUS');
        } else {
            $records_updated = $internship->updateInternshipRequest($request);
            if ($records_updated >= 0) {
                $this->emailResponseInternship($request);
                $resp = array('code' => $this->successParam, 'response' => array('status' => $records_updated . " records updated"));
            } else {
                $resp = array('code' => $this->errorParam, 'message' => 'Nessuno stage trovato.', 'error' => 'NOTHING_FOUND');
            }
        }


        return $this->response->array($resp);
    }

    /**
     * Check all intership to which studendts has applied
     * STATUS = 0 PENDIG
     * STATUS = 1 APPROVED
     * STATUS = 2 REJECTED
     * @param type $param
     * @return type
     */
    public function schoolInternshipAppliers(Request $request) {
        /* Fetching request params */
        $request = $request->only('school_id', "offset", "limit", "internship_id", "internship_request_id");

        /* Checking for required parameters */
        $required_parameter = array('school_id', "offset", "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $internship = new InternshipRequest();
        /* Check for already apply */
        $arrayInternshipObj = $internship->getAppliersInterhipBySchool($request);
        $count = $internship->countAppliersInterhipBySchool($request);

        $arrayInternshipObj = Image::getImageProfile("School", $arrayInternshipObj);
        $arrayInternshipObj = Image::getImageProfile("Student", $arrayInternshipObj);
        $arrayInternshipObj = Image::getImageProfile("Company", $arrayInternshipObj);
        $this->getCategoriesPerIntership($arrayInternshipObj);
        if (count($arrayInternshipObj) > 0) {
            $next = (count($arrayInternshipObj) >= $request["limit"]) ? TRUE : FALSE;

            $resp = array('code' => $this->successParam, 'response' => array('internship_applier' => $arrayInternshipObj, "has_next" => $next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuno stage trovato', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }

    /**
     * To be insert in the model
     * Get Intership categorie for each internship
     * @param type $interships
     * @return type
     */
    public function getCategoriesPerIntership($interships) {
        foreach ($interships as $key => $value) {
            $interships[$key]->internship_categories = InternshipJobCategories::where(array('internship_id' => $value->id))->get();
        }
        return $interships;
    }

    public function emailResponseInternship($request) {
        $internship_request = InternshipRequest::where(array('id' => $request['internship_request_id']))->first();
        $internship = Internship::where(array('id' => $internship_request->internship_id))->first();
        $student = Students::where(array('id' => $internship_request->student_id))->first();
        $users = Users::where(array('id' => $student->student_user_id))->first();

        $company = Company::where(array('id' => $internship->company_id))->first();
        $users_company = Users::where(array('id' => $company->company_user_id))->first();

        $school = School::where(array('id' => $internship->school_id))->first();
        if (isset($school)) {
            $users_school = Users::where(array('id' => $school->school_user_id))->first();
            $school_email = $users_school->email;
            $school_name = $users_school->name;
        } else {
            $school_email = $internship_request->school_email;
            $school_name = $internship_request->school_name;
        }

        $data = array(
            'internship_id' => $internship->id,
            'internship_data' => $internship,
            'student_id' => $student->id,
            'student_name' => ucfirst($student->first_name . ' ' . $student->last_name),
            'student_email' => $users->email,
            'company_id' => $internship->company_id,
            'company_name' => $internship->company_name,
            'company_email' => $users_company->email,
            'school_id' => $request['school_id'],
            'school_email' => $school_email,
            'school_name' => $school_name
        );

        $internshiplink = env('FRONT_END_URL') . env('VIEW_STAGE_FRONT_SLUG') . $data['internship_id'];
        /* Send notification to Student */
        $studentEmail = $data['student_email'];
        /* Send Email */
        if ($request["status"] == Internship::$CONFIRMED) {
            /* Send notification to School */
            $data_content = ($internship_request->is_approved_by_company == Internship::$CONFIRMED) ? Email::getEmailContent("REQUEST-INTERNSHIP-CONFIRMED-BY-SCHOOL-AFTER-COMPANY", $data) : $data_content = Email::getEmailContent("REQUEST-INTERNSHIP-CONFIRMED-BY-SCHOOL-NOT-YET-BY-COMPANY", $data);
        } elseif ($request["status"] == Internship::$REJECTED) {
            $data_content = Email::getEmailContent("REQUEST-INTERNSHIP-REJECTED-BY-SCHOOL", $data);
        }
        $html = Email::getHtmlContent($data_content, $internshiplink);
        $result = Email::sendEmail($studentEmail, $html, $data_content);


        /* Send notification to School that has just to be formalised the request internship */

        if ($request["status"] == Internship::$CONFIRMED && $internship_request->is_approved_by_company == Internship::$CONFIRMED) {
            $data_content = Email::getEmailContent("TO-SCHOOL-COMPANY-HAS-APPROVED-BEFORE", $data);
            $schoolEmail = $data['school_email'];
            /* Send Email */
            $html = Email::getHtmlContent($data_content, $internshiplink);
            $result = Email::sendEmail($schoolEmail, $html, $data_content);

            //Start the module 
            $module = new AllModule;
            $module->createAllModule($internship_request->id);
        }


        /* Sending email to company, to inform that also the school has approved the internship request  */
        if ($request["status"] == Internship::$CONFIRMED && $internship_request->is_approved_by_company == Internship::$CONFIRMED) {
            $companyEmail = $data['company_email'];
            /* Send Email */
            $data_content = Email::getEmailContent("TO-COMPANY-SCHOOL-HAS-APPROVED-AFTER", $data);
            $html = Email::getHtmlContent($data_content, $internshiplink);
            $result = Email::sendEmail($companyEmail, $html, $data_content);
        }

        return true;
    }

    public function getModuleASchool(Request $request) {
        /* Fetching request params */
        $request = $request->only('school_id', "offset", "limit", "status", "internship_request_id");

        /* Checking for required parameters */
        $required_parameter = array('school_id', "offset", "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $moduleASchool = new ModuleASchool;
        /* Check for already apply */
        $data = $moduleASchool->getModuleASchool($request);

//        $count  = $internship->countIntershipByCompany($request);
        $data = Image::getImageProfile("Company", $data);
        $data = Image::getImageProfile("Student", $data);

        if (count($data) > 0) {
            $next = (count($data) >= $request["limit"]) ? TRUE : FALSE;

            $resp = array('code' => $this->successParam, 'response' => array('data' => $data, "has_next" => $next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessun risultato trovato.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }

    public function updateModuleASchool(Request $request) {
        /* Fetching request params */
        $request = $request->only('school_id', "internship_request_id", "status", 'ls_1_a', 'ls_1_b', 'ls_1_c', 'ls_1_d', 'ls_7_a', 'ls_7_b', 'ls_7_c', 'ls_15_a',
                "ls_1_c_a","ls_1_c_b","ls_1_c_c","ls_1_c_d","ls_1_c_e");
        /* Checking for required parameters */
        $required_parameter = array('school_id', "internship_request_id", "status");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        $data = ModuleASChool::where(array('internship_request_id' => $request['internship_request_id']))->update($request);
        $resp = array('code' => $this->successParam, 'message' => 'Modifiche effettuate correttamente.', 'response' => array('data' => $data));
        return $this->response->array($resp);
    }

}
