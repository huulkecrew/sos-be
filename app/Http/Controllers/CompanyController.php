<?php

namespace App\Http\Controllers;

use URL,
    Auth,
    Password,
    Mail,
    JWTAuth;
use App\User;
use App\Users;
use App\UserTypes;
use App\Students;
use App\StudentsWorkExperience;
use App\StudentsSchoolRecords;
use App\Company;
use App\Internship;
use App\InternshipRequest;
use App\InternshipCategories;
use App\InternshipJobCategories;
use App\Image;
use App\Email;
use App\School;
use App\AllModule;
use App\ModuleACompany;
use App\ModuleECompany;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;

class CompanyController extends Controller {

    /**
     * Create company profile
     * @param Request $rquest
     * @return $resp
     */
    public function createCompanyProfile(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_user_id', 'first_name', 'last_name', 'company_name', 'vat_number', 'country', 'state', 'city', 'zip_code', 'address', 'phone_number', 'website');

        /* Checking for required parameters */
        $required_parameter = array('company_user_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid company user */
        $checkCompanyUser = Users::where(array('id' => $request['company_user_id']))->first();
        if (empty($checkCompanyUser)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Utente inesistente.', 'error' => 'COMPANY_USER_IS_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $company = new Company();
        $company->company_user_id = $request['company_user_id'];
        $company->first_name = isset($request['first_name']) ? $request['first_name'] : "";
        $company->last_name = isset($request['last_name']) ? $request['last_name'] : "";
        $company->company_name = $request['company_name'];
        $company->vat_number = $request['vat_number'];
        $company->country = $request['country'];
        $company->state = $request['state'];
        $company->city = $request['city'];
        $company->zip_code = $request['zip_code'];
        $company->address = $request['address'];
        $company->phone_number = $request['phone_number'];
        $company->website = isset($request['website']) ? $request['website'] : "";
        $company->status = 1;
        $company->save();

        if (!empty($company->id)) {
            $resp = array('code' => $this->successParam, 'response' => array('profile_data' => $company));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Impossibile creare profilo società, riprova.', 'error' => 'UNABLE_TO_CREATE_COMPANY_PROFILE');
        }

        /* Return Response */
        return $this->response->array($resp);
    }

    /**
     * Complete company profile
     * @param Request $rquest
     * @return $resp
     */
    public function completeCompanyProfile(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id', 'first_name', 'last_name', 'company_name', 'vat_number', 'country', 'state', 'city', 'zip_code', 'address', 'phone_number', 'website');

        /* Checking for required parameters */
        $required_parameter = array('company_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid company */
        $checkCompany = Company::where(array('id' => $request['company_id']))->first();
        if (empty($checkCompany)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Profilo non esistente.', 'error' => 'COMPANY_NOT_EXISTS');
            return $this->response->array($resp);
        }

        /* Process Request */
        $updateData = $request;
        unset($updateData['company_id']);

        /* Updating records */
        Company::where(array('id' => $request['company_id']))->update($updateData);

        /* Fetching profile info */
        $profileData = Company::where(array('id' => $request['company_id']))->first();
        $profileData->company_logo = (!empty($profileData->company_logo)) ? URL::to('/') . env('COMPANY_LOGO_PATH') . $profileData->company_logo : URL::to('/') . env('COMPANY_DEFAULT_PIC');

        $resp = array('code' => $this->successParam, 'message' => 'Modifiche effettuate correttamente.', 'response' => array('profile_data' => $profileData));
        return $this->response->array($resp);
    }

    /**
     * Check all intership to which the user has been upplied
     * STATUS = 0 PENDIG
     * STATUS = 1 APPROVED
     * STATUS = 2 REJECTED
     * @param type $param
     * @return type
     */
    public function companyListInternship(Request $request) {


        /* Fetching request params */
        $request = $request->only('company_id', "offset", "limit");

        /* Checking for required parameters */
        $required_parameter = array('company_id', "offset", "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $internship = new Internship();
        /* Check for already apply */
        $arrayInternshipObj = $internship->getIntershipByCompany($request);
//        $count  = $internship->countIntershipByCompany($request);
        $arrayInternshipObj = Image::getImageProfile("Company", $arrayInternshipObj);
        $this->getCategoriesPerIntership($arrayInternshipObj);
        if (count($arrayInternshipObj) > 0) {
            $next = (count($arrayInternshipObj) >= $request["limit"]) ? TRUE : FALSE;

            $resp = array('code' => $this->successParam, 'response' => array('internship' => $arrayInternshipObj, "has_next" => $next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuno stage trovato.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }

    /**
     * To be insert in the model
     * Get Intership categorie for each internship
     * @param type $interships
     * @return type
     */
    public function getCategoriesPerIntership($interships) {
        foreach ($interships as $key => $value) {
            $interships[$key]->internship_categories = InternshipJobCategories::where(array('internship_id' => $value->id))->get();
        }
        return $interships;
    }

    /**
     * Check all intership to which the user has been upplied
     * STATUS = 0 PENDIG
     * STATUS = 1 APPROVED
     * STATUS = 2 REJECTED
     * @param type $param
     * @return type
     */
    public function companyInternshipAppliers(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id', "offset", "limit", "internship_id");

        /* Checking for required parameters */
        $required_parameter = array('company_id', "offset", "limit", "internship_id");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $internship = new InternshipRequest();
        /* Check for already apply */
        $arrayInternshipObj = $internship->getAppliersInterhipByCompany($request);
        $count = $internship->countAppliersInterhipByCompany($request);

        $arrayInternshipObj = Image::getImageProfile("Company", $arrayInternshipObj);
        $arrayInternshipObj = Image::getImageProfile("Student", $arrayInternshipObj);
        $this->getCategoriesPerIntership($arrayInternshipObj);
        if (count($arrayInternshipObj) > 0) {
            $next = (count($arrayInternshipObj) >= $request["limit"]) ? TRUE : FALSE;

            $resp = array('code' => $this->successParam, 'response' => array('internship_applier' => $arrayInternshipObj, "has_next" => $next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessuno stage trovato.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }

    /**
     * Response to the internship
     * STATUS = 0 PENDIG
     * STATUS = 1 APPROVED
     * STATUS = 2 REJECTED
     * @param type $param
     * @return type
     */
    public function responseInternshipAppliers(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id', "internship_request_id", "status");


        /* Checking for required parameters */
        $required_parameter = array('company_id', "internship_request_id", "status");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }
        $internship = new InternshipRequest();
        /* Check for already apply */
        if ($request["status"] != Internship::$CONFIRMED && $request["status"] != Internship::$REJECTED) {
            $resp = array('code' => $this->errorParam, 'message' => 'Status wrong ', 'error' => 'WORNG_STATUS');
        } else {
            $records_updated = $internship->updateInternshipRequest($request);
            if ($records_updated >= 0) {
                $this->emailResponseInternship($request);
                $resp = array('code' => $this->successParam, 'response' => array('status' => $records_updated . " records updated"));
            } else {
                $resp = array('code' => $this->errorParam, 'message' => 'Nessuno stage trovato.', 'error' => 'NOTHING_FOUND');
            }
        }


        return $this->response->array($resp);
    }

    public function emailResponseInternship($request) {
        $internship_request = InternshipRequest::where(array('id' => $request['internship_request_id']))->first();
        $internship = Internship::where(array('id' => $internship_request->internship_id))->first();
        $student = Students::where(array('id' => $internship_request->student_id))->first();
        $users = Users::where(array('id' => $student->student_user_id))->first();

        $company = Company::where(array('id' => $internship->company_id))->first();
        $users_company = Users::where(array('id' => $company->company_user_id))->first();

        $school = School::where(array('id' => $internship->school_id))->first();
        if (isset($school)) {
            $users_school = Users::where(array('id' => $school->school_user_id))->first();
            $school_email = $users_school->email;
            $school_name = $users_school->name;
        } else {
            $school_email = $internship_request->school_email;
            $school_name = $internship_request->school_name;
        }

        $data = array(
            'internship_id' => $internship->id,
            'internship_data' => $internship,
            'student_id' => $student->id,
            'student_name' => ucfirst($student->first_name . ' ' . $student->last_name),
            'student_email' => $users->email,
            'company_id' => $internship->company_id,
            'company_name' => $internship->company_name,
            'company_email' => $users_company->email,
            'school_id' => $internship->school_id,
            'school_email' => $school_email,
            'school_name' => $school_name
        );

        $internshiplink = env('FRONT_END_URL') . env('VIEW_STAGE_FRONT_SLUG') . $data['internship_id'];

        /* Send notification to Student */
        $studentEmail = $data['student_email'];
        /* Send Email */
        if ($request["status"] == Internship::$CONFIRMED) {
            /* Send notification to School */
            $data_content = ($internship_request->is_approved_by_school == Internship::$CONFIRMED) ? Email::getEmailContent("REQUEST-INTERNSHIP-CONFIRMED-BY-COMPANY-AFTER-SCHOOL", $data) : Email::getEmailContent("REQUEST-INTERNSHIP-CONFIRMED-BY-COMPANY-NOT-YET-BY-SCHOOL", $data);
        } elseif ($request["status"] == Internship::$REJECTED) {
            $data_content = Email::getEmailContent("REQUEST-INTERNSHIP-REJECTED-BY-COMPANY", $data);
        }
        $html = Email::getHtmlContent($data_content, $internshiplink);
        $result = Email::sendEmail($studentEmail, $html, $data_content);



        /* Send notification to Company, that the form has just to be formalised */

        if ($request["status"] == Internship::$CONFIRMED && $internship_request->is_approved_by_school == Internship::$CONFIRMED) {
            $companyEmail = $data['company_email'];
            /* Send Email */
            $data_content = Email::getEmailContent("TO-COMPANY-SCHOOL-HAS-APPROVED-BEFORE", $data);
            $html = Email::getHtmlContent($data_content, $internshiplink);
            $result = Email::sendEmail($companyEmail, $html, $data_content);

            //Start the module 
            $module = new AllModule;
            $module->createAllModule($internship_request->id);
        }

        /* Sending email to school, to inform that also the company has approved the internship request  */
        if ($request["status"] == Internship::$CONFIRMED && $internship_request->is_approved_by_school == Internship::$CONFIRMED) {
            $schoolEmail = $data['school_email'];
            /* Send Email */
            $data_content = Email::getEmailContent("TO-SCHOOL-COMPANY-HAS-APPROVED-AFTER", $data);
            $html = Email::getHtmlContent($data_content, $internshiplink);
            $result = Email::sendEmail($schoolEmail, $html, $data_content);
        }

        return true;
    }

    public function getCompanyDetail(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id');

        /* Checking for required parameters */
        $required_parameter = array('company_id');
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        /* Check for valid company user */
        $companyData = Company::where(array('id' => $request['company_id']))->first();

        if (empty($companyData)) {
            $resp = array('code' => $this->errorParam, 'message' => 'Scuola inesistente.', 'error' => 'SCHOOL_NOT_EXISTS');
            return $this->response->array($resp);
        }
        $userData = Users::where(array('id' => $companyData->company_user_id))->first();
        $companyData->email = $userData->email;
        $resp = array('code' => $this->successParam, 'response' => array('company' => $companyData));

        return $this->response->array($resp);
    }

    public function updateModuleACompany(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id', "internship_request_id", "status", 'lc_1', 'lc_5_a', 'lc_5_b', 'lc_6', 'lc_8', 'lc_9', 'lc_10', 'lc_11_a', 'lc_11_b', 'lc_11_c', 'lc_11_d', 'lc_12_a', 'lc_12_b', 'lc_13_a', 'lc_13_b', 'lc_14_a', 'lc_14_b', 'lc_15',
                "lc_c_a","lc_c_b","lc_c_c","lc_c_d","lc_d_1","lc_d_2","lc_d_3","lc_d_4","lc_d_5","lc_d_6","lc_d_7_a","lc_d_7_b","lc_d_8","lc_d_9","lc_d_10","lc_d_11","lc_d_12","lc_d_13","lc_d_14");

        /* Checking for required parameters */
        $required_parameter = array('company_id', "internship_request_id", "status");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        $data = ModuleACompany::where(array('internship_request_id' => $request['internship_request_id']))->update($request);
        $resp = array('code' => $this->successParam, 'message' => 'Modifiche effettuate correttamente.', 'response' => array('data' => $data));
        return $this->response->array($resp);
    }
    
    public function updateModuleECompany(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id', "internship_request_id", "status",
                "module_company_e_1_a", "module_company_e_1_b","module_company_e_1_c","module_company_e_1_d","module_company_e_1_e",
                "module_company_e_2","module_company_e_3","module_company_e_4","module_company_e_5","module_company_e_6","module_company_e_7","module_company_e_8",
                "module_company_e_9","module_company_e_10","module_company_e_11","module_company_e_12","module_company_e_13","module_company_e_14","module_company_e_15","module_company_e_16",
                "module_company_e_17","module_company_g_c2_1_a","module_company_g_c2_1_b","module_company_g_c2_1_c","module_company_g_c2_1_d","module_company_g_d_1",
                "module_company_g_d_2","module_company_g_d_3","module_company_g_d_4","module_company_g_d_5","module_company_g_d_6","module_company_g_e_1","module_company_g_e_2","module_company_g_e_3",
                "module_company_g_e_4","module_company_g_e_5","module_company_g_e_6","module_company_g_e_7","module_company_g_e_8","module_company_g_e_9",
                "module_company_g_e_10","module_company_g_e_11","module_company_g_e_12","module_company_g_e_13","module_company_g_e_14","module_company_g_e_15","module_company_g_e_16","module_company_g_e_17");

        /* Checking for required parameters */
        $required_parameter = array('company_id', "internship_request_id", "status");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }

        $data = ModuleECompany::where(array('internship_request_id' => $request['internship_request_id']))->update($request);
        $resp = array('code' => $this->successParam, 'message' => 'Modifiche effettuate correttamente.', 'response' => array('data' => $data));
        return $this->response->array($resp);
    }

    
    public function getModuleACompany(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id', "offset", "limit" , "status" , "internship_request_id");

        /* Checking for required parameters */
        $required_parameter = array('company_id', "offset", "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $moduleACompany = new ModuleACompany();
        /* Check for already apply */
        $data = $moduleACompany->getModuleACompany($request);
    
//        $count  = $internship->countIntershipByCompany($request);
        $data = Image::getImageProfile("School", $data);
        $data = Image::getImageProfile("Student", $data);
        
        if (count($data) > 0) {
            $next = (count($data) >= $request["limit"]) ? TRUE : FALSE;

            $resp = array('code' => $this->successParam, 'response' => array('data' => $data, "has_next" => $next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessun risultato trovato.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }
    
    public function getModuleECompany(Request $request) {
        /* Fetching request params */
        $request = $request->only('company_id', "offset", "limit" , "status" , "internship_request_id");

        /* Checking for required parameters */
        $required_parameter = array('company_id', "offset", "limit");
        $chk_error = check_required_value($required_parameter, $request);
        if ($chk_error) {
            $resp = array('code' => $this->missingParam, 'message' => 'You have missed a parameter ' . $chk_error['param'], 'error' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            return $this->response->array($resp);
        }


        $moduleACompany = new ModuleECompany();
        /* Check for already apply */
        $data = $moduleACompany->getModuleECompany($request);
    
//        $count  = $internship->countIntershipByCompany($request);
        $data = Image::getImageProfile("School", $data);
        $data = Image::getImageProfile("Student", $data);
        
        if (count($data) > 0) {
            $next = (count($data) >= $request["limit"]) ? TRUE : FALSE;

            $resp = array('code' => $this->successParam, 'response' => array('data' => $data, "has_next" => $next));
        } else {
            $resp = array('code' => $this->errorParam, 'message' => 'Nessun risultato trovato.', 'error' => 'NOTHING_FOUND');
        }

        return $this->response->array($resp);
    }

}
