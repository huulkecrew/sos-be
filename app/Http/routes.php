<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

$api = app('Dingo\Api\Routing\Router');

Route::get('/', function () {
    return view('welcome');
});

$api->version('v1', function ($api) {
    /* Get all user types */
    $api->get('getallusertypes', 'App\Http\Controllers\UsersController@getAlluserTypes');

    /* Social Login & Register */
    $api->post('dosocial', 'App\Http\Controllers\UsersController@doSocial');

    /* Register User */
    $api->post('register', 'App\Http\Controllers\UsersController@register');

    /* Login User */
    $api->post('login', 'App\Http\Controllers\UsersController@login');

    /* Verify Email */
    $api->post('verifyemail', 'App\Http\Controllers\UsersController@verifyEmail');

    /* Reset Password */
    $api->post('resetpassword', 'App\Http\Controllers\UsersController@resetPassword');

    /* Change Password */
    $api->post('changepassword', 'App\Http\Controllers\UsersController@changePassword');

    /* Get all internship categories */
    $api->get('getinternshipcategories', 'App\Http\Controllers\InternshipController@getInternshipCategories');

    /* Get internship detail */
    $api->post('getinternshipdetail', 'App\Http\Controllers\InternshipController@getInternshipDetails');

    /* List all internships */
    $api->post('listinternship', 'App\Http\Controllers\InternshipController@listAllInternships');

    /* List all internships by company id */
    $api->post('listinternshipbycompanyid', 'App\Http\Controllers\InternshipController@listCompanyAllInternships');

    /* List Schools */
    $api->post('listschools', 'App\Http\Controllers\SchoolController@listSchools');
    
    /* Detail  Schools */
    $api->post('getschooldetail', 'App\Http\Controllers\SchoolController@getSchoolDetail');
    
    /* Detail  Get Company Detail */
    $api->post('getcompanydetail', 'App\Http\Controllers\CompanyController@getCompanyDetail');
    /* Detail  Get Company Detail */
    $api->post('sendmailchangepassword', 'App\Http\Controllers\UsersController@sendMailChangePassword');
});

/* Authenticated API */
$api->version('v1', ['middleware' => 'jwt.auth'], function ($api) {
    /* Fetch user from token */
    $api->get('tokencheck', 'App\Http\Controllers\UsersController@tokencheck');

    /* Change user password */
    $api->post('changeuserpassword', 'App\Http\Controllers\UsersController@editUserPassword');

    /* Complete Student Profile */
    $api->post('completestudentprofile', 'App\Http\Controllers\StudentsController@completeStudentProfile');

    /* Upload Student Profile Pic */
    $api->post('uploadstudentprofilepic', 'App\Http\Controllers\UsersController@uploadStudentProfilePic');

    /* Upload company logo */
    $api->post('uploadcompanylogo', 'App\Http\Controllers\UsersController@uploadCompanyLogo');

    /* Upload company logo */
    $api->post('uploadschoollogo', 'App\Http\Controllers\UsersController@uploadSchoolLogo');

    /* Get user profile data */
    $api->post('getuserdata', 'App\Http\Controllers\UsersController@getUserProfileData');

    /* Add student school records */
    $api->post('addstudentschoolrecord', 'App\Http\Controllers\StudentsController@addStudentSchoolRecords');

    /* Get student school records */
    $api->post('getstudentschoolrecords', 'App\Http\Controllers\StudentsController@getStudentSchoolRecords');

    /* Delete Student School Records */
    $api->post('deletestudentschoolrecord', 'App\Http\Controllers\StudentsController@deleteStudentSchoolRecord');

    /* Add student work experience */
    $api->post('addstudentworkexperience', 'App\Http\Controllers\StudentsController@addStudentWorkExperience');

    /* Get student work experience */
    $api->post('getstudentworkexperience', 'App\Http\Controllers\StudentsController@getStudentWorkExperiences');

    /* Delete Student Work Experience */
    $api->post('deletestudentworkexperience', 'App\Http\Controllers\StudentsController@deleteStudentWorkExperience');

    /* Add certification of students */
    $api->post('addstudentcertification', 'App\Http\Controllers\StudentsController@addStudentCertification');

    /* Get student certification experience */
    $api->post('getstudentcertification', 'App\Http\Controllers\StudentsController@getStudentCertification');

    /* Delete Student certification */
    $api->post('deletestudentcertification', 'App\Http\Controllers\StudentsController@deleteStudentCertification');


    /* Create company profile */
    $api->post('createcompanyprofile', 'App\Http\Controllers\CompanyController@createCompanyProfile');

    /* Complete Company Profile */
    $api->post('completecompanyprofile', 'App\Http\Controllers\CompanyController@completeCompanyProfile');

    /* Add internship */
    $api->post('addinternship', 'App\Http\Controllers\InternshipController@addInternship');

    /* Edit internship */
    $api->post('editinternship', 'App\Http\Controllers\InternshipController@editInternship');

    /* Delete Internship */
    $api->post('deleteinternship', 'App\Http\Controllers\InternshipController@deleteInternship');

    /* Apply on internship */
    $api->post('applyoninternship', 'App\Http\Controllers\InternshipController@applyOnInternship');

    /* List intership applied with status */
    $api->post('internshipappliedlist', 'App\Http\Controllers\InternshipController@internshipAppliedList');

    /* Detail intership applied with status */
    $api->post('internshipapplieddetail', 'App\Http\Controllers\InternshipController@internshipAppliedDetail');

    /* Student List notificaton internship applied with */
    $api->post('notificationstudentlist', 'App\Http\Controllers\NotificationController@notificationStudentList');
    /* Company List notificaton internship applied with */
    $api->post('notificationcompanylist', 'App\Http\Controllers\NotificationController@notificationCompanyList');
    /* Student List notificaton internship applied with */
    $api->post('notificationschoollist', 'App\Http\Controllers\NotificationController@notificationSchoolList');

    //COMPANY SERVICES
    /* Company Profile List Internship */
    $api->post('companylistinternship', 'App\Http\Controllers\CompanyController@companyListInternship');

    /* Company Internship  Appliers  */
    $api->post('companyinternshipappliers', 'App\Http\Controllers\CompanyController@companyInternshipAppliers');

    /* Compay Update - Internship Request  */
    $api->post('updateinternshiprequest', 'App\Http\Controllers\CompanyController@responseInternshipAppliers');


    /* Create School profile */
    $api->post('createschoolprofile', 'App\Http\Controllers\SchoolController@createSchoolProfile');

    /* Complete School Profile */
    $api->post('completeschoolprofile', 'App\Http\Controllers\SchoolController@completeShoolProfile');

    /* School Update - Internship Request  */
    $api->post('updateinternshiprequestschool', 'App\Http\Controllers\SchoolController@responseInternshipAppliers');

    /* School Internship  Appliers  */
    $api->post('schoolinternshipappliers', 'App\Http\Controllers\SchoolController@schoolInternshipAppliers');
    
    
    /*Module A*/
    
    $api->post('updatemoduleacompany', 'App\Http\Controllers\CompanyController@updateModuleACompany');
    $api->post('updatemoduleaschool', 'App\Http\Controllers\SchoolController@updateModuleASchool');
    $api->post('updatemoduleecompany', 'App\Http\Controllers\CompanyController@updateModuleECompany');
    
    $api->post('getmoduleacompany', 'App\Http\Controllers\CompanyController@getModuleACompany');
    $api->post('getmoduleecompany', 'App\Http\Controllers\CompanyController@getModuleECompany');
    $api->post('getmoduleaschool', 'App\Http\Controllers\SchoolController@getModuleASchool');
    
    /* Module B */
    
    $api->post('updatemodulebstudent', 'App\Http\Controllers\StudentsController@updateModuleBStudent');
    $api->post('updatemodulefstudent', 'App\Http\Controllers\StudentsController@updateModuleFStudent');
    $api->post('getmodulebstudent', 'App\Http\Controllers\StudentsController@getModuleBStudent');
    $api->post('getmodulefstudent', 'App\Http\Controllers\StudentsController@getModuleFStudent');
    
    //

});
