<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ModuleACompany extends Model
{
    protected $table = 'module_a_company';
    
    public function getModuleACompany($param, $count=false){
        /* Check for valid company user */
        
        
        $module_a_company = DB::table($this->table)
                ->join('internship_request', 'internship_request.id', '=', 'module_a_company.internship_request_id')
                ->join('students', 'students.id', '=', 'internship_request.student_id')
                ->join('school', 'school.id', '=', 'internship_request.school_id')
                ->join('internship', 'internship.id', '=', 'internship_request.internship_id')
                ->select('internship_request.*', "students.*","school.name", "school.school_logo","internship.*", "internship_request.id as internship_request_id")
                ->where('module_a_company.company_id', '=', $param["company_id"])
                ->orderBy('module_a_company.updated_at', 'desc');
        if(isset($param["status"]) && $param["status"] > 0 ){
            $module_a_company->where('module_a_company.status', '=', $param["status"]);
        }
        if( isset($param["internship_request_id"]) && $param["internship_request_id"]>0 ){

            $module_a_company->where('module_a_company.internship_request_id', '=', $param["internship_request_id"])
                    ->addSelect('module_a_company.*');
            $data = $module_a_company->first();
        } else {
            if($count){
                $data = $module_a_company->count(); 
            } else {
                $module_a_company->limit($param['limit'])
                        ->offset($param['offset']);
                $data = $module_a_company->get(); 
            }
        }


        return $data;
        
        
    }
}
