<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternshipCategories extends Model
{
    protected $table = 'internship_categories';
}
