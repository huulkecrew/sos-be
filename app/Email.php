<?php

namespace App;

use URL;
use DB;
use Mail;
use PHPMailer;
use Illuminate\Database\Eloquent\Model;

class Email extends Model {

    public static function getEmailContent($type_email, $title_to = "", $title_extra = "") {
        switch ($type_email) {
            case "EMAIL-VERIFICATION":
                $subject = "StudentsOnStage - Verifica Email";
                $title = "Verifica Email";
                $body = "Benvenuto su StudentsOnStage.<BR><BR> Verfica l'email cliccando su questo link.";
                $label_call_to_action = "VERIFICA EMAIL";
                break;
            case "RESET-PASSWORD":
                $subject = "StudentsOnStage - Reimposta Password";
                $title = "Reimposta Password";
                $body = "Benvenuto su StudentsOnStage.<BR><BR> Reimposta la Password cliccando su questo link.";
                $label_call_to_action = "REIMPOSTA PASSWORD";
                break;
            case "REGISTRATION-STUDENT":
                $subject = "Benvenuto su StudentsOnStage";
                $title = $title_to . " Benvenuto su StudentsOnStage, il portale per l'alternanza scuola-lavoro.<br>Clicca sul seguente link per attivare il tuo account:";
                $body = "Grazie per esserti iscritto su StudentsOnStage. Ora puoi cercare stage lavorativi in tutta Italia!
                    <BR><BR>Prima completa il tuo profilo con i dati personali, i voti dell’ultima pagella, le certificazioni e i progetti scolastici ed extrascolastici.
                    <BR><BR>Inserisci anche il nome della tua scuola: se non dovesse comparire nei suggerimenti, contatta il tuo Referente per l'Orientamento per far iscrivere la scuola al nostro portale.
                    Se verrai selezionato da un’azienda, avrai bisogno anche dell’approvazione della scuola per formalizzare lo stage!
                    <BR><BR>Buon Lavoro!
                    <BR>Il Team di StudentsOnStage
                    <BR>Per qualsiasi informazione, visita il sito studentsonstage.it o scrivi a info@studentsonstage.it";
                $label_call_to_action = "VERIFICA EMAIL";
                break;
            case "REGISTRATION-COMPANY":
                $subject = "Benvenuti su StudentsOnStage";
                $title = $title_to . " Benvenuti su StudentsOnStage, il portale per l'alternanza scuola-lavoro.<br>Cliccate sul seguente link per attivare il tuo account: ";
                $body = "Grazie per esservi iscritti su StudentsOnStage. Ora potete pubblicare offerte di stage!
                    <BR><BR>Per fare ciò, dovete compilare tutti i campi dell’annuncio nella sezione “Crea stage”. Dopodiché, gli studenti iscritti al portale potranno inviare le proprie candidature.
                    <BR>Grazie al format standardizzato dei Curricula Vitae, sarà semplice per voi selezionare i candidati più idonei.
                    <BR><BR>Potete inoltre creare differenti profili aziendali, se per voi più comodo per la gestione degli stage (ad esempio, varie sedi).
                    <BR><BR>Buon lavoro!
                    <BR><BR>Il Team di StudentsOnStage
                    <BR><BR>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "VERIFICA EMAIL";
                break;
            case "REGISTRATION-SCHOOL":
                $subject = "Benvenuti su StudentsOnStage";
                $title = $title_to . "Benvenuti su StudentsOnStage, il portale per l'alternanza scuola-lavoro.<br>Clicca sul seguente link per attivare il tuo account:";
                $body = "Grazie per esservi iscritti su StudentsOnStage. 
                    <br><br>Ora potete valutare le candidature degli studenti della vostra scuola per i nostri stage.
                    <br><br>
                    Qualora riteniate un alunno idoneo allo stage proposto e l’azienda facesse lo stesso, il portale vi metterà a disposizione tutti i moduli per la formalizzazione dell’attività di Alternanza.                    <br><br>Per accedere al portale, utilizzate il vostro login su studentsonstage.it.
                    <br><br>Per accedere al portale, utilizzate il vostro login su studentsonstage.it.
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "VERIFICA EMAIL";
                break;
            case "STUDENT-REQUEST-INTERNSHIP":
                $subject = "Candidatura per " . $title_to['internship_data']->job_title . " presso " . $title_to['internship_data']->company_name . " inviata";
                $title = "Ciao " . $title_to['student_name'] . ",";
                $body = "Grazie per aver utilizzato StudentsOnStage. 
                    <br><br>Adesso devi aspettare l’approvazione della richiesta di stage da parte della tua scuola. Il Referente per l’Orientamento ha ricevuto una mail e deve accedere al sito per approvare la tua candidatura. 
                    <br><br>Appena la scuola avrà accettato o meno la richiesta, riceverai una mail di conferma. Puoi vedere lo status della tua richiesta nella sezione “Home” del portale.
                    <br><br>Utilizza il tuo login per accedere a studentsonstage.it.
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "REQUEST-INTERNSHIP-TO-SCHOOL-SIGNED":
                //$subject = $title_to['student_name'] . " si è candidato per " . $title_to['internship_data']->job_title . " presso " . $title_to['internship_data']->company_name;
                $subject = "Candidatura di un vostro studente per uno stage";
                $title = "Buongiorno da StudentsOnStage, il portale per l'alternanza scuola-lavoro.";
                $body = "Lo studente " . $title_to['student_name'] . " ha inviato la propria candidatura per uno dei nostri stage. All’interno del vostro profilo potete confermare o meno l’idoneità dello studente allo stage. 
                    <br><br>Qualora il vostro parere fosse positivo e l’azienda ritenesse lo stesso, il portale vi metterà a disposizione tutti i moduli per la formalizzazione dell’attività di Alternanza.
                    <br><br>Per valutare le candidature, utilizzate il vostro login su studentsonstage.it.
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "REQUEST-INTERNSHIP-TO-SCHOOL-NOT-SIGNED":
                //$subject = $title_to['student_name'] . " si è candidato per " . $title_to['internship_data']->job_title . " presso " . $title_to['internship_data']->company_name;
                $subject = "Candidatura di un vostro studente per uno stage";
                $title = "Buongiorno da StudentsOnStage, il portale per l'alternanza scuola-lavoro.";
                $body = "Lo studente " . $title_to['student_name'] . " ha inviato la propria candidatura per uno dei nostri stage. Registrandovi al portale, avrete la possibilità di confermare o meno l’idoneità dello studente allo stage.
                    <br><br>Qualora il vostro parere fosse positivo e l’azienda ritenesse lo stesso, il portale vi metterà a disposizione tutti i moduli per la formalizzazione dell’attività di Alternanza.
                    <br><br>Per registrarvi, cliccate qui: <a href='www.studentsonstage.it'>www.studentsonstage.it</a>
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "REQUEST-INTERNSHIP-TO-COMPANY":
                $subject = "Candidatura ricevuta per " . $title_to['internship_data']->job_title;
                $title = "Buongiorno da StudentsOnStage, il portale per l'alternanza scuola-lavoro.";
                $body = "Uno studente ha inviato la propria candidatura per lo stage. All’interno del vostro profilo potete valutare l’idoneità della stessa.
                    <br><br>Qualora il vostro parere fosse positivo e la scuola ritenesse lo stesso, il portale vi metterà a disposizione tutti i moduli per la formalizzazione dell’attività di Alternanza.
                    <br><br>Per valutare le candidature, utilizzate il login su studentsonstage.it.
                    <br><br>Buon lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "REQUEST-INTERNSHIP-CONFIRMED-BY-COMPANY-AFTER-SCHOOL":
                $subject = "Candidatura accettata da " . $title_to['internship_data']->company_name;
                $title = "Ciao " . $title_to['student_name'] . ",";
                $body = "Complimenti da StudentsOnStage!
                    <br><br>Dopo un’attenta revisione del tuo profilo, anche l’azienda ha deciso di accettare la tua candidatura allo stage. Il portale ha appena fornito a te, alla scuola e all’azienda i moduli necessari per la formalizzazione dell’attività di Alternanza.
                    <br><br>Per accedere al portale, utilizzate il vostro login su studentsonstage.it.
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "TO-COMPANY-SCHOOL-HAS-APPROVED-AFTER":
                $subject = "Candidatura per " . $title_to['internship_data']->job_title . " accettata dalla scuola";
                $title = "Buon giorno da StudentsOnStage, il portale per l'Alternanza scuola-lavoro.";
                $body = "Complimenti da StudentsOnStage!
                    <br><br>Il Referente per l’Orientamento ha accettato la candidatura di " . $title_to['student_name'] . ", già selezionato/a da voi per lo stage.
                    <br><br>Il portale ha appena fornito a voi, alla scuola e allo studente i moduli necessari per la formalizzazione dell’attività di Alternanza.
                    <br><br>Utilizzate il vostro login per accedere a studentsonstage.it
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "TO-COMPANY-SCHOOL-HAS-APPROVED-BEFORE":
                $subject = "Formalizzazione  " . $title_to['internship_data']->job_title;
                $title = "Buon giorno da StudentsOnStage, il portale per l'Alternanza scuola-lavoro.";
                $body = "Avete appena accettato la candidatura di " . $title_to['student_name'] . ", già ritenuto/a idoneo/a dalla scuola per lo stage.
                    <br><br>Il portale ha appena fornito a voi, alla scuola e allo studente i moduli necessari per la formalizzazione dell’attività di Alternanza.
                    <br><br>Vi ringraziamo per l’opportunità che avete dato ai nostri studenti!
                    <br><br>Utilizzate il vostro login per accedere a studentsonstage.it
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "REQUEST-INTERNSHIP-CONFIRMED-BY-COMPANY-NOT-YET-BY-SCHOOL":
                $subject = "Candidatura per " . $title_to['internship_data']->job_title . " accettata da " . $title_to['internship_data']->company_name;
                $title = "Ciao " . $title_to['student_name'] . ",";
                $body = "Complimenti da StudentsOnStage!
                    <br><br>Dopo un’attenta revisione del tuo profilo, l’azienda ha deciso di accettare la tua candidatura allo stage. La candidatura deve ancora essere valutata dalla scuola.
                    <br><br>Qualora la scuola ritenesse idoneo il tuo profilo per lo stage, il portale ti fornirà tutti i moduli necessari per la formalizzazione dell’attività di Alternanza.
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "REQUEST-INTERNSHIP-CONFIRMED-BY-SCHOOL-NOT-YET-BY-COMPANY":
                $subject = "Candidatura per " . $title_to['internship_data']->job_title . " accettata dalla tua scuola";
                $title = "Ciao " . $title_to['student_name'] . ", ";
                $body = "Complimenti da StudentsOnStage!
                    <br><br>Il Referente per l’Orientamento ha accettato la tua candidatura. La candidatura deve ancora essere valutata dall’azienda.
                    <br><br>Qualora l’azienda ritenesse idoneo il tuo profilo, il portale ti fornirà tutti i moduli necessari per la formalizzazione dell’attività di Alternanza.
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "REQUEST-INTERNSHIP-CONFIRMED-BY-SCHOOL-AFTER-COMPANY":
                $subject = "Candidatura per " . $title_to['internship_data']->job_title . " accettata dalla tua scuola";
                $title = "Ciao " . $title_to['student_name'] . ",";
                $body = "Complimenti da StudentsOnStage!
                    <br><br>Anche il Referente per l’Orientamento ha accettato la tua candidatura! Il portale ha appena fornito a te, alla scuola e all’azienda i moduli necessari per la formalizzazione dell’attività di Alternanza.
                    <br><br>Utilizza il tuo login per accedere a studentsonstage.it.
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "TO-SCHOOL-COMPANY-HAS-APPROVED-AFTER":
                $subject = "Candidatura accettata da " . $title_to['internship_data']->company_name;
                $title = "Buon giorno da StudentsOnStage, il portale per l'Alternanza scuola-lavoro. ";
                $body = "Dopo un’attenta revisione del profilo, l’azienda ha deciso di accettare la candidatura di " . $title_to['student_name'] . ", già ritenuto/a idoneo/a da voi per lo stage " . $title_to['internship_data']->job_title . " .
                    <br>Il portale ha appena fornito a voi, all’azienda e allo studente i moduli necessari per la formalizzazione dell’attività di Alternanza.
                    <br><br>Utilizzate il vostro login per accedere a studentsonstage.it.
                    <br><br>Buon lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "TO-SCHOOL-COMPANY-HAS-APPROVED-BEFORE":
                $subject = "Formalizzazione " . $title_to['internship_data']->job_title;
                $title = "Buon giorno da StudentsOnStage, il portale per l'Alternanza scuola-lavoro. ";
                $body = "Avete appena ritenuta idonea la candidatura di  " . $title_to['student_name'] . ", già selezionato/a da " . $title_to['internship_data']->company_name . " per lo stage.
                    <br>Il portale ha appena fornito a voi, all’azienda e allo studente i moduli necessari per la formalizzazione dell’attività di Alternanza.
                    <br><br>Utilizzate il vostro login per accedere a studentsonstage.it.
                    <br><br>Buon lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "REQUEST-INTERNSHIP-REJECTED-BY-COMPANY":
                $subject = "Candidatura rifiutata da " . $title_to['internship_data']->company_name;
                $title = "Ciao " . $title_to['student_name'] . ",";
                $body = "ti ringraziamo per aver utilizzato StudentsOnStage. 
                    <br><br>Dopo un’attenta revisione del tuo profilo, l’azienda ha deciso di non accettare la tua candidatura  e quindi l’attività di Alternanza non può essere formalizzata.<br>
                    Ti consigliamo di cercare un altro stage e riproporre la tua candidatura!
                    <br><br>Utilizza il tuo login per accedere a studentsonstage.it.
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "REQUEST-INTERNSHIP-REJECTED-BY-SCHOOL":
                $subject = "Candidatura per " . $title_to['internship_data']->job_title . " non accettata dalla tua scuola";
                $title = "Ciao " . $title_to['student_name'] . ",";
                $body = "ti ringraziamo per aver utilizzato StudentsOnStage. 
                    <br><br>Purtroppo il Referente per l’Orientamento non ha accettato la tua candidatura e quindi l’attività di Alternanza non può essere formalizzata.<br>
                    Ti consigliamo di cercare un altro stage e riproporre la tua candidatura!
                    <br><br>Utilizza il tuo login per accedere a studentsonstage.it.
                    <br><br>Buon Lavoro!
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
            case "NEW-WEB-SITE":
                $subject = "Aggiornamento portale StudentsOnStage";
                $title = "Buongiorno da StudentsOnStage,";
                $body = "La presente per informarvi che a seguito di un aggiornamento del portale, si è reso necessario il reset delle password di tutti gli utenti.
                    <br><br>Vi preghiamo pertanto di inserire una nuova password tramite l'opzione \"Password dimenticata\" presente in fase di login su studentsonstage.it.
                    <br><br>Vi invitiamo a scoprire le nuove funzionalità del sito e cogliamo l'occasione per ringraziarvi della fiducia accordataci.
                    <br><br>Il Team di StudentsOnStage
                    <br><br>Per qualsiasi informazione, visitate il sito studentsonstage.it o scrivete a info@studentsonstage.it";
                $label_call_to_action = "Vai sul sito.";
                break;
        }
        $data = array("subject" => $subject, "body" => $body, "label_call_to_action" => $label_call_to_action, "title" => $title);


        return $data;
    }

    public static function getHtmlContent($data, $url_call_to_action, $email_tmp = null) {
        $url = "../public/";
        $url .= ($email_tmp != null) ? $email_tmp : env('EMAIL_TMP');
        $html = file_get_contents($url);
        $html = preg_replace('%title_replace%', $data["title"], $html);
        $html = preg_replace('%body_replace%', $data["body"], $html);
        $html = preg_replace('%label_call_to_action%', $data["label_call_to_action"], $html);
        $html = preg_replace('%url_call_to_action%', $url_call_to_action, $html);
        return $html;
    }

    public static function sendEmail($email, $html, $data) {
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = env('MAIL_HOST');  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = env('MAIL_USERNAME');                 // SMTP username
        $mail->Password = env('MAIL_PASSWORD');                            // SMTP password
        $mail->SMTPSecure = env('MAIL_ENCRYPTION');                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = env('MAIL_PORT');

        /* Send Email */
        $mail->setFrom(env('FROM_EMAIL'), 'StudentsOnStage');
        $mail->addAddress($email, "");
        $mail->isHTML(true);
        $mail->Subject =  $data["subject"];
        $mail->Body = $html;

        if (!$mail->send()) {
            $result = $mail->ErrorInfo;
        } else {
            $result = 'sent';
        }
        return $result;
    }

}
