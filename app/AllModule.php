<?php

namespace App;

use DB;
use App\ModuleACompany;
use App\ModuleASchool;
use App\InternshipRequest;
use App\ModuleBStudent;
use App\ModuleFStudent;
use App\Internship;
use Illuminate\Database\Eloquent\Model;

class AllModule extends Model {

    public static $PENDING = 0;
    public static $PROGRESSING = 1;
    public static $COMPLETED = 2;
    public static $SENT = 3;

    public function createAllModule($internship_request_id) {
        /* Check for valid company user */
        $internship_request = InternshipRequest::where(array('id' => $internship_request_id))->first();

        //Create modul a company
        try {
            //ModuleACompany
            $moduleACompany = new ModuleACompany();
            $moduleACompany->internship_request_id = $internship_request->id;
            $moduleACompany->company_id = $internship_request->company_id;
            $moduleACompany->status = self::$PENDING;
            $moduleACompany->save();

            //ModuleASchool
            $moduleASchool = new ModuleASchool();
            $moduleASchool->internship_request_id = $internship_request->id;
            $moduleASchool->school_id = $internship_request->school_id;
            $moduleASchool->status = self::$PENDING;
            $moduleASchool->save();

            //ModuleASchool
            $moduleASchool = new ModuleBStudent;
            $moduleASchool->internship_request_id = $internship_request->id;
            $moduleASchool->student_id = $internship_request->student_id;
            $moduleASchool->status = self::$PENDING;
            $moduleASchool->save();
        } catch (Exception $exc) {
            $error = $exc->getTraceAsString();
        }


        return TRUE;
    }

    public function createRatingModule() {
        $internship_request = InternshipRequest::where(array('is_approved_by_company' => Internship::$CONFIRMED, "is_approved_by_school" => Internship::$CONFIRMED))->get();
        print_r($internship_request);
        exit();
        //Create modul a company
        foreach ($internship_request as $key => $value) {

            try {
                //ModuleECompany
                $moduleECompany = new ModuleECompany();
                $moduleECompany->internship_request_id = $value->id;
                $moduleECompany->company_id = $value->company_id;
                $moduleECompany->status = self::$PENDING;
                $moduleECompany->save();

                //ModuleASchool
                /*
                  $moduleASchool = new ModuleASchool();
                  $moduleASchool->internship_request_id = $internship_request->id;
                  $moduleASchool->school_id = $internship_request->school_id;
                  $moduleASchool->status = self::$PENDING;
                  $moduleASchool->save();
                * 
                */
                  //ModuleASchool
                  $moduleFStudent = new ModuleFStudent;
                  $moduleFStudent->internship_request_id = $value->id;
                  $moduleFStudent->student_id = $value->student_id;
                  $moduleFStudent->status = self::$PENDING;
                  $moduleFStudent->save();
                 
            } catch (Exception $exc) {
                $error = $exc->getTraceAsString();
            }
        }
    }

}
