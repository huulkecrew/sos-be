<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ModuleFStudent extends Model
{
    protected $table = 'module_f_student';
    
    public function getModuleFStudent($param, $count=false){
        /* Check for valid company user */
        
        
        $module_f_student = DB::table($this->table)
                ->join('internship_request', 'internship_request.id', '=', 'module_f_student.internship_request_id')
                ->join('company', 'company.id', '=', 'internship_request.company_id')
                ->join('school', 'school.id', '=', 'internship_request.school_id')
                ->join('internship', 'internship.id', '=', 'internship_request.internship_id')
                ->select('internship_request.*', "company.*","school.name", "school.school_logo","internship.*" ,"internship_request.id as internship_request_id")
                ->where('module_f_student.student_id', '=', $param["student_id"])
                ->orderBy('module_f_student.updated_at', 'desc');
        if(isset($param["status"]) && $param["status"] > 0 ){
            $module_f_student->where('module_f_student.status', '=', $param["status"]);
        }
        if( isset($param["internship_request_id"]) && $param["internship_request_id"]>0 ){

            $module_f_student->where('module_f_student.internship_request_id', '=', $param["internship_request_id"])
                    ->addSelect('module_f_student.*');
            $data = $module_f_student->first();
        } else {
            if($count){
                $data = $module_f_student->count(); 
            } else {
                $module_f_student->limit($param['limit'])
                        ->offset($param['offset']);
                $data = $module_f_student->get(); 
            }
        }


        return $data;
        
        
    }
}
