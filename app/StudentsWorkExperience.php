<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentsWorkExperience extends Model
{
    protected $table = 'students_work_experience';
}
