<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class ModuleASchool extends Model
{
    protected $table = 'module_a_school';
    
    public function getModuleASchool($param, $count=false){
        /* Check for valid company user */
        
        
        $module_a_company = DB::table($this->table)
                ->join('internship_request', 'internship_request.id', '=', 'module_a_school.internship_request_id')
                ->join('students', 'students.id', '=', 'internship_request.student_id')
                ->join('company', 'company.id', '=', 'internship_request.company_id')
                ->join('internship', 'internship.id', '=', 'internship_request.internship_id')
                ->select('internship_request.*', "students.*","company.company_name", "company.company_logo","internship.*", "internship_request.id as internship_request_id")
                ->where('module_a_school.school_id', '=', $param["school_id"])
                ->orderBy('module_a_school.updated_at', 'desc');
        if(isset($param["status"]) && $param["status"] > 0 ){
            $module_a_company->where('module_a_school.status', '=', $param["status"]);
        }
        if( isset($param["internship_request_id"]) && $param["internship_request_id"]>0 ){

            $module_a_company->where('module_a_school.internship_request_id', '=', $param["internship_request_id"])
                    ->addSelect('module_a_school.*');
            $data = $module_a_company->first();
        } else {
            if($count){
                $data = $module_a_company->count(); 
            } else {
                $module_a_company->limit($param['limit'])
                        ->offset($param['offset']);
                $data = $module_a_company->get(); 
            }
        }


        return $data;
        
        
    }
}
