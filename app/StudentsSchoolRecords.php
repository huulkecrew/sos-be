<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentsSchoolRecords extends Model
{
    protected $table = 'students_school_records';
}
