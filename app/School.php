<?php

namespace App;

use DB;
use App\School;
use Illuminate\Database\Eloquent\Model;

class School extends Model {

    protected $table = 'school';

    /**
     * Get all schools
     * @param $data
     * @return $schools
     */
    public function getAllSchools($data) {
        $schools = array();
        $query = DB::table('school')
                ->join('users', 'users.id', '=', 'school.school_user_id')
                ->select('school.*')
                ->where('school.status', '=', 1)
                ->where('school.zip', '>',0);

        if (!empty($data['keyword'])) {
            $query->whereRaw('(school.name LIKE "%' . $data['keyword'] . '%")');
        }

        $query->orderBy('school.name', 'asc');
        $schools = $query->get();
        return $schools;
    }

    /**
     * Get total schools
     * @param $data
     * @return $internships
     */
    public function getTotalSchools($data) {
        $query = Internship::where(array('status' => 1));

        if (!empty($data['keyword'])) {
            $query->whereRaw('(name LIKE "%' . $data['keyword'] . '%")');
        }

        $internships = $query->count();
        return $internships;
    }

}
