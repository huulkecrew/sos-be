<?php
namespace app\Helpers;

use App\User;
use App\Users;
use App\UserTypes;

class GlobalHelper {

	public function __construct(){

	}

	/**
	 * Get all user types
	 * @return $userTypes
	 */
	public function getAllUserTypes(){
		$userTypes = array();
		$types = UserTypes::all();
		if(!empty($types)) {
			foreach($types as $val){
				$userTypes[] = array(
					'id' => $val->id,
					'type_name' => $val->type_name
				);
			}
		}
		return $userTypes;
	}

	/**
	 * Check for valid user type
	 * @param $typeId
	 * @return $checkType 
	 */
	public function checkValidUserType($typeId){
		$checkType = UserTypes::where('id', $typeId)->count();
	}
}