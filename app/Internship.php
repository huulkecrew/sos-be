<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Internship extends Model
{
    protected $table = 'internship';
    public static $INTERNSHIP_ACTIVE = 1;
    public static $CONFIRMED = 1;
    public static $REJECTED = 2;
    public static $PENDING = 0;
    public  $internship_categories = array();

    /**
     * Get all internship
     * @param $data
     * @return $internships
     */
    public function getAllInternship($data){
    	$internships = array();
    	$query = Internship::where(array('status' => 1));

        if(!empty($data['company_id'])) {
            $query->where('company_id', $data['company_id']);
        }

    	if(!empty($data['keyword'])) {
    		$query->whereRaw('(company_name LIKE "%'.$data['keyword'].'%" OR job_title LIKE "%'.$data['keyword'].'%" OR job_description LIKE "%'.$data['keyword'].'%" OR job_location LIKE "%'.$data['keyword'].'%")');
    	}

		$query->limit($data['limit']);
		$query->offset($data['offset']);
		$query->orderBy('created_at', 'desc');
		$internships = $query->get();
		return $internships;
    }

    /**
     * Get total internship
     * @param $data
     * @return $internships
     */
    public function getTotalInternship($data){
    	$query = Internship::where(array('status' => 1));

        if(!empty($data['company_id'])) {
            $query->where('company_id', $data['company_id']);
        }
        
    	if(!empty($data['keyword'])) {
    		$query->whereRaw('(company_name LIKE "%'.$data['keyword'].'%" OR job_title LIKE "%'.$data['keyword'].'%" OR job_description LIKE "%'.$data['keyword'].'%" OR job_location LIKE "%'.$data['keyword'].'%")');
    	}

		$internships = $query->count();
		return $internships;
    }
    
    /**
     * Get all internship by company or school
     * @param $data
     * @return $internships
     */
    public function getIntershipByCompany($data , $count = false ) {
        $internships = DB::table('internship')
                ->join('company', 'company.id', '=', 'internship.company_id')
                ->select('internship.*', "company.company_name", "company.company_logo")
                ->where('company.id', '=', $data["company_id"])
                ->where('internship.status', '=', self::$INTERNSHIP_ACTIVE)
                ->orderBy('updated_at', 'desc');
        if (isset($data["internship_id"])) {
            $internships->where('internship.id', '=', $data["internship_id"])
                    ->first();
        } else {
            if($count){
                $data = $internships->count(); 
            } else {
                $internships->limit($data['limit'])
                        ->offset($data['offset']);
                $data = $internships->get(); 
            }
        }


        return $data;
    }
    
    /**
     * Get total internship
     * @param $data
     * @return $internships
     */
    public function countIntershipByCompany($data ) {
        return count($this->getIntershipByCompany($data , TRUE));
    }
    

    
    
    
}
