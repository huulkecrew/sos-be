<?php

namespace App;

use DB;
use App\InternshipRequest;
use Illuminate\Database\Eloquent\Model;

class InternshipRequest extends Model {

    protected $table = 'internship_request';

    /**
     * Get total internship
     * @param $data
     * @return $internships
     */
    public function getIntershipApplied($data , $count = false ) {
        $internships = DB::table('internship_request')
                ->join('internship', 'internship.id', '=', 'internship_request.internship_id')
                ->join('company', 'company.id', '=', 'internship.company_id')
                ->select('internship_request.*', 'internship.job_title', 'internship.job_description', "company.company_name", "company.company_logo")
                ->where('internship_request.student_id', '=', $data["student_id"])
                ->orderBy('updated_at', 'desc');
        if (isset($data["internship_request_id"])) {
            $result = $internships->where('internship_request.id', '=', $data["internship_request_id"])
                    ->get();
        } else {
            if($count){
                $result  = $internships->count(); 
            } else {
                $internships->limit($data['limit'])
                        ->offset($data['offset']);
                $result = $internships->get(); 
            }
        }


        return $result;
    }
    /**
     * Get appliers internship
     * @param $data
     * @return $internships
     */
    public function getAppliersInterhipByCompany($data , $count = false ) {
        $internships = DB::table('internship_request')
                ->join('internship', 'internship.id', '=', 'internship_request.internship_id')
                ->join('company', 'company.id', '=', 'internship.company_id')
                ->join('students', 'students.id', '=', 'internship_request.student_id')
                ->leftJoin('school', 'school.id', '=', 'internship_request.school_id')
                ->select('internship.id as internship_id','internship.*','internship_request.motivation_letter','internship_request.id as internship_request_id','internship_request.is_approved_by_company','internship_request.is_approved_by_school','internship_request.school_name','students.*', "company.company_name", "company.company_logo"
                        ,'school.phone as school_phone', 'school.zip as school_zip' , 'school.city as school_city','school.province as school_province','school.address as school_address'
                        )
                ->where('company.id', '=', $data["company_id"]);
        if(isset($data["internship_id"])){ 
            $internships->where('internship.id', '=', $data["internship_id"]);
        }        
        if (isset($data["internship_request_id"])) {
            $internships->where('internship_request.id', '=', $data["internship_request_id"]);
        } else {
            if($count){
                $data = $internships->count(); 
            } else {
                $internships->limit($data['limit'])
                        ->offset($data['offset'])
                        ->orderBy('internship_request.updated_at', 'desc');
                $data = $internships->get(); 
            }
        }
        return $data;
    }
    
    /**
     * count appliers internship
     * @param $data
     * @return int
     */
    public function countAppliersInterhipByCompany($data ) {
        return count($this->getAppliersInterhipByCompany($data , TRUE));
    }
    /**
     * Get appliers internship
     * @param $data
     * @return $internships
     */
    public function getAppliersInterhipBySchool($data , $count = false ) {
        $internships = DB::table('internship_request')
                ->join('internship', 'internship.id', '=', 'internship_request.internship_id')
                ->join('company', 'company.id', '=', 'internship.company_id')
                ->join('students', 'students.id', '=', 'internship_request.student_id')
                ->join('school', 'school.id', '=', 'students.school')
                ->select('internship.id as internship_id','internship_request.motivation_letter','internship_request.is_approved_by_school','internship_request.is_approved_by_company','internship_request.id as internship_request_id','internship_request.school_name','students.*', 'internship.*', "company.company_name", "company.company_logo")
                ->where('school.id', '=', $data["school_id"]);
        if(isset($data["internship_id"])){ 
            $internships->where('internship.id', '=', $data["internship_id"]);
        }        
        if (isset($data["internship_request_id"])) {
            $internships->where('internship_request.id', '=', $data["internship_request_id"]);
        }
        
        if ($count) {
            $data = $internships->count();
        } else {
            $internships->limit($data['limit'])
                    ->offset($data['offset'])
                    ->orderBy('internship_request.updated_at', 'desc');
            $data = $internships->get();
        }


        return $data;
    }
    
    /**
     * count appliers internship
     * @param $data
     * @return int
     */
    public function countAppliersInterhipBySchool($data ) {
        return count($this->getAppliersInterhipBySchool($data , TRUE));
    }

    /**
     * Get total internship
     * @param $data
     * @return int
     */
    public function countIntershipApplied($data ) {
        return count($this->getIntershipApplied($data , TRUE));
    }

    /**
     * 
     */
    public function getNotificationStudent($data) {
        $internships = DB::table('internship_request')
                ->join('internship', 'internship.id', '=', 'internship_request.internship_id')
                ->join('company', 'company.id', '=', 'internship.company_id')
                ->select('internship_request.*', 'internship.job_title', 'internship.job_description', "company.company_name", "company.company_name", "company.company_logo");
                //WE chack if one of them has already said yes
                if(isset($data["student_id"])){
                $internships->whereRaw('internship_request.is_approved_by_company + internship_request.is_approved_by_school > 0 ')
                ->where('internship_request.student_id', '=', $data["student_id"]);
                }
                if(isset($data["company_id"])){
                $internships->whereRaw(' internship_request.is_approved_by_school != 3  ')
//                ->where('internship_request.is_approved_by_company  > 0 ')
                ->where('internship.company_id', '=', $data["company_id"]);
                }
                if(isset($data["school_id"])){
                $internships->whereRaw('internship_request.is_approved_by_company  > 0 ')
//                ->where('internship_request.is_approved_by_company  > 0 ')
                ->where('internship.school_id', '=', $data["school_id"]);
                }
                
                $internships->orderBy('updated_at', 'desc')
                ->limit($data['limit'])
                ->offset($data['offset']);

        return $internships->get();
    }
    
    /**
     * 
     */
    
    public function updateInternshipRequest($data) {
        $internships = DB::table('internship_request')
                    ->where('id', $data["internship_request_id"]);
        if(isset($data["company_id"])){
            $internships->where('company_id', $data["company_id"]);
            $response = $internships->update(['is_approved_by_company' =>$data["status"] ]);
        } elseif (isset($data["school_id"])) {
//            $internships->update(['school_id' =>$data["school_id"] ]);    
            $response  = $internships->update(['is_approved_by_school' =>$data["status"] ]);    
        }
        return $response;
    }
    
    
    
    

}
