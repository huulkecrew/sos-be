<?php

namespace App;

use URL;
use DB;
use Illuminate\Database\Eloquent\Model;

class Image extends Model {

    public static function getImageProfile($type_img, $Object) {
        switch ($type_img) {
            case "Company":
                $path_img = "COMPANY_LOGO_PATH";
                $path_default = "COMPANY_DEFAULT_PIC";
                $param = "company_logo";
                break;
            case "School":
                $path_img = "COMPANY_LOGO_PATH";
                $path_default = "COMPANY_DEFAULT_PIC";
                $param = "school_logo";
                break;
            case "Student":
                $path_img = "STUDENT_PROFILE_PIC_PATH";
                $path_default = "STUDENT_DEFAULT_PIC";
                $param = "profile_pic";
                break;
        }
        if (isset($Object->id)) {
            $profileData = $Object;
            $Object->{$param} = (!empty($profileData->{$param})) ? URL::to('/') . env($path_img) . $profileData->{$param} : URL::to('/') . env($path_default);

        } else {
            for ($i = 0; $i < count($Object); $i ++) {
                $profileData = $Object[$i];
                $Object[$i]->{$param} = (!empty($profileData->{$param})) ? URL::to('/') . env($path_img) . $profileData->{$param} : URL::to('/') . env($path_default);
            }
        }


        return $Object;
    }

}
