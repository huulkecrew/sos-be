// var elixir = require('laravel-elixir');
//
// /*
//  |--------------------------------------------------------------------------
//  | Elixir Asset Management
//  |--------------------------------------------------------------------------
//  |
//  | Elixir provides a clean, fluent API for defining some basic Gulp tasks
//  | for your Laravel application. By default, we are compiling the Sass
//  | file for our application, as well as publishing vendor resources.
//  |
//  */
//
// elixir(function(mix) {
//     mix.sass('app.scss');
// });

'use strict';
var gulp = require('gulp'),
    apidoc = require('gulp-apidoc');

gulp.task('apidoc', function(done) {
      apidoc({
        src: "./app/Http/Controllers/",
        dest: "./public/docs/",
        debug: true,
        includeFilters: [ ".*\\.php$" ]
      }, done);
});

gulp.task('doc', ['apidoc']);

gulp.task('default',['apidoc']);
